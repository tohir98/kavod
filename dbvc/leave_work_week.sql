-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2016 at 09:37 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hrs_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `leave_work_week`
--

CREATE TABLE IF NOT EXISTS `leave_work_week` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `work_mon` varchar(20) NOT NULL,
  `work_tue` varchar(20) NOT NULL,
  `work_wed` varchar(20) NOT NULL,
  `work_thu` varchar(20) NOT NULL,
  `work_fri` varchar(20) NOT NULL,
  `work_sat` varchar(20) NOT NULL,
  `work_sun` varchar(20) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `leave_work_week`
--

INSERT INTO `leave_work_week` (`id`, `work_mon`, `work_tue`, `work_wed`, `work_thu`, `work_fri`, `work_sat`, `work_sun`, `creator_id`, `company_id`) VALUES
(1, 'fullday', 'fullday', 'fullday', 'fullday', 'fullday', 'nwday', 'nwday', 18, 100);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
