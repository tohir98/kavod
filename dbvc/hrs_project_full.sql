/*
Navicat MySQL Data Transfer

Source Server         : adms
Source Server Version : 50045
Source Host           : localhost:17770
Source Database       : hrs_project

Target Server Type    : MYSQL
Target Server Version : 50045
File Encoding         : 65001

Date: 2016-02-23 06:21:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `access_levels`
-- ----------------------------
DROP TABLE IF EXISTS `access_levels`;
CREATE TABLE `access_levels` (
  `access_level_id` int(10) unsigned NOT NULL auto_increment,
  `access_level` varchar(150) default NULL,
  `default_values` varchar(150) default NULL,
  PRIMARY KEY  (`access_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of access_levels
-- ----------------------------

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(10) unsigned NOT NULL auto_increment,
  `email` varchar(45) default NULL,
  `password` varchar(45) default NULL,
  PRIMARY KEY  (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------

-- ----------------------------
-- Table structure for `attendance_shifts`
-- ----------------------------
DROP TABLE IF EXISTS `attendance_shifts`;
CREATE TABLE `attendance_shifts` (
  `attendance_shift_id` int(10) unsigned NOT NULL auto_increment,
  `shift_name` varchar(150) default NULL,
  `clock_in` datetime default NULL,
  `clock_out` datetime default NULL,
  `break_in` varchar(150) default NULL,
  `break_out` varchar(150) default NULL,
  `overtime` varchar(150) default NULL,
  `company_id` int(10) unsigned default NULL,
  `lateness` varchar(150) default NULL,
  `created_by` varchar(150) default NULL,
  `date_created` datetime default NULL,
  PRIMARY KEY  (`attendance_shift_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attendance_shifts
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_group_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL auto_increment,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_group_id` (`group_id`),
  KEY `auth_group_permissions_permission_id` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_message`
-- ----------------------------
DROP TABLE IF EXISTS `auth_message`;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `auth_message_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_message
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_permission`
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_content_type_id` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'Can add session', '1', 'add_session');
INSERT INTO `auth_permission` VALUES ('2', 'Can change session', '1', 'change_session');
INSERT INTO `auth_permission` VALUES ('3', 'Can delete session', '1', 'delete_session');
INSERT INTO `auth_permission` VALUES ('4', 'Can add permission', '2', 'add_permission');
INSERT INTO `auth_permission` VALUES ('5', 'Can change permission', '2', 'change_permission');
INSERT INTO `auth_permission` VALUES ('6', 'Can delete permission', '2', 'delete_permission');
INSERT INTO `auth_permission` VALUES ('7', 'Can add group', '3', 'add_group');
INSERT INTO `auth_permission` VALUES ('8', 'Can change group', '3', 'change_group');
INSERT INTO `auth_permission` VALUES ('9', 'Can delete group', '3', 'delete_group');
INSERT INTO `auth_permission` VALUES ('10', 'Can add user', '4', 'add_user');
INSERT INTO `auth_permission` VALUES ('11', 'Can change user', '4', 'change_user');
INSERT INTO `auth_permission` VALUES ('12', 'Can delete user', '4', 'delete_user');
INSERT INTO `auth_permission` VALUES ('13', 'Can add message', '5', 'add_message');
INSERT INTO `auth_permission` VALUES ('14', 'Can change message', '5', 'change_message');
INSERT INTO `auth_permission` VALUES ('15', 'Can delete message', '5', 'delete_message');
INSERT INTO `auth_permission` VALUES ('16', 'Can add content type', '6', 'add_contenttype');
INSERT INTO `auth_permission` VALUES ('17', 'Can change content type', '6', 'change_contenttype');
INSERT INTO `auth_permission` VALUES ('18', 'Can delete content type', '6', 'delete_contenttype');
INSERT INTO `auth_permission` VALUES ('19', 'Can add log entry', '7', 'add_logentry');
INSERT INTO `auth_permission` VALUES ('20', 'Can change log entry', '7', 'change_logentry');
INSERT INTO `auth_permission` VALUES ('21', 'Can delete log entry', '7', 'delete_logentry');
INSERT INTO `auth_permission` VALUES ('22', 'Can add department', '8', 'add_department');
INSERT INTO `auth_permission` VALUES ('23', 'Can change department', '8', 'change_department');
INSERT INTO `auth_permission` VALUES ('24', 'Can delete department', '8', 'delete_department');
INSERT INTO `auth_permission` VALUES ('25', 'Can add device', '9', 'add_iclock');
INSERT INTO `auth_permission` VALUES ('26', 'Can change device', '9', 'change_iclock');
INSERT INTO `auth_permission` VALUES ('27', 'Can delete device', '9', 'delete_iclock');
INSERT INTO `auth_permission` VALUES ('28', 'Pause device', '9', 'pause_iclock');
INSERT INTO `auth_permission` VALUES ('29', 'Resume a resumed device', '9', 'resume_iclock');
INSERT INTO `auth_permission` VALUES ('30', 'Upgrade firmware', '9', 'upgradefw_iclock');
INSERT INTO `auth_permission` VALUES ('31', 'Copy data between device', '9', 'copyudata_iclock');
INSERT INTO `auth_permission` VALUES ('32', 'Upload data again', '9', 'reloaddata_iclock');
INSERT INTO `auth_permission` VALUES ('33', 'Upload transactions again', '9', 'reloadlogdata_iclock');
INSERT INTO `auth_permission` VALUES ('34', 'Refresh device information', '9', 'info_iclock');
INSERT INTO `auth_permission` VALUES ('35', 'Reboot device', '9', 'reboot_iclock');
INSERT INTO `auth_permission` VALUES ('36', 'Upload new data', '9', 'loaddata_iclock');
INSERT INTO `auth_permission` VALUES ('37', 'Clear data in device', '9', 'cleardata_iclock');
INSERT INTO `auth_permission` VALUES ('38', 'Clear transactions in device', '9', 'clearlog_iclock');
INSERT INTO `auth_permission` VALUES ('39', 'Set options of device', '9', 'devoption_iclock');
INSERT INTO `auth_permission` VALUES ('40', 'Reset Password in device', '9', 'resetPwd_iclock');
INSERT INTO `auth_permission` VALUES ('41', 'Restore employee to device', '9', 'restoreData_iclock');
INSERT INTO `auth_permission` VALUES ('42', 'Output unlock signal', '9', 'unlock_iclock');
INSERT INTO `auth_permission` VALUES ('43', 'Terminate alarm signal', '9', 'unalarm_iclock');
INSERT INTO `auth_permission` VALUES ('44', 'Clear all employee', '9', 'clear_all_employee');
INSERT INTO `auth_permission` VALUES ('45', 'Upgrade by u-pack', '9', 'upgrade_by_u-pack');
INSERT INTO `auth_permission` VALUES ('46', 'Can add admin granted department', '10', 'add_deptadmin');
INSERT INTO `auth_permission` VALUES ('47', 'Can change admin granted department', '10', 'change_deptadmin');
INSERT INTO `auth_permission` VALUES ('48', 'Can delete admin granted department', '10', 'delete_deptadmin');
INSERT INTO `auth_permission` VALUES ('49', 'Can add employee', '11', 'add_employee');
INSERT INTO `auth_permission` VALUES ('50', 'Can change employee', '11', 'change_employee');
INSERT INTO `auth_permission` VALUES ('51', 'Can delete employee', '11', 'delete_employee');
INSERT INTO `auth_permission` VALUES ('52', 'Transfer employee to the device', '11', 'toDev_employee');
INSERT INTO `auth_permission` VALUES ('53', 'Transfer to the device templately', '11', 'toDevWithin_employee');
INSERT INTO `auth_permission` VALUES ('54', 'Delete employee from the device', '11', 'delDev_employee');
INSERT INTO `auth_permission` VALUES ('55', 'Employee leave', '11', 'empLeave_employee');
INSERT INTO `auth_permission` VALUES ('56', 'Move employee to a new device', '11', 'mvToDev_employee');
INSERT INTO `auth_permission` VALUES ('57', 'Change employee\'s department', '11', 'toDepart_employee');
INSERT INTO `auth_permission` VALUES ('58', 'Enroll employee\'s fingerprint', '11', 'enroll_employee');
INSERT INTO `auth_permission` VALUES ('59', 'DataBase', '11', 'optionsDatabase_employee');
INSERT INTO `auth_permission` VALUES ('60', 'Can add fingerprint', '12', 'add_fptemp');
INSERT INTO `auth_permission` VALUES ('61', 'Can change fingerprint', '12', 'change_fptemp');
INSERT INTO `auth_permission` VALUES ('62', 'Can delete fingerprint', '12', 'delete_fptemp');
INSERT INTO `auth_permission` VALUES ('63', 'Can add transaction', '13', 'add_transaction');
INSERT INTO `auth_permission` VALUES ('64', 'Can change transaction', '13', 'change_transaction');
INSERT INTO `auth_permission` VALUES ('65', 'Can delete transaction', '13', 'delete_transaction');
INSERT INTO `auth_permission` VALUES ('66', 'Clear Obsolete Data', '13', 'clearObsoleteData_transaction');
INSERT INTO `auth_permission` VALUES ('67', 'Can add device operation log', '14', 'add_oplog');
INSERT INTO `auth_permission` VALUES ('68', 'Can change device operation log', '14', 'change_oplog');
INSERT INTO `auth_permission` VALUES ('69', 'Can delete device operation log', '14', 'delete_oplog');
INSERT INTO `auth_permission` VALUES ('70', 'Transaction Monitor', '14', 'monitor_oplog');
INSERT INTO `auth_permission` VALUES ('71', 'Can add data from device', '15', 'add_devlog');
INSERT INTO `auth_permission` VALUES ('72', 'Can change data from device', '15', 'change_devlog');
INSERT INTO `auth_permission` VALUES ('73', 'Can delete data from device', '15', 'delete_devlog');
INSERT INTO `auth_permission` VALUES ('74', 'Can add command to device', '16', 'add_devcmds');
INSERT INTO `auth_permission` VALUES ('75', 'Can change command to device', '16', 'change_devcmds');
INSERT INTO `auth_permission` VALUES ('76', 'Can delete command to device', '16', 'delete_devcmds');
INSERT INTO `auth_permission` VALUES ('77', 'Can add public information', '17', 'add_messages');
INSERT INTO `auth_permission` VALUES ('78', 'Can change public information', '17', 'change_messages');
INSERT INTO `auth_permission` VALUES ('79', 'Can delete public information', '17', 'delete_messages');
INSERT INTO `auth_permission` VALUES ('80', 'Can add information subscription', '18', 'add_iclockmsg');
INSERT INTO `auth_permission` VALUES ('81', 'Can change information subscription', '18', 'change_iclockmsg');
INSERT INTO `auth_permission` VALUES ('82', 'Can delete information subscription', '18', 'delete_iclockmsg');
INSERT INTO `auth_permission` VALUES ('83', 'Can add administration log', '19', 'add_adminlog');
INSERT INTO `auth_permission` VALUES ('84', 'Can change administration log', '19', 'change_adminlog');
INSERT INTO `auth_permission` VALUES ('85', 'Can delete administration log', '19', 'delete_adminlog');
INSERT INTO `auth_permission` VALUES ('86', 'Can browse ContentType', '6', 'browse_contenttype');
INSERT INTO `auth_permission` VALUES ('87', 'Can browse DeptAdmin', '10', 'browse_deptadmin');
INSERT INTO `auth_permission` VALUES ('88', 'Can browse Group', '3', 'browse_group');
INSERT INTO `auth_permission` VALUES ('89', 'Can browse IclockMsg', '18', 'browse_iclockmsg');
INSERT INTO `auth_permission` VALUES ('90', 'Can browse Permission', '2', 'browse_permission');
INSERT INTO `auth_permission` VALUES ('91', 'Can browse User', '4', 'browse_user');
INSERT INTO `auth_permission` VALUES ('92', 'Can browse adminLog', '19', 'browse_adminlog');
INSERT INTO `auth_permission` VALUES ('93', 'Can browse department', '8', 'browse_department');
INSERT INTO `auth_permission` VALUES ('94', 'Can browse devcmds', '16', 'browse_devcmds');
INSERT INTO `auth_permission` VALUES ('95', 'Can browse devlog', '15', 'browse_devlog');
INSERT INTO `auth_permission` VALUES ('96', 'Can browse employee', '11', 'browse_employee');
INSERT INTO `auth_permission` VALUES ('97', 'Can browse fptemp', '12', 'browse_fptemp');
INSERT INTO `auth_permission` VALUES ('98', 'Can browse iclock', '9', 'browse_iclock');
INSERT INTO `auth_permission` VALUES ('99', 'Can browse messages', '17', 'browse_messages');
INSERT INTO `auth_permission` VALUES ('100', 'Can browse oplog', '14', 'browse_oplog');
INSERT INTO `auth_permission` VALUES ('101', 'Can browse transaction', '13', 'browse_transaction');
INSERT INTO `auth_permission` VALUES ('102', 'Init database', '13', 'init_database');

-- ----------------------------
-- Table structure for `auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES ('1', 'admin', '', '', 'fae_service@zkteco.com', 'sha1$3eb60$103b53a937f36a3809544315291b474ec66469b1', '1', '1', '1', '2015-11-17 08:43:39', '2013-06-18 16:32:08');

-- ----------------------------
-- Table structure for `auth_user_groups`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_user_id` (`user_id`),
  KEY `auth_user_groups_group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_user_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_user_id` (`user_id`),
  KEY `auth_user_user_permissions_permission_id` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `bank_account_type`
-- ----------------------------
DROP TABLE IF EXISTS `bank_account_type`;
CREATE TABLE `bank_account_type` (
  `account_type_id` int(10) unsigned NOT NULL auto_increment,
  `account_type` varchar(45) default NULL,
  PRIMARY KEY  (`account_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank_account_type
-- ----------------------------

-- ----------------------------
-- Table structure for `checkinout`
-- ----------------------------
DROP TABLE IF EXISTS `checkinout`;
CREATE TABLE `checkinout` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `checktime` datetime NOT NULL,
  `checktype` varchar(1) NOT NULL,
  `verifycode` int(11) NOT NULL,
  `SN` varchar(20) default NULL,
  `sensorid` varchar(5) default NULL,
  `WorkCode` varchar(20) default NULL,
  `Reserved` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `userid` (`userid`,`checktime`),
  KEY `checkinout_userid` (`userid`),
  KEY `checkinout_SN` (`SN`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of checkinout
-- ----------------------------
INSERT INTO `checkinout` VALUES ('1', '1', '2013-10-16 23:00:09', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('2', '1', '2013-10-16 23:15:35', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('3', '2', '2014-05-12 16:30:47', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('4', '2', '2014-05-12 09:32:22', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('5', '2', '2014-05-12 09:35:31', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('6', '2', '2014-05-12 10:16:22', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('7', '2', '2014-05-12 12:06:44', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('8', '2', '2014-05-14 15:44:13', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('9', '4', '2014-05-14 15:49:36', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('10', '5', '2014-05-14 15:57:04', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('11', '2', '2014-05-14 16:51:09', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('12', '2', '2014-05-14 16:51:26', '1', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('13', '2', '2014-05-14 16:52:11', '3', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('14', '4', '2014-05-14 16:53:21', '2', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('15', '2', '2014-05-14 16:53:44', '4', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('16', '4', '2014-05-14 21:10:56', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('17', '2', '2014-05-14 21:11:41', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('18', '5', '2014-05-14 21:12:32', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('19', '4', '2014-05-14 21:14:11', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('20', '4', '2014-05-14 21:41:43', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('21', '5', '2014-05-14 21:42:16', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('22', '2', '2014-05-14 21:52:25', '3', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('23', '2', '2014-05-15 00:15:11', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('24', '2', '2014-05-15 00:15:21', '1', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('25', '10', '2014-05-16 00:57:03', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('26', '10', '2014-05-16 00:58:48', '1', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('27', '10', '2014-05-15 18:01:48', '1', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('28', '9', '2014-05-15 18:03:06', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('29', '9', '2014-05-15 18:08:33', '1', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('30', '9', '2014-05-15 18:36:24', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('31', '10', '2014-05-15 18:36:36', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('32', '10', '2014-05-16 10:30:06', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('33', '10', '2014-05-16 10:30:19', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('34', '9', '2014-05-16 10:30:51', '0', '1', '0455134200004', '1', '', '');
INSERT INTO `checkinout` VALUES ('35', '9', '2014-05-16 10:32:21', '0', '1', '0455134200004', '1', '', '');

-- ----------------------------
-- Table structure for `companies`
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `company_id` int(10) unsigned NOT NULL auto_increment,
  `company_name` varchar(150) default NULL,
  `ref_code` varchar(45) default NULL,
  `email` varchar(150) default NULL,
  `first_name` varchar(45) default NULL,
  `last_name` varchar(45) default NULL,
  `user_count` varchar(45) default NULL,
  `password` varchar(45) default NULL,
  `account_type_id` int(10) unsigned default NULL,
  `status` varchar(45) default NULL,
  `date_created` datetime default NULL,
  `date_updated` datetime default NULL,
  PRIMARY KEY  (`company_id`),
  KEY `FK_companies_account_type` (`account_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('100', 'Zend Solutions', null, 'info@zendsolutions.com', 'Zend', 'Solutions', '500', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '1', '1', '2016-02-22 23:30:25', null);

-- ----------------------------
-- Table structure for `departments`
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `department_id` int(10) unsigned NOT NULL auto_increment,
  `department` varchar(150) default NULL,
  `company_id` int(10) unsigned default NULL,
  `date_created` datetime default NULL,
  `date_updated` datetime default NULL,
  `creator_id` int(10) unsigned default NULL,
  `parent_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of departments
-- ----------------------------

-- ----------------------------
-- Table structure for `departments_copy`
-- ----------------------------
DROP TABLE IF EXISTS `departments_copy`;
CREATE TABLE `departments_copy` (
  `DeptID` int(11) NOT NULL,
  `DeptName` varchar(20) NOT NULL,
  `supdeptid` int(11) default NULL,
  PRIMARY KEY  (`DeptID`),
  KEY `DEPTNAME` (`DeptName`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of departments_copy
-- ----------------------------
INSERT INTO `departments_copy` VALUES ('1', 'Our Company', '0');

-- ----------------------------
-- Table structure for `devcmds`
-- ----------------------------
DROP TABLE IF EXISTS `devcmds`;
CREATE TABLE `devcmds` (
  `id` int(11) NOT NULL auto_increment,
  `SN_id` varchar(20) NOT NULL,
  `CmdContent` longtext NOT NULL,
  `CmdCommitTime` datetime NOT NULL,
  `CmdTransTime` datetime default NULL,
  `CmdOverTime` datetime default NULL,
  `CmdReturn` int(11) default NULL,
  `User_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `devcmds_SN_id` (`SN_id`),
  KEY `devcmds_User_id` (`User_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of devcmds
-- ----------------------------
INSERT INTO `devcmds` VALUES ('33', '0455134200004', 'DATA DEL_USER PIN=178', '2015-11-03 10:55:59', null, null, null, null);
INSERT INTO `devcmds` VALUES ('34', '0455134200004', 'DATA USER PIN=222	Name=Karlen Kirakosyan	Pri=0		Card=[0000000000]	TZ=0000000000000000	Grp=1', '2015-11-03 10:55:59', null, null, null, null);
INSERT INTO `devcmds` VALUES ('35', '0455134200004', 'DATA FP PIN=222	FID=0	Size=1124	Valid=1	TMP=SgdTUzIxAAADREcECAUHCc7QAAAbRWkBAAAAg+kZj0QpAAQPtQCEAItLNABaAHwPIQBvRI0PgQBxAEQP20R4AA8PkwC6AAtLyACMAIwPggCoRPoPqgCvAEIOrES9AA4OwAAHAI5KWQDEAHEPTADQRH4P+ADmAFMPqET4AIgPpQDIAYFKjAAaAQAOZAAZRYEOdQAjAZoPrEQwAYkOmwD5AWlKWgBCAdoPcwBKRX4PiwBPATIPlDjGh7v/k4Te/yA/Ev8/g/N7kH1RR5YHHXrj+wJ/UsHMhh+LFYpigYA60H7SA1MEa4ycO2Z/LXgZ/x78JsyAgd6TgYFUC7hGu3gf9QfvK3PgvCb5gwtjE0aLRk1HCVqLTQQM/WrDgIHxo22DCACCxSj7FWN+ht/jZmYsDqELAeKwdimoh36z5Y/fcPqVUH+LZY6Ge6pwA9SBASAsAQEzHRpAAZshg38DxaQoQv4LAGYu+jr9ZrtWBwBhMgkHN8NBAVc0ff9dygAxegFYwS9t//ITA0tL/VL//8E6RVcPwxIABlnwOMD+gD5MwFhGA8UwWT7BEwAJZQZW/MIDwVD//8FdzwB/NIHAa3jAFMULdrBVVMBDVMD7wQdE2noMNQQAU4AAexcADIzwPQVUWHk2VMAWAAlf7UgQRv9J//7B88AVRA2m7Uo2OI9UVwgJAEyq9zUE//xTAQe46cD/g/9Hu0JE/8A+CMVUwTPC/8DBhxDFXMe+wf1VTFhEzQBVjHFrwXYZAMfO33VWQcA9WD4F/sC5GQAI0+IvBf38hE1i/f7BS6ANA8HSgMDDfv9MbBZE9eOawv/CB/3ChsHAwlnCakYPA+z0kMN1g8EHwlFKAaf7g2fCBv/AOEgJAK/8EAU3wbr8CxCgDIMEwsKHwMPBwP8Z1BMO3mtiwHT+mQHAwzXAgRgRDBZZwcK7cWLCwsDCBcNOIMAXEQkgmgf+woX+eMDBk8Ksc8JAESohWoQD1XEmJsMJEKovjAbCxoXDKhUQ+jJTwVaFbcGCwnzB6QQT0j1tfxMQ9oWaeIX+joDDwsIF+v5AEbJFiasP1e9G13L/fcDGwqENE6JLicH8h8IDwsG5/gMQskt6BQMTjVp6/wAAAAAAAAA=', '2015-11-03 10:55:59', null, null, null, null);
INSERT INTO `devcmds` VALUES ('36', '0455134200004', 'DATA DEL_USER PIN=10', '2015-11-03 11:04:12', null, null, null, null);
INSERT INTO `devcmds` VALUES ('37', '0455134200004', 'DATA USER PIN=11	Name=Andrea Sepotero	Pri=0		Card=[0000000000]	TZ=0000000000000000	Grp=1', '2015-11-03 11:04:12', null, null, null, null);
INSERT INTO `devcmds` VALUES ('38', '0455134200004', 'DATA FP PIN=11	FID=0	Size=1528	Valid=1	TMP=TTFTUzIxAAAEcnMECAUHCc7QAAAcc2kBAAAAhJ8prHIrAJcPcwDrAIl9ZAA8AIAPWgBDcpcPewBTAEMPSnJsAGoPqwCwAJR9PwCFAO0PbACPcpMPbACgAM8PgnKtAIoP5QB5ABZ9QgC9AOYPCQDEcicPagDPAMQOhXLRAIIO1AAXAKF9wgDTAJ0PagDYcpcPhwDiALkNDXPvAJsPYQA0AOt9eADzAAkO/QDzct0PjAD3AEwOJ3L/AF8O8AA7AKJ87gAFAZgOJQAOcxkOgQAhAcgNbHIiAe8OkADjAYp/pQAoAToNAQAvcx4LSQA1AZMNnHI5AaIMVwD+AeZ5owA8ATQMJgBDc6IMZwBKATkNqXJRAZcLNgCuDUthVA0uCgv5JPUmjlYaugBeEuYImnqz+u7ycX/G5e+cNAKn/IsPqIevl9IKsQZTCN5vjfG6CN8liYP7CVb56O7Jb9Z2a+eHr1r6iYGtiZAWhfCbhcoVMQlEgk4LWAiNgbWXJPixC1AK2fri8gtyvY889mF2TvSPBUv3lOpe8YJ2RIFR/PweGX6i7qr7hfOsDq4KFYbec095IRJBkcYMafZZ8s/9XYBNdQP7hfLwIpZsjeYoD/rrgIAh7flVZJnJ34wJ5uTS7kd/OQYXaUZd7HNIFb0bMIy96nn3GZcVj3iefodbDlqPIWwfqUYOlWZL9KYsWAGIVr8B5T0FcGMekgYAbsAPPLMHAIMFF/+ZBgQbCBPAQgsAmw0UNlllCwBdFdVMV42JCABOGwbuwE53ATsiAz0DxawqaP4KAC4w9Dv+M438wg8ANjcxKkCNXMFDBQBh+YOPdwGjShp0EMUvSZ8wwPxVYGTNAHMikZ3CewoAulUXssHA/mv+CMV0UvvDkIQFAEqqbXp+Aadxk8FuB8GEsw0ApHaQwAWIhPAGAK95GmQF/w1yQITtwSP9nA0E142QwIbDwQXAwI3ABwCtjhwH/nRnASOV4sBD5ET6jV3/gggAcFoJ+o1FwAYAg6pMwcDlGAAZu9o4Ojv7WkXAS8HD/JAGBJu/FsD//P/NANSxJsF4VxIAF8+mGG7Aw8TAwgdpxX4BqtqXwMBdw8UKDACD3oPDAcUys2/EGQAY4AX8+4w/L/z9/8E4wMWMw/7/wHcZxSDgpEU9/f/7/vjAxIzDwGVXAwDZ5VqyAgCC5YDDzwCPlxL/RVD+EMU886xSwPoewML3wRByJ/rcXTv+Of36s/5QwMDAA8U1/yXBFwDs/aIEwcSzUcDDwMLFB2nFPcMEAB7/ZLIDFFYDXsEDEPTHFvp2EeQJDCgE1eIKbkYGEGgf4jj6J3QRbCTt//k++RRiwCekwlzEOsjHJIkDEKgpNAUEFLoqHD0DEMPqK8B/EUsw4GL7PvzEjP/9/gQRBvVWXXQRkzSexcYFxBdiDTkrwcLFBML4t0/8gP5dGdX4PcjJ/nD/wMGoxcW3wlvCwP6VwxCVMKbHxagWEPREwo/+NzT9+/87wzaw/v35BBBog+kSdhFrS/0mAAAAAAAAAA==', '2015-11-03 11:04:12', null, null, null, null);
INSERT INTO `devcmds` VALUES ('39', '0421142600037', 'CLEAR DATA', '2015-11-17 09:13:39', '2015-11-17 09:13:39', '2015-11-17 09:13:40', '0', null);
INSERT INTO `devcmds` VALUES ('40', '0421142600037', 'DATA USER PIN=23	Name=sugun ige	Pri=null	Card=[0000000000]	Grp=1', '2015-11-17 09:38:04', '2015-11-17 09:39:20', '2015-11-17 09:39:21', '0', null);
INSERT INTO `devcmds` VALUES ('41', '0421142600037', 'CHECK', '2015-11-17 09:38:04', '2015-11-17 09:39:20', '2015-11-17 09:39:21', '0', null);
INSERT INTO `devcmds` VALUES ('42', '0421142600037', 'DATA USER PIN=15	Name=ADEDOKUN OPEYEMI	Pri=null	Card=[0000000000]	Grp=1', '2015-11-18 12:24:26', null, null, null, null);
INSERT INTO `devcmds` VALUES ('43', '0421142600037', 'CHECK', '2015-11-18 12:24:26', null, null, null, null);
INSERT INTO `devcmds` VALUES ('44', '0421142600037', 'DATA USER PIN=20	Name=taiwo Oko-osi	Pri=null	Card=[0000000000]	Grp=1', '2015-11-18 12:24:27', null, null, null, null);
INSERT INTO `devcmds` VALUES ('45', '0421142600037', 'CHECK', '2015-11-18 12:24:27', null, null, null, null);

-- ----------------------------
-- Table structure for `devlog`
-- ----------------------------
DROP TABLE IF EXISTS `devlog`;
CREATE TABLE `devlog` (
  `id` int(11) NOT NULL auto_increment,
  `SN_id` varchar(20) NOT NULL,
  `OP` varchar(8) NOT NULL,
  `Object` varchar(20) default NULL,
  `Cnt` int(11) NOT NULL,
  `ECnt` int(11) NOT NULL,
  `OpTime` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `devlog_SN_id` (`SN_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of devlog
-- ----------------------------
INSERT INTO `devlog` VALUES ('1', '0455134200004', 'USERDATA', '000000003  ', '3', '4', '2014-05-15 10:46:13');
INSERT INTO `devlog` VALUES ('2', '0455134200004', 'USERDATA', '000000004  ', '1', '7', '2014-05-15 10:46:13');
INSERT INTO `devlog` VALUES ('3', '0455134200004', 'TRANSACT', '1	2013-10-16 23:00:0', '24', '0', '2014-05-15 10:46:17');
INSERT INTO `devlog` VALUES ('4', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 10:46:17');
INSERT INTO `devlog` VALUES ('5', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 10:49:47');
INSERT INTO `devlog` VALUES ('6', '0455134200004', 'USERDATA', '000000003 John Mikel', '7', '0', '2014-05-15 10:59:05');
INSERT INTO `devlog` VALUES ('7', '0455134200004', 'USERDATA', '000000009 Jeki Surma', '8', '0', '2014-05-15 10:59:05');
INSERT INTO `devlog` VALUES ('8', '0455134200004', 'TRANSACT', '1	2013-10-16 23:00:0', '24', '0', '2014-05-15 10:59:09');
INSERT INTO `devlog` VALUES ('9', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 10:59:09');
INSERT INTO `devlog` VALUES ('10', '0455134200004', 'USERDATA', 'None', '40', '0', '2014-05-15 11:13:25');
INSERT INTO `devlog` VALUES ('11', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 11:14:26');
INSERT INTO `devlog` VALUES ('12', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 11:14:56');
INSERT INTO `devlog` VALUES ('13', '0455134200004', 'USERDATA', 'None', '10', '0', '2014-05-15 11:16:36');
INSERT INTO `devlog` VALUES ('14', '0455134200004', 'USERDATA', '000000398 Smith', '2', '0', '2014-05-15 14:02:44');
INSERT INTO `devlog` VALUES ('15', '0455134200004', 'TRANSACT', '', '0', '0', '2014-05-15 14:02:45');
INSERT INTO `devlog` VALUES ('16', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 14:02:50');
INSERT INTO `devlog` VALUES ('17', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 17:50:28');
INSERT INTO `devlog` VALUES ('18', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 17:50:29');
INSERT INTO `devlog` VALUES ('19', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 17:51:31');
INSERT INTO `devlog` VALUES ('20', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 17:51:31');
INSERT INTO `devlog` VALUES ('21', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-15 17:55:41');
INSERT INTO `devlog` VALUES ('22', '0455134200004', 'USERDATA', 'None', '9', '0', '2014-05-15 17:57:12');
INSERT INTO `devlog` VALUES ('23', '0455134200004', 'TRANSACT', '17	2014-05-16 00:57:', '1', '0', '2014-05-15 17:57:22');
INSERT INTO `devlog` VALUES ('24', '0455134200004', 'TRANSACT', '17	2014-05-16 00:58:', '1', '0', '2014-05-15 17:58:52');
INSERT INTO `devlog` VALUES ('25', '0455134200004', 'USERDATA', '000000017 Karlen Kir', '83', '0', '2014-05-15 18:00:02');
INSERT INTO `devlog` VALUES ('26', '0455134200004', 'USERDATA', 'None', '200', '0', '2014-05-15 18:00:13');
INSERT INTO `devlog` VALUES ('27', '0455134200004', 'USERDATA', 'None', '142', '0', '2014-05-15 18:00:23');
INSERT INTO `devlog` VALUES ('28', '0455134200004', 'TRANSACT', '', '0', '0', '2014-05-15 18:00:23');
INSERT INTO `devlog` VALUES ('29', '0455134200004', 'USERDATA', 'None', '142', '0', '2014-05-15 18:00:23');
INSERT INTO `devlog` VALUES ('30', '0455134200004', 'USERDATA', 'None', '152', '0', '2014-05-15 18:00:49');
INSERT INTO `devlog` VALUES ('31', '0455134200004', 'TRANSACT', '', '0', '0', '2014-05-15 18:00:49');
INSERT INTO `devlog` VALUES ('32', '0455134200004', 'TRANSACT', '17	2014-05-15 18:01:', '1', '0', '2014-05-15 18:01:53');
INSERT INTO `devlog` VALUES ('33', '0455134200004', 'USERDATA', '000000010 Andrea Sep', '4', '0', '2014-05-15 18:03:09');
INSERT INTO `devlog` VALUES ('34', '0455134200004', 'USERDATA', 'None', '4', '0', '2014-05-15 18:03:09');
INSERT INTO `devlog` VALUES ('35', '0455134200004', 'TRANSACT', '10	2014-05-15 18:03:', '1', '0', '2014-05-15 18:03:19');
INSERT INTO `devlog` VALUES ('36', '0455134200004', 'TRANSACT', '10	2014-05-15 18:08:', '1', '0', '2014-05-15 18:08:39');
INSERT INTO `devlog` VALUES ('37', '0455134200004', 'TRANSACT', '10	2014-05-15 18:36:', '1', '0', '2014-05-15 18:36:28');
INSERT INTO `devlog` VALUES ('38', '0455134200004', 'TRANSACT', '17	2014-05-15 18:36:', '1', '0', '2014-05-15 18:36:46');
INSERT INTO `devlog` VALUES ('39', '0455134200004', 'USERDATA', 'None', '68', '0', '2014-05-16 10:30:22');
INSERT INTO `devlog` VALUES ('40', '0455134200004', 'TRANSACT', '17	2014-05-16 10:30:', '1', '0', '2014-05-16 10:30:22');
INSERT INTO `devlog` VALUES ('41', '0455134200004', 'TRANSACT', '17	2014-05-16 10:30:', '1', '0', '2014-05-16 10:30:22');
INSERT INTO `devlog` VALUES ('42', '0455134200004', 'TRANSACT', '10	2014-05-16 10:30:', '1', '0', '2014-05-16 10:31:02');
INSERT INTO `devlog` VALUES ('43', '0455134200004', 'TRANSACT', '10	2014-05-16 10:32:', '1', '0', '2014-05-16 10:32:33');
INSERT INTO `devlog` VALUES ('44', '0455134200004', 'USERDATA', 'None', '2', '0', '2014-05-16 11:12:49');
INSERT INTO `devlog` VALUES ('45', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-16 11:14:29');
INSERT INTO `devlog` VALUES ('46', '0455134200004', 'USERDATA', 'None', '1', '0', '2014-05-16 11:25:43');
INSERT INTO `devlog` VALUES ('47', '0421142600037', 'TRANSACT', '8	2015-11-16 20:22:1', '2', '0', '2015-11-17 08:53:42');
INSERT INTO `devlog` VALUES ('48', '0421142600037', 'USERDATA', '000000023 sugun ige', '1', '0', '2015-11-17 09:39:29');

-- ----------------------------
-- Table structure for `django_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL auto_increment,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) default NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `django_admin_log_user_id` (`user_id`),
  KEY `django_admin_log_content_type_id` (`content_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for `django_content_type`
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES ('1', 'session', 'sessions', 'session');
INSERT INTO `django_content_type` VALUES ('2', 'permission', 'auth', 'permission');
INSERT INTO `django_content_type` VALUES ('3', 'group', 'auth', 'group');
INSERT INTO `django_content_type` VALUES ('4', 'user', 'auth', 'user');
INSERT INTO `django_content_type` VALUES ('5', 'message', 'auth', 'message');
INSERT INTO `django_content_type` VALUES ('6', 'content type', 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES ('7', 'log entry', 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES ('8', 'department', 'iclock', 'department');
INSERT INTO `django_content_type` VALUES ('9', 'device', 'iclock', 'iclock');
INSERT INTO `django_content_type` VALUES ('10', 'admin granted department', 'iclock', 'deptadmin');
INSERT INTO `django_content_type` VALUES ('11', 'employee', 'iclock', 'employee');
INSERT INTO `django_content_type` VALUES ('12', 'fingerprint', 'iclock', 'fptemp');
INSERT INTO `django_content_type` VALUES ('13', 'transaction', 'iclock', 'transaction');
INSERT INTO `django_content_type` VALUES ('14', 'device operation log', 'iclock', 'oplog');
INSERT INTO `django_content_type` VALUES ('15', 'data from device', 'iclock', 'devlog');
INSERT INTO `django_content_type` VALUES ('16', 'command to device', 'iclock', 'devcmds');
INSERT INTO `django_content_type` VALUES ('17', 'public information', 'iclock', 'messages');
INSERT INTO `django_content_type` VALUES ('18', 'information subscription', 'iclock', 'iclockmsg');
INSERT INTO `django_content_type` VALUES ('19', 'administration log', 'iclock', 'adminlog');

-- ----------------------------
-- Table structure for `django_session`
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY  (`session_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_session
-- ----------------------------

-- ----------------------------
-- Table structure for `employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `user_id` int(10) unsigned NOT NULL auto_increment,
  `company_id` int(10) unsigned default NULL,
  `first_name` varchar(150) default NULL,
  `last_name` varchar(150) default NULL,
  `email` varchar(150) default NULL,
  `personal_phone` varchar(45) default NULL,
  `work_phone` varchar(45) default NULL,
  `address` varchar(150) default NULL,
  `city` varchar(45) default NULL,
  `state` varchar(45) default NULL,
  `country_id` int(10) unsigned default NULL,
  `nationality_id` int(10) unsigned default NULL,
  `marital_status_id` int(10) unsigned default NULL,
  `gender_id` int(10) unsigned default NULL,
  `date_of_birth` datetime default NULL,
  `date_created` datetime default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employees
-- ----------------------------

-- ----------------------------
-- Table structure for `employee_bank_details`
-- ----------------------------
DROP TABLE IF EXISTS `employee_bank_details`;
CREATE TABLE `employee_bank_details` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `bank_id` int(10) unsigned default NULL,
  `account_type_id` int(10) unsigned default NULL,
  `account_name` varchar(150) default NULL,
  `account_number` varchar(45) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employee_bank_details
-- ----------------------------

-- ----------------------------
-- Table structure for `employee_kin_details`
-- ----------------------------
DROP TABLE IF EXISTS `employee_kin_details`;
CREATE TABLE `employee_kin_details` (
  `employee_kin_detail_id` int(10) unsigned NOT NULL auto_increment,
  `employee_id` int(10) unsigned default NULL,
  `kin_first_name` varchar(150) default NULL,
  `kin_last_name` varchar(150) default NULL,
  `relationship_id` int(10) unsigned default NULL,
  `kin_email` varchar(45) default NULL,
  `kin_phone` varchar(45) default NULL,
  `kin_address` varchar(150) default NULL,
  PRIMARY KEY  (`employee_kin_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employee_kin_details
-- ----------------------------

-- ----------------------------
-- Table structure for `genders`
-- ----------------------------
DROP TABLE IF EXISTS `genders`;
CREATE TABLE `genders` (
  `gender_id` int(10) unsigned NOT NULL auto_increment,
  `gender` varchar(10) default NULL,
  PRIMARY KEY  (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of genders
-- ----------------------------

-- ----------------------------
-- Table structure for `iclock`
-- ----------------------------
DROP TABLE IF EXISTS `iclock`;
CREATE TABLE `iclock` (
  `SN` varchar(20) NOT NULL,
  `State` int(11) NOT NULL,
  `LastActivity` datetime default NULL,
  `TransTimes` varchar(50) default NULL,
  `TransInterval` int(11) NOT NULL,
  `LogStamp` varchar(20) default NULL,
  `OpLogStamp` varchar(20) default NULL,
  `PhotoStamp` varchar(20) default NULL,
  `Alias` varchar(20) NOT NULL,
  `DeptID` int(11) default NULL,
  `UpdateDB` varchar(10) NOT NULL,
  `Style` varchar(20) default NULL,
  `FWVersion` varchar(30) default NULL,
  `FPCount` int(11) default NULL,
  `TransactionCount` int(11) default NULL,
  `UserCount` int(11) default NULL,
  `MainTime` varchar(20) default NULL,
  `MaxFingerCount` int(11) default NULL,
  `MaxAttLogCount` int(11) default NULL,
  `DeviceName` varchar(30) default NULL,
  `AlgVer` varchar(30) default NULL,
  `FlashSize` varchar(10) default NULL,
  `FreeFlashSize` varchar(10) default NULL,
  `Language` varchar(30) default NULL,
  `VOLUME` varchar(10) default NULL,
  `DtFmt` varchar(10) default NULL,
  `IPAddress` varchar(20) default NULL,
  `IsTFT` varchar(5) default NULL,
  `Platform` varchar(20) default NULL,
  `Brightness` varchar(5) default NULL,
  `BackupDev` varchar(30) default NULL,
  `OEMVendor` varchar(30) default NULL,
  `City` varchar(50) default NULL,
  `AccFun` smallint(6) NOT NULL,
  `TZAdj` smallint(6) NOT NULL,
  `DelTag` smallint(6) NOT NULL,
  `FPVersion` varchar(10) default NULL,
  `PushVersion` varchar(10) default NULL,
  PRIMARY KEY  (`SN`),
  KEY `iclock_DeptID` (`DeptID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of iclock
-- ----------------------------
INSERT INTO `iclock` VALUES ('0455134200004', '1', '2014-05-16 11:28:05', '00:00;14:05', '1', '462018741', '462021871', '', '192.168.1.201', '1', '1111111100', 'F7', 'Ver 6.5.4(build 130)', '2', '35', '4', '1970-01-01 00:00:09', '3000', '100000', '  ', 'BioFinger VX', '105472', '72672', '69', '67', '0', '192.168.1.201', '1', 'ZEM510_TFT', '80', '', '  ', '', '0', '1', '0', '10', '0.0');
INSERT INTO `iclock` VALUES ('0577363558474', '1', '2015-10-28 07:27:04', '00:00; 12:10', '1', '86473643', '987466', null, '192.168.1.214', '1', '111001111', 'F7', 'Ver 5.0', '3', '250', '5', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', '0', null, null);
INSERT INTO `iclock` VALUES ('0421142600037', '1', '2015-11-17 10:11:41', '00:00;14:05', '1', '9999', '9999', null, '192.168.1.201', '1', '1111111100', 'F7', 'Ver 1.0.0-20140512', '0', '0', '1', null, null, null, null, null, null, null, null, null, null, '192.168.1.201', null, null, null, null, null, null, '0', '8', '0', '10', '0.0');

-- ----------------------------
-- Table structure for `iclock_adminlog`
-- ----------------------------
DROP TABLE IF EXISTS `iclock_adminlog`;
CREATE TABLE `iclock_adminlog` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `User_id` int(11) default NULL,
  `model` varchar(40) default NULL,
  `action` varchar(40) NOT NULL,
  `object` varchar(40) default NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `iclock_adminlog_User_id` (`User_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of iclock_adminlog
-- ----------------------------
INSERT INTO `iclock_adminlog` VALUES ('1', '2014-05-15 10:47:08', '1', null, 'LOGOUT', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('2', '2014-05-15 10:47:15', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('3', '2014-05-15 17:59:39', '1', 'iclock', 'Modify', '0455134200004(192.168.1.201)', '1');
INSERT INTO `iclock_adminlog` VALUES ('4', '2014-05-15 18:00:34', '1', 'iclock', 'Modify', '0455134200004(192.168.1.201)', '1');
INSERT INTO `iclock_adminlog` VALUES ('5', '2014-05-16 10:34:10', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('6', '2014-05-16 10:38:02', '1', 'employee', 'del', '11', '1');
INSERT INTO `iclock_adminlog` VALUES ('7', '2014-05-16 16:29:03', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('8', '2014-05-16 16:30:27', '1', null, 'LOGOUT', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('9', '2015-04-16 23:04:30', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('10', '2015-10-07 17:45:01', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('11', '2015-10-10 13:58:01', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('12', '2015-10-10 13:59:14', '1', 'employee', 'Modify', '000000178 Karlen Kirakosyan', '1');
INSERT INTO `iclock_adminlog` VALUES ('13', '2015-10-13 08:47:01', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('14', '2015-11-03 10:53:25', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('15', '2015-11-03 10:55:59', '1', 'employee', 'Modify', '000000222 Karlen Kirakosyan', '1');
INSERT INTO `iclock_adminlog` VALUES ('16', '2015-11-03 11:04:12', '1', 'employee', 'Modify', '000000011 Andrea Sepotero', '1');
INSERT INTO `iclock_adminlog` VALUES ('17', '2015-11-04 00:53:11', '1', 'employee', 'Modify', '000000201', '1');
INSERT INTO `iclock_adminlog` VALUES ('18', '2015-11-17 08:43:39', '1', 'None', 'LOGIN', '127.0.0.1', '1');
INSERT INTO `iclock_adminlog` VALUES ('19', '2015-11-17 09:08:35', '1', 'iclock', 'clearlog', '0421142600037', '1');
INSERT INTO `iclock_adminlog` VALUES ('20', '2015-11-17 09:13:39', '1', 'iclock', 'cleardata', '0421142600037', '1');

-- ----------------------------
-- Table structure for `iclock_deptadmin`
-- ----------------------------
DROP TABLE IF EXISTS `iclock_deptadmin`;
CREATE TABLE `iclock_deptadmin` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `iclock_deptadmin_user_id` (`user_id`),
  KEY `iclock_deptadmin_dept_id` (`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of iclock_deptadmin
-- ----------------------------

-- ----------------------------
-- Table structure for `iclock_iclockmsg`
-- ----------------------------
DROP TABLE IF EXISTS `iclock_iclockmsg`;
CREATE TABLE `iclock_iclockmsg` (
  `id` int(11) NOT NULL auto_increment,
  `SN_id` varchar(20) NOT NULL,
  `MsgType` int(11) NOT NULL,
  `StartTime` datetime NOT NULL,
  `EndTime` datetime default NULL,
  `MsgParam` varchar(32) default NULL,
  `MsgContent` varchar(200) default NULL,
  `LastTime` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `iclock_iclockmsg_SN_id` (`SN_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of iclock_iclockmsg
-- ----------------------------

-- ----------------------------
-- Table structure for `iclock_messages`
-- ----------------------------
DROP TABLE IF EXISTS `iclock_messages`;
CREATE TABLE `iclock_messages` (
  `id` int(11) NOT NULL auto_increment,
  `MsgType` int(11) NOT NULL,
  `StartTime` datetime NOT NULL,
  `EndTime` datetime default NULL,
  `MsgContent` longtext,
  `MsgImage` varchar(64) default NULL,
  `DeptID_id` int(11) default NULL,
  `MsgParam` varchar(32) default NULL,
  PRIMARY KEY  (`id`),
  KEY `iclock_messages_DeptID_id` (`DeptID_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of iclock_messages
-- ----------------------------

-- ----------------------------
-- Table structure for `iclock_oplog`
-- ----------------------------
DROP TABLE IF EXISTS `iclock_oplog`;
CREATE TABLE `iclock_oplog` (
  `id` int(11) NOT NULL auto_increment,
  `SN` varchar(20) default NULL,
  `admin` int(11) NOT NULL,
  `OP` smallint(6) NOT NULL,
  `OPTime` datetime NOT NULL,
  `Object` int(11) default NULL,
  `Param1` int(11) default NULL,
  `Param2` int(11) default NULL,
  `Param3` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `SN` (`SN`,`OPTime`),
  KEY `iclock_oplog_SN` (`SN`)
) ENGINE=MyISAM AUTO_INCREMENT=363 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of iclock_oplog
-- ----------------------------
INSERT INTO `iclock_oplog` VALUES ('1', '0455134200004', '0', '0', '2014-05-15 17:58:56', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('2', '0455134200004', '2', '4', '2014-05-15 18:09:41', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('3', '0455134200004', '2', '9', '2014-05-15 18:10:21', '4', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('4', '0455134200004', '2', '9', '2014-05-15 18:10:39', '5', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('5', '0455134200004', '2', '9', '2014-05-15 18:10:46', '6', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('6', '0455134200004', '2', '9', '2014-05-15 18:10:54', '7', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('7', '0455134200004', '2', '9', '2014-05-15 18:10:55', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('8', '0455134200004', '2', '9', '2014-05-15 18:11:05', '8', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('9', '0455134200004', '2', '9', '2014-05-15 18:11:06', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('10', '0455134200004', '2', '9', '2014-05-15 18:11:07', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('11', '0455134200004', '2', '9', '2014-05-15 18:11:11', '9', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('12', '0455134200004', '2', '9', '2014-05-15 18:11:12', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('13', '0455134200004', '2', '9', '2014-05-15 18:11:13', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('14', '0455134200004', '2', '9', '2014-05-15 18:11:14', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('15', '0455134200004', '2', '9', '2014-05-15 18:11:19', '10', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('16', '0455134200004', '2', '9', '2014-05-15 18:11:20', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('17', '0455134200004', '2', '9', '2014-05-15 18:11:21', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('18', '0455134200004', '2', '9', '2014-05-15 18:11:22', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('19', '0455134200004', '2', '9', '2014-05-15 18:11:23', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('20', '0455134200004', '2', '9', '2014-05-15 18:11:25', '11', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('21', '0455134200004', '2', '9', '2014-05-15 18:11:26', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('22', '0455134200004', '2', '9', '2014-05-15 18:11:27', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('23', '0455134200004', '2', '9', '2014-05-15 18:11:28', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('24', '0455134200004', '2', '9', '2014-05-15 18:11:29', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('25', '0455134200004', '2', '9', '2014-05-15 18:11:30', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('26', '0455134200004', '2', '9', '2014-05-15 18:11:37', '1', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('27', '0455134200004', '2', '9', '2014-05-15 18:11:38', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('28', '0455134200004', '2', '9', '2014-05-15 18:11:39', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('29', '0455134200004', '2', '9', '2014-05-15 18:11:40', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('30', '0455134200004', '2', '9', '2014-05-15 18:11:41', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('31', '0455134200004', '2', '9', '2014-05-15 18:11:42', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('32', '0455134200004', '2', '9', '2014-05-15 18:11:43', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('33', '0455134200004', '2', '7', '2014-05-15 18:13:11', '398', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('34', '0455134200004', '2', '71', '2014-05-15 18:13:12', '3', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('35', '0455134200004', '2', '9', '2014-05-15 18:13:13', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('36', '0455134200004', '2', '9', '2014-05-15 18:13:14', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('37', '0455134200004', '2', '9', '2014-05-15 18:13:15', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('38', '0455134200004', '2', '9', '2014-05-15 18:13:16', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('39', '0455134200004', '2', '9', '2014-05-15 18:13:17', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('40', '0455134200004', '2', '9', '2014-05-15 18:13:18', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('41', '0455134200004', '3', '4', '2014-05-15 18:13:30', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('42', '0455134200004', '2', '4', '2014-05-15 18:14:29', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('43', '0455134200004', '2', '4', '2014-05-15 18:15:05', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('44', '0455134200004', '2', '30', '2014-05-15 18:15:52', '2000', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('45', '0455134200004', '2', '9', '2014-05-15 18:16:23', '2', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('46', '0455134200004', '2', '9', '2014-05-15 18:16:24', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('47', '0455134200004', '2', '9', '2014-05-15 18:16:25', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('48', '0455134200004', '2', '9', '2014-05-15 18:16:26', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('49', '0455134200004', '2', '9', '2014-05-15 18:16:27', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('50', '0455134200004', '2', '9', '2014-05-15 18:16:28', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('51', '0455134200004', '2', '9', '2014-05-15 18:16:29', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('52', '0455134200004', '0', '0', '2014-05-15 21:02:38', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('53', '0455134200004', '0', '0', '2014-05-16 00:50:16', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('54', '0455134200004', '1', '4', '2014-05-16 00:50:56', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('55', '0455134200004', '1', '4', '2014-05-16 00:54:35', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('56', '0455134200004', '1', '4', '2014-05-16 00:56:05', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('57', '0455134200004', '1', '6', '2014-05-16 00:56:51', '17', '0', '0', '836');
INSERT INTO `iclock_oplog` VALUES ('58', '0455134200004', '1', '9', '2014-05-16 00:56:52', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('59', '0455134200004', '1', '9', '2014-05-16 00:56:53', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('60', '0455134200004', '1', '9', '2014-05-16 00:56:54', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('61', '0455134200004', '1', '9', '2014-05-16 00:56:55', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('62', '0455134200004', '1', '9', '2014-05-16 00:56:56', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('63', '0455134200004', '0', '13', '2013-10-14 18:59:42', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('64', '0455134200004', '0', '0', '2013-10-14 19:00:03', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('65', '0455134200004', '0', '4', '2013-10-14 19:00:28', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('66', '0455134200004', '0', '4', '2013-10-14 19:01:24', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('67', '0455134200004', '0', '0', '2013-10-16 22:41:03', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('68', '0455134200004', '0', '1', '2013-10-16 22:41:45', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('69', '0455134200004', '0', '0', '2013-10-16 22:42:03', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('70', '0455134200004', '0', '4', '2013-10-16 22:42:48', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('71', '0455134200004', '0', '5', '2013-10-16 22:43:09', '132', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('72', '0455134200004', '0', '5', '2013-10-16 22:43:10', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('73', '0455134200004', '0', '5', '2013-10-16 22:43:11', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('74', '0455134200004', '0', '5', '2013-10-16 22:43:12', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('75', '0455134200004', '0', '5', '2013-10-16 22:43:13', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('76', '0455134200004', '0', '5', '2013-10-16 22:43:14', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('77', '0455134200004', '0', '5', '2013-10-16 22:43:15', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('78', '0455134200004', '0', '5', '2013-10-16 22:43:16', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('79', '0455134200004', '0', '5', '2013-10-16 22:43:17', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('80', '0455134200004', '0', '5', '2013-10-16 22:43:18', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('81', '0455134200004', '0', '5', '2013-10-16 22:43:19', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('82', '0455134200004', '0', '5', '2013-10-16 22:43:55', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('83', '0455134200004', '0', '5', '2013-10-16 22:43:56', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('84', '0455134200004', '0', '5', '2013-10-16 22:43:57', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('85', '0455134200004', '0', '5', '2013-10-16 22:43:58', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('86', '0455134200004', '0', '5', '2013-10-16 22:43:59', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('87', '0455134200004', '0', '5', '2013-10-16 22:44:00', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('88', '0455134200004', '0', '5', '2013-10-16 22:44:01', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('89', '0455134200004', '0', '5', '2013-10-16 22:44:02', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('90', '0455134200004', '0', '5', '2013-10-16 22:44:03', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('91', '0455134200004', '0', '5', '2013-10-16 22:44:04', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('92', '0455134200004', '0', '5', '2013-10-16 22:44:05', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('93', '0455134200004', '0', '1', '2013-10-16 22:44:10', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('94', '0455134200004', '0', '0', '2013-10-16 22:44:30', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('95', '0455134200004', '0', '1', '2013-10-16 22:48:43', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('96', '0455134200004', '0', '0', '2013-10-16 22:54:03', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('97', '0455134200004', '0', '4', '2013-10-16 22:57:04', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('98', '0455134200004', '0', '4', '2013-10-16 22:59:49', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('99', '0455134200004', '0', '30', '2013-10-16 23:00:04', '2000', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('100', '0455134200004', '0', '6', '2013-10-16 23:00:05', '2000', '0', '0', '1258');
INSERT INTO `iclock_oplog` VALUES ('101', '0455134200004', '0', '1', '2013-10-16 23:07:35', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('102', '0455134200004', '0', '0', '2013-10-16 23:07:53', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('103', '0455134200004', '0', '4', '2013-10-16 23:13:24', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('104', '0455134200004', '0', '1', '2013-10-16 23:18:05', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('105', '0455134200004', '0', '0', '2014-04-06 15:28:54', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('106', '0455134200004', '0', '4', '2014-04-06 15:29:10', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('107', '0455134200004', '0', '4', '2014-04-06 15:30:19', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('108', '0455134200004', '0', '4', '2014-04-06 15:32:15', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('109', '0455134200004', '0', '4', '2014-04-06 15:34:09', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('110', '0455134200004', '0', '5', '2014-04-06 15:34:27', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('111', '0455134200004', '0', '5', '2014-04-06 15:34:28', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('112', '0455134200004', '0', '5', '2014-04-06 15:34:29', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('113', '0455134200004', '0', '5', '2014-04-06 15:34:30', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('114', '0455134200004', '0', '5', '2014-04-06 15:34:31', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('115', '0455134200004', '0', '5', '2014-04-06 15:34:32', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('116', '0455134200004', '0', '5', '2014-04-06 15:34:33', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('117', '0455134200004', '0', '5', '2014-04-06 15:34:34', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('118', '0455134200004', '0', '5', '2014-04-06 15:34:35', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('119', '0455134200004', '0', '5', '2014-04-06 15:34:36', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('120', '0455134200004', '0', '5', '2014-04-06 15:34:37', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('121', '0455134200004', '0', '1', '2014-04-06 15:38:24', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('122', '0455134200004', '0', '0', '2014-05-12 16:28:32', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('123', '0455134200004', '0', '4', '2014-05-12 16:28:50', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('124', '0455134200004', '0', '4', '2014-05-12 16:29:29', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('125', '0455134200004', '0', '30', '2014-05-12 16:30:34', '17', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('126', '0455134200004', '0', '6', '2014-05-12 16:30:35', '17', '0', '0', '886');
INSERT INTO `iclock_oplog` VALUES ('127', '0455134200004', '2', '4', '2014-05-12 16:31:17', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('128', '0455134200004', '2', '21', '2014-05-12 09:31:55', '114', '4', '12', '279');
INSERT INTO `iclock_oplog` VALUES ('129', '0455134200004', '2', '5', '2014-05-12 09:31:56', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('130', '0455134200004', '2', '5', '2014-05-12 09:31:57', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('131', '0455134200004', '2', '5', '2014-05-12 09:31:58', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('132', '0455134200004', '2', '5', '2014-05-12 09:31:59', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('133', '0455134200004', '2', '5', '2014-05-12 09:32:00', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('134', '0455134200004', '2', '5', '2014-05-12 09:32:01', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('135', '0455134200004', '2', '5', '2014-05-12 09:32:02', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('136', '0455134200004', '2', '5', '2014-05-12 09:32:03', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('137', '0455134200004', '2', '5', '2014-05-12 09:32:04', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('138', '0455134200004', '2', '5', '2014-05-12 09:32:05', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('139', '0455134200004', '2', '4', '2014-05-12 09:32:45', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('140', '0455134200004', '2', '7', '2014-05-12 09:33:45', '17', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('141', '0455134200004', '2', '9', '2014-05-12 09:33:46', '0', '65532', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('142', '0455134200004', '2', '9', '2014-05-12 09:33:47', '0', '65532', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('143', '0455134200004', '2', '9', '2014-05-12 09:33:48', '0', '65532', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('144', '0455134200004', '2', '9', '2014-05-12 09:33:49', '0', '65532', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('145', '0455134200004', '2', '9', '2014-05-12 09:33:50', '0', '65532', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('146', '0455134200004', '2', '9', '2014-05-12 09:33:51', '0', '65532', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('147', '0455134200004', '2', '4', '2014-05-12 09:39:52', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('148', '0455134200004', '2', '4', '2014-05-12 09:44:16', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('149', '0455134200004', '2', '4', '2014-05-12 09:45:50', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('150', '0455134200004', '2', '5', '2014-05-12 09:46:41', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('151', '0455134200004', '2', '5', '2014-05-12 09:46:42', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('152', '0455134200004', '2', '5', '2014-05-12 09:46:43', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('153', '0455134200004', '2', '5', '2014-05-12 09:46:44', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('154', '0455134200004', '2', '5', '2014-05-12 09:46:45', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('155', '0455134200004', '2', '5', '2014-05-12 09:46:46', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('156', '0455134200004', '2', '5', '2014-05-12 09:46:47', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('157', '0455134200004', '2', '5', '2014-05-12 09:46:48', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('158', '0455134200004', '2', '5', '2014-05-12 09:46:49', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('159', '0455134200004', '2', '5', '2014-05-12 09:46:50', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('160', '0455134200004', '2', '5', '2014-05-12 09:46:51', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('161', '0455134200004', '2', '5', '2014-05-12 09:47:03', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('162', '0455134200004', '2', '5', '2014-05-12 09:47:04', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('163', '0455134200004', '2', '5', '2014-05-12 09:47:05', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('164', '0455134200004', '2', '5', '2014-05-12 09:47:06', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('165', '0455134200004', '2', '5', '2014-05-12 09:47:07', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('166', '0455134200004', '2', '5', '2014-05-12 09:47:08', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('167', '0455134200004', '2', '5', '2014-05-12 09:47:09', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('168', '0455134200004', '2', '5', '2014-05-12 09:47:10', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('169', '0455134200004', '2', '5', '2014-05-12 09:47:11', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('170', '0455134200004', '2', '5', '2014-05-12 09:47:12', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('171', '0455134200004', '2', '5', '2014-05-12 09:47:13', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('172', '0455134200004', '2', '4', '2014-05-12 08:54:53', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('173', '0455134200004', '2', '4', '2014-05-12 08:57:09', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('174', '0455134200004', '0', '1', '2014-05-12 09:08:35', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('175', '0455134200004', '0', '0', '2014-05-12 09:27:03', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('176', '0455134200004', '2', '4', '2014-05-12 09:27:14', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('177', '0455134200004', '2', '5', '2014-05-12 09:27:36', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('178', '0455134200004', '2', '5', '2014-05-12 09:27:37', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('179', '0455134200004', '2', '5', '2014-05-12 09:27:38', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('180', '0455134200004', '2', '5', '2014-05-12 09:27:39', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('181', '0455134200004', '2', '5', '2014-05-12 09:27:40', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('182', '0455134200004', '2', '5', '2014-05-12 09:27:41', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('183', '0455134200004', '2', '5', '2014-05-12 09:27:42', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('184', '0455134200004', '2', '5', '2014-05-12 09:27:43', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('185', '0455134200004', '2', '5', '2014-05-12 09:27:44', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('186', '0455134200004', '2', '5', '2014-05-12 09:27:45', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('187', '0455134200004', '2', '5', '2014-05-12 09:27:46', '349', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('188', '0455134200004', '2', '4', '2014-05-12 09:44:41', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('189', '0455134200004', '2', '5', '2014-05-12 09:45:05', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('190', '0455134200004', '2', '5', '2014-05-12 09:45:06', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('191', '0455134200004', '2', '5', '2014-05-12 09:45:07', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('192', '0455134200004', '2', '5', '2014-05-12 09:45:08', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('193', '0455134200004', '2', '5', '2014-05-12 09:45:09', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('194', '0455134200004', '2', '5', '2014-05-12 09:45:10', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('195', '0455134200004', '2', '5', '2014-05-12 09:45:11', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('196', '0455134200004', '2', '5', '2014-05-12 09:45:12', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('197', '0455134200004', '2', '5', '2014-05-12 09:45:13', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('198', '0455134200004', '2', '5', '2014-05-12 09:45:14', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('199', '0455134200004', '2', '5', '2014-05-12 09:45:15', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('200', '0455134200004', '2', '4', '2014-05-12 10:16:35', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('201', '0455134200004', '2', '5', '2014-05-12 10:17:09', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('202', '0455134200004', '2', '5', '2014-05-12 10:17:10', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('203', '0455134200004', '2', '5', '2014-05-12 10:17:11', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('204', '0455134200004', '2', '5', '2014-05-12 10:17:12', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('205', '0455134200004', '2', '5', '2014-05-12 10:17:13', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('206', '0455134200004', '2', '5', '2014-05-12 10:17:14', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('207', '0455134200004', '2', '5', '2014-05-12 10:17:15', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('208', '0455134200004', '2', '5', '2014-05-12 10:17:16', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('209', '0455134200004', '2', '5', '2014-05-12 10:17:17', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('210', '0455134200004', '2', '5', '2014-05-12 10:17:18', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('211', '0455134200004', '2', '5', '2014-05-12 10:17:19', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('212', '0455134200004', '2', '4', '2014-05-12 10:34:49', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('213', '0455134200004', '2', '5', '2014-05-12 10:35:06', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('214', '0455134200004', '2', '5', '2014-05-12 10:35:07', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('215', '0455134200004', '2', '5', '2014-05-12 10:35:08', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('216', '0455134200004', '2', '5', '2014-05-12 10:35:09', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('217', '0455134200004', '2', '5', '2014-05-12 10:35:10', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('218', '0455134200004', '2', '5', '2014-05-12 10:35:11', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('219', '0455134200004', '2', '5', '2014-05-12 10:35:12', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('220', '0455134200004', '2', '5', '2014-05-12 10:35:13', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('221', '0455134200004', '2', '5', '2014-05-12 10:35:14', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('222', '0455134200004', '2', '5', '2014-05-12 10:35:15', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('223', '0455134200004', '2', '5', '2014-05-12 10:35:16', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('224', '0455134200004', '0', '0', '2014-05-12 10:47:12', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('225', '0455134200004', '0', '1', '2014-05-12 10:54:56', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('226', '0455134200004', '0', '0', '2014-05-12 11:25:04', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('227', '0455134200004', '2', '4', '2014-05-12 11:26:52', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('228', '0455134200004', '2', '5', '2014-05-12 11:27:21', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('229', '0455134200004', '2', '5', '2014-05-12 11:27:22', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('230', '0455134200004', '2', '5', '2014-05-12 11:27:23', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('231', '0455134200004', '2', '5', '2014-05-12 11:27:24', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('232', '0455134200004', '2', '5', '2014-05-12 11:27:25', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('233', '0455134200004', '2', '5', '2014-05-12 11:27:26', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('234', '0455134200004', '2', '5', '2014-05-12 11:27:27', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('235', '0455134200004', '2', '5', '2014-05-12 11:27:28', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('236', '0455134200004', '2', '5', '2014-05-12 11:27:29', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('237', '0455134200004', '2', '5', '2014-05-12 11:27:30', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('238', '0455134200004', '2', '5', '2014-05-12 11:27:31', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('239', '0455134200004', '2', '4', '2014-05-12 11:36:07', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('240', '0455134200004', '2', '30', '2014-05-12 11:36:43', '10', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('241', '0455134200004', '2', '6', '2014-05-12 11:36:44', '10', '0', '0', '1222');
INSERT INTO `iclock_oplog` VALUES ('242', '0455134200004', '2', '4', '2014-05-12 11:42:27', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('243', '0455134200004', '2', '4', '2014-05-12 11:49:21', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('244', '0455134200004', '0', '0', '2014-05-12 11:56:17', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('245', '0455134200004', '2', '4', '2014-05-12 11:57:27', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('246', '0455134200004', '2', '4', '2014-05-12 11:59:17', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('247', '0455134200004', '2', '4', '2014-05-12 12:00:49', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('248', '0455134200004', '0', '0', '2014-05-12 12:03:03', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('249', '0455134200004', '2', '4', '2014-05-12 12:06:14', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('250', '0455134200004', '2', '4', '2014-05-12 12:06:54', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('251', '0455134200004', '0', '0', '2014-05-12 12:29:26', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('252', '0455134200004', '0', '0', '2014-05-13 08:22:10', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('253', '0455134200004', '2', '4', '2014-05-13 08:22:27', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('254', '0455134200004', '2', '5', '2014-05-13 08:23:01', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('255', '0455134200004', '2', '5', '2014-05-13 08:23:02', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('256', '0455134200004', '2', '5', '2014-05-13 08:23:03', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('257', '0455134200004', '2', '5', '2014-05-13 08:23:04', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('258', '0455134200004', '2', '5', '2014-05-13 08:23:05', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('259', '0455134200004', '2', '5', '2014-05-13 08:23:06', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('260', '0455134200004', '2', '5', '2014-05-13 08:23:07', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('261', '0455134200004', '2', '5', '2014-05-13 08:23:08', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('262', '0455134200004', '2', '5', '2014-05-13 08:23:09', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('263', '0455134200004', '2', '5', '2014-05-13 08:23:10', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('264', '0455134200004', '2', '5', '2014-05-13 08:23:11', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('265', '0455134200004', '2', '4', '2014-05-13 08:51:24', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('266', '0455134200004', '2', '5', '2014-05-13 08:51:56', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('267', '0455134200004', '2', '5', '2014-05-13 08:51:57', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('268', '0455134200004', '2', '5', '2014-05-13 08:51:58', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('269', '0455134200004', '2', '5', '2014-05-13 08:51:59', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('270', '0455134200004', '2', '5', '2014-05-13 08:52:00', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('271', '0455134200004', '2', '5', '2014-05-13 08:52:01', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('272', '0455134200004', '2', '5', '2014-05-13 08:52:02', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('273', '0455134200004', '2', '5', '2014-05-13 08:52:03', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('274', '0455134200004', '2', '5', '2014-05-13 08:52:04', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('275', '0455134200004', '2', '5', '2014-05-13 08:52:05', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('276', '0455134200004', '2', '5', '2014-05-13 08:52:06', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('277', '0455134200004', '2', '4', '2014-05-13 09:58:27', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('278', '0455134200004', '2', '5', '2014-05-13 09:58:49', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('279', '0455134200004', '2', '5', '2014-05-13 09:58:50', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('280', '0455134200004', '2', '5', '2014-05-13 09:58:51', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('281', '0455134200004', '2', '5', '2014-05-13 09:58:52', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('282', '0455134200004', '2', '5', '2014-05-13 09:58:53', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('283', '0455134200004', '2', '5', '2014-05-13 09:58:54', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('284', '0455134200004', '2', '5', '2014-05-13 09:58:55', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('285', '0455134200004', '2', '5', '2014-05-13 09:58:56', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('286', '0455134200004', '2', '5', '2014-05-13 09:58:57', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('287', '0455134200004', '2', '5', '2014-05-13 09:58:58', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('288', '0455134200004', '2', '5', '2014-05-13 09:58:59', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('289', '0455134200004', '0', '1', '2014-05-13 10:11:41', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('290', '0455134200004', '0', '0', '2014-05-13 10:14:07', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('291', '0455134200004', '2', '4', '2014-05-13 10:23:15', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('292', '0455134200004', '2', '5', '2014-05-13 10:23:59', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('293', '0455134200004', '2', '5', '2014-05-13 10:24:00', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('294', '0455134200004', '2', '5', '2014-05-13 10:24:01', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('295', '0455134200004', '2', '5', '2014-05-13 10:24:02', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('296', '0455134200004', '2', '5', '2014-05-13 10:24:03', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('297', '0455134200004', '2', '5', '2014-05-13 10:24:04', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('298', '0455134200004', '2', '5', '2014-05-13 10:24:05', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('299', '0455134200004', '2', '5', '2014-05-13 10:24:06', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('300', '0455134200004', '2', '5', '2014-05-13 10:24:07', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('301', '0455134200004', '2', '5', '2014-05-13 10:24:08', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('302', '0455134200004', '2', '5', '2014-05-13 10:24:09', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('303', '0455134200004', '2', '4', '2014-05-13 10:43:48', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('304', '0455134200004', '2', '5', '2014-05-13 10:44:08', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('305', '0455134200004', '2', '5', '2014-05-13 10:44:09', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('306', '0455134200004', '2', '5', '2014-05-13 10:44:10', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('307', '0455134200004', '2', '5', '2014-05-13 10:44:11', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('308', '0455134200004', '2', '5', '2014-05-13 10:44:12', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('309', '0455134200004', '2', '5', '2014-05-13 10:44:13', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('310', '0455134200004', '2', '5', '2014-05-13 10:44:14', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('311', '0455134200004', '2', '5', '2014-05-13 10:44:15', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('312', '0455134200004', '2', '5', '2014-05-13 10:44:16', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('313', '0455134200004', '2', '5', '2014-05-13 10:44:17', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('314', '0455134200004', '2', '5', '2014-05-13 10:44:18', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('315', '0455134200004', '2', '4', '2014-05-13 11:00:12', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('316', '0455134200004', '2', '5', '2014-05-13 11:00:34', '186', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('317', '0455134200004', '2', '5', '2014-05-13 11:00:35', '299', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('318', '0455134200004', '2', '5', '2014-05-13 11:00:36', '300', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('319', '0455134200004', '2', '5', '2014-05-13 11:00:37', '301', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('320', '0455134200004', '2', '5', '2014-05-13 11:00:38', '302', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('321', '0455134200004', '2', '5', '2014-05-13 11:00:39', '303', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('322', '0455134200004', '2', '5', '2014-05-13 11:00:40', '305', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('323', '0455134200004', '2', '5', '2014-05-13 11:00:41', '313', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('324', '0455134200004', '2', '5', '2014-05-13 11:00:42', '330', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('325', '0455134200004', '2', '5', '2014-05-13 11:00:43', '347', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('326', '0455134200004', '2', '5', '2014-05-13 11:00:44', '348', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('327', '0455134200004', '0', '0', '2014-05-13 13:05:08', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('328', '0455134200004', '0', '0', '2014-05-14 07:42:31', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('329', '0455134200004', '0', '0', '2014-05-14 21:10:50', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('330', '0455134200004', '0', '1', '2014-05-14 21:20:23', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('331', '0455134200004', '0', '0', '2014-05-14 21:41:18', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('332', '0455134200004', '0', '1', '2014-05-14 22:21:34', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('333', '0455134200004', '0', '0', '2014-05-14 23:04:16', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('334', '0455134200004', '0', '1', '2014-05-14 23:04:49', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('335', '0455134200004', '0', '0', '2014-05-15 00:14:39', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('336', '0455134200004', '2', '4', '2014-05-15 00:55:34', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('337', '0455134200004', '0', '1', '2014-05-15 00:56:23', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('338', '0455134200004', '0', '0', '2014-05-15 00:56:54', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('339', '0455134200004', '2', '4', '2014-05-15 00:57:26', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('340', '0455134200004', '2', '4', '2014-05-15 01:04:04', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('341', '0455134200004', '0', '1', '2014-05-15 01:22:30', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('342', '0455134200004', '0', '0', '2014-05-15 01:45:49', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('343', '0455134200004', '2', '4', '2014-05-15 01:46:01', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('344', '0455134200004', '0', '1', '2014-05-15 01:47:04', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('345', '0455134200004', '0', '0', '2014-05-15 16:16:37', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('346', '0455134200004', '2', '4', '2014-05-15 16:21:17', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('347', '0455134200004', '2', '4', '2014-05-15 16:42:04', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('348', '0455134200004', '2', '4', '2014-05-15 16:46:54', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('349', '0455134200004', '0', '1', '2000-01-01 00:00:00', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('350', '0455134200004', '1', '4', '2014-05-15 18:02:14', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('351', '0455134200004', '1', '6', '2014-05-15 18:02:58', '10', '0', '0', '1138');
INSERT INTO `iclock_oplog` VALUES ('352', '0455134200004', '1', '9', '2014-05-15 18:02:59', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('353', '0455134200004', '1', '9', '2014-05-15 18:03:00', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('354', '0455134200004', '1', '9', '2014-05-15 18:03:01', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('355', '0455134200004', '1', '9', '2014-05-15 18:03:02', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('356', '0455134200004', '0', '1', '2014-05-15 18:21:36', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('357', '0455134200004', '0', '1', '2014-05-15 18:44:48', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('358', '0455134200004', '0', '0', '2014-05-16 10:27:36', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('359', '0455134200004', '0', '1', '2014-05-16 10:33:46', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('360', '0455134200004', '0', '0', '2014-05-16 11:12:38', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('361', '0455134200004', '1', '4', '2014-05-16 11:13:18', '0', '0', '0', '0');
INSERT INTO `iclock_oplog` VALUES ('362', '0455134200004', '1', '4', '2014-05-16 11:24:31', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `levels`
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
  `level_id` int(10) unsigned NOT NULL auto_increment,
  `level` varchar(150) default NULL,
  `company_id` int(10) unsigned default NULL,
  `date_created` datetime default NULL,
  `date_updated` datetime default NULL,
  `creator_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of levels
-- ----------------------------

-- ----------------------------
-- Table structure for `locations`
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `location_id` int(10) unsigned NOT NULL auto_increment,
  `location_tag` varchar(150) default NULL,
  `address` varchar(150) default NULL,
  `city` varchar(150) default NULL,
  `state_id` int(10) unsigned default NULL,
  `company_id` int(10) unsigned default NULL,
  `date_created` datetime default NULL,
  `date_updated` datetime default NULL,
  `creator_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of locations
-- ----------------------------

-- ----------------------------
-- Table structure for `marital_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `marital_statuses`;
CREATE TABLE `marital_statuses` (
  `marital_status_id` int(10) unsigned NOT NULL auto_increment,
  `marital_status` varchar(45) default NULL,
  PRIMARY KEY  (`marital_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of marital_statuses
-- ----------------------------

-- ----------------------------
-- Table structure for `modules`
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL auto_increment,
  `subject` varchar(150) default NULL,
  `id_string` varchar(150) default NULL,
  `status` int(10) unsigned default NULL,
  `requires_login` int(10) unsigned default NULL,
  `menu_order` int(10) unsigned default NULL,
  PRIMARY KEY  (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('1', 'Staff Records', 'employee', '1', '1', '100');
INSERT INTO `modules` VALUES ('2', 'Attendance', 'attendance', '1', '1', '200');
INSERT INTO `modules` VALUES ('3', 'Leave', 'leave', '1', '1', '300');
INSERT INTO `modules` VALUES ('4', 'Communication', 'communication', '1', '1', '400');
INSERT INTO `modules` VALUES ('5', 'Payroll', 'payroll', '1', '1', '500');
INSERT INTO `modules` VALUES ('6', 'Events', 'event', '1', '1', '600');

-- ----------------------------
-- Table structure for `module_perms`
-- ----------------------------
DROP TABLE IF EXISTS `module_perms`;
CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL auto_increment,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL default '0',
  `menu_order` smallint(6) default '100',
  PRIMARY KEY  (`perm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of module_perms
-- ----------------------------
INSERT INTO `module_perms` VALUES ('1', '1', 'Staff Directory', 'staff_directory', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('2', '2', 'Attendance Record', 'attendance_record', '1', '1', '100');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(10) unsigned NOT NULL auto_increment,
  `role` varchar(150) default NULL,
  `company_id` int(10) unsigned default NULL,
  `date_created` datetime default NULL,
  `date_updated` datetime default NULL,
  `creator_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------

-- ----------------------------
-- Table structure for `states`
-- ----------------------------
DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `state_id` int(255) unsigned NOT NULL auto_increment,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL,
  PRIMARY KEY  (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of states
-- ----------------------------
INSERT INTO `states` VALUES ('1', 'Abia', '154');
INSERT INTO `states` VALUES ('2', 'Abuja', '154');
INSERT INTO `states` VALUES ('3', 'Adamawa', '154');
INSERT INTO `states` VALUES ('4', 'Akwa Ibom', '154');
INSERT INTO `states` VALUES ('5', 'Anambara', '154');
INSERT INTO `states` VALUES ('6', 'Bauchi', '154');
INSERT INTO `states` VALUES ('7', 'Bayelsa', '154');
INSERT INTO `states` VALUES ('8', 'Benue', '154');
INSERT INTO `states` VALUES ('9', 'Borno', '154');
INSERT INTO `states` VALUES ('10', 'Cross River', '154');
INSERT INTO `states` VALUES ('11', 'Delta', '154');
INSERT INTO `states` VALUES ('12', 'Ebonyi', '154');
INSERT INTO `states` VALUES ('13', 'Edo', '154');
INSERT INTO `states` VALUES ('14', 'Ekiti', '154');
INSERT INTO `states` VALUES ('15', 'Enugu', '154');
INSERT INTO `states` VALUES ('16', 'Gombe', '154');
INSERT INTO `states` VALUES ('17', 'Imo', '154');
INSERT INTO `states` VALUES ('18', 'Jigawa', '154');
INSERT INTO `states` VALUES ('19', 'Kaduna', '154');
INSERT INTO `states` VALUES ('20', 'Kano', '154');
INSERT INTO `states` VALUES ('21', 'Katsina', '154');
INSERT INTO `states` VALUES ('22', 'Kebbi', '154');
INSERT INTO `states` VALUES ('23', 'Kogi', '154');
INSERT INTO `states` VALUES ('24', 'Kwara', '154');
INSERT INTO `states` VALUES ('25', 'Lagos', '154');
INSERT INTO `states` VALUES ('26', 'Nassarawa', '154');
INSERT INTO `states` VALUES ('27', 'Niger', '154');
INSERT INTO `states` VALUES ('28', 'Ogun', '154');
INSERT INTO `states` VALUES ('29', 'Ondo', '154');
INSERT INTO `states` VALUES ('30', 'Osun', '154');
INSERT INTO `states` VALUES ('31', 'Oyo', '154');
INSERT INTO `states` VALUES ('32', 'Plateau', '154');
INSERT INTO `states` VALUES ('33', 'Rivers', '154');
INSERT INTO `states` VALUES ('34', 'Sokoto', '154');
INSERT INTO `states` VALUES ('35', 'Taraba', '154');
INSERT INTO `states` VALUES ('36', 'Yobe', '154');
INSERT INTO `states` VALUES ('37', 'Zamfara', '154');

-- ----------------------------
-- Table structure for `template`
-- ----------------------------
DROP TABLE IF EXISTS `template`;
CREATE TABLE `template` (
  `templateid` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `Template` longtext NOT NULL,
  `FingerID` smallint(6) NOT NULL,
  `Valid` smallint(6) NOT NULL,
  `DelTag` smallint(6) NOT NULL,
  `SN` varchar(20) default NULL,
  `UTime` datetime default NULL,
  `BITMAPPICTURE` longtext,
  `BITMAPPICTURE2` longtext,
  `BITMAPPICTURE3` longtext,
  `BITMAPPICTURE4` longtext,
  `USETYPE` smallint(6) default NULL,
  `Template2` longtext,
  `Template3` longtext,
  PRIMARY KEY  (`templateid`),
  UNIQUE KEY `userid` (`userid`,`FingerID`),
  UNIQUE KEY `USERFINGER` (`userid`,`FingerID`),
  KEY `template_userid` (`userid`),
  KEY `template_SN` (`SN`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of template
-- ----------------------------
INSERT INTO `template` VALUES ('1', '1', 'TalTUzIxAAAE6usECAUHCc7QAAAc62kBAAAAhBcysupEAJoPfwCDAHrl5QBKALENJwBT6qsNPgBfAIANQupeANUNrgCnAJrlVgBlAGANUwBp6pMPwgBvAOoPP+pwAMEMPQC/AEHnPgCJAEwOtwCI6kEOpgCRAP4Pc+qWADIO+wBSACrlfgCpALIOcQCu6kEPiQCyAHwOU+qyADQPKAB9AE3l2AC5ADkPvgC+6rcOOQDDAPoPf+rJAK0OhQAIANfk7wDPADQPkQDY6iQOGgDdAIwPp+rlAGcPKgAgADvlBgHnAK8OKgDt6jgNSQDxAOwOhuryAIcP5wA/AO3nOgD8AC4MlgAE66YM0AAFASYPSeoLASEMJgDVATTl3AAXAfkOrQAe64oPFwAmAfMOgOoxAXYPOQD+AaPlNABBAZgPHgBM6/UPewBLAZkONhYfb+/oLu+uIR8IFQtXGC+FWfs6lGYQPHKBhYDnKHi4dQEV+XKUD6rurI2pH5KgGPcasPNn6ZI6hsOX/JSEfZXt9Iaw+Pnp8AjtjvYLtRN2emuLcf/2qmOH+B18gXp4t/Ra/gNt7fsZ/H2BKAdW4LuLZAsxBOQPvh9XgZISUQ1DAnLlawTa+isMyAixFpQE2fkuDacd7DWU+K33QCucHXH1jP/u/G6FdP6aa/vpTQ0GAtsOjgu3lre81fmsE1byTHpyfT7JXFFBZAQHvf2BhegG7su/trbfabHACuIaOAVtDlmK9YuFa2B6xe6N/kevMJX89hn6qf/0B074LBpWEMsES2yeDhMVJQtikq+iXfWn7cvzMBK3co+SwfKKZp9pivxD+VYP8evX0Wq3XjDYmbKCAyD7AQaHHEADALcV0v8H6sEkHsAGAFIZCBX/NwsAjh7JwPmq/2gVAIch1cD7FfptWv3CwDrB+isuBQB9Jgw6QRDqayoJwP//hTLErWVzCQBkNTgp+8QJAOc6N8I6/cYpwQUAe0ODSAQEaUYTPQQAd419iO4B9lQ0bgzFPl8p+P0n//8xwADgsSj6khoAsqMtVaE6wPrD/cA6wfkUwf79+v/+2wCWgZunxonD/kn/xSnAw8XDwsMEiMUowwwANW9MkFjGugkAxnEp/gTAVOwBwXIxwMCPDgTTfUz/wMDABVz7K//EBgAhhZ/CYu0BIZRWwXIFAwT+hlD/BwA7SUz7K8U6EACkjGjCwCrFwsjCi8I6lQHqQI1G/4YExXGWrKMDAHeSFz0PBECSNCn//fs6/fgX/CcNAKWU+P80Efz7/Pw1BMX/nc00BQD5mzCtEwS8r8Aw/PobOPzEzRMLAIyuw2DNwSnFwcsHAN15MPkr+ykWANi9+P/EEf7+wMBYODn8xRf9PgsAJ76MwMapiMUPAGjEdf/6F/77+fr8JOMEBN/GQ34fADsCOsSXwsGnkMHBAcDGKHfCb8LEktgASrHq/v3B+/72//sqwf/+O/3/OsD7FP7+wfwEADfSLbUFAFjfJ6XNAFYKMMLGp8MHxRnmrF5KAwAn6IbAD+ot6Df/wcIEw8colQMA8+wnBggUJghkYlQOEcoPkijGwXLBwVEEAxTjE0PABBAk0Tql7hHiFvfA+M4RBPd8/8LCh8CSCRQdKH2Mav4H1fs0nYVaAxD7Nq7+APo4QwlcBBASSn5zCRAcTwPCOMHE8QUQPl/9UQAAAAAAAA==', '0', '1', '0', '0455134200004', '2014-05-15 10:59:04', null, null, null, null, null, null, null);
INSERT INTO `template` VALUES ('2', '2', 'SjVTUzIxAAADdnQECAUHCc7QAAAbd2kBAAAAg5sZbHYkAIQPmAD7AIV5kwBGAAoPewBmdo8POgBrAL4Ph3aGAIUP5QBQAJB5lQCXAA4PHACcdhcPygCtAFcPOHa7APkPpwADAI14qwDSABYOfADYdpYOhQDcAEEPo3YAAY0P8ADCAZ95lgAYAYgOvgAjdwsOlgAnAU8OH3YqAVsPZQDuAWJ5mgA9AY8NTQBHd24NoABMAdcNFHYefFsLVH9WBByMUYcXiJN+SoaUfBr/R4cX+yb7HgzGf+f7UXyXAZBxFIqngYN/iITO89KL0X3SAZMHSPWad3N3KHvQ+114IYuBgOKWRICOfUMP8nLm+uL4RnA/Bs77gweuE4D9PADJ/m2DIKqC9WyHuQQt/kSDJIkiUzPvDVr/9xyuJHxlIakGwOEiKOfkIYT9puOKATYwASAxAQLtIQFyAVYLAz0HxVIRcEhMBACoH9Z/CHZWGgb/wF2dwQB2NykA/wsAmSsDPVz/XQoAG/UDa7dGwQsAFTTVxMM8WcIUABE81cT8Kv/Awf7+wjpQ/S8IAJw+DMGOwDt4AQpFDMJrBf9WtkIKAJVGDJJUSmMBBlAGdFSdQ2OIwWwWAAdhMf3Bi8BV/8BDVIBaEXYFbPT/PlKFYMJIBQA2bH1p0gAEC/XA/cP+TP3B/bf+Z//9WRfFBoWBwUfAQ/3BkUZYHAsAf4aDYrRWBnaZmAz/VRjFBZyfKsA+wEovO2XDFgQA3aETTN0AC9HwT1P/PVGOwEm3/xcACbPtgf/9tjxM/1NMwAUJA0i49/9ARBjFCMKfQjNH/0ZKgcBacAFLzHTBXN0AAaTlQE//wP6OQk8ywAwAgdiGvMHDGcHCFQAB3SJE/ItVRsDCIsCRDAP/3Aw2/1L/kAwD99+DdcJiwq8VA3To3v1M/zWDwDw6wRcQAwDcOv1JtsH8L8D9wTr/wojD//8MEJzEk4v/w8B3AxD0zh78fRGSFonBkgeDwG4RByXX/sA6/mNHMf9UwMBgzBBqX/b7wP5DwcYQYlxqxBcQCysnZCyIOv3/TMBFmQQTEy5kbQgQl/+TwLTHwcP9FRDZQtQ5PS5G/v9VtgcT9EVxwsP7LcIQnz+bw8TDmQ/VO0il/v/9/TT/OcH8GRUQ40yT/jrAwrbBwcOIk4cBAxMSXgDBAAA7SUz7Kw==', '0', '1', '0', '0455134200004', '2014-05-15 10:59:04', null, null, null, null, null, null, null);
INSERT INTO `template` VALUES ('3', '4', 'TYVTUzIxAAAExsQECAUHCc7QAAAcx2kBAAAAhGstkMYdAJYPYADkAInJqAAqABEPlQArxn8PjgA4AFYPaMZHAIYPPwCkAHXJnQBjAI0PWgBwxooP6wB7ANIPZcaTAAcP7gBRAJzJfACbAIcP/gC1xuUPzwC2AFoOZca9AAEPwAB4AJzIfAC+AH4POgDDxhUOqwDMAFYPg8bQAH0OCQEdAB/IWADgAOsPtgDlxgQPjADkAEAOBsfnAJkOIAAqAIDJLgD5AHoOzwH/xh0NzQAEAVIPgcYMAQYOlwDIAXvIKQAOAXoOaAAKx5IPagARASgPLsYZAekOUwDaAVrJJgAjAf0N0gAhxwQOOgAmAUEOh8YyAWEN7gD2AZnJLgBDAb0PswBBx+YMNQBPASYPAUA0B2sJVRHvCg87CH7egLuHIPEmP1INtf/ahJsNns/v9a76++0a9T+pLASq/4sKdABTw6r7pYK/eyqPgUW7C98kpYKO+BtAhIM6CVaLb+aHGsb6UQhODwYIgUHLGQ4e2fiUB1vZMAGFf8b5XPmuuvp11f9qGZMSPT+wAWl7uYTUCaLHmOte7YtwRIJlTfwfsQmBgif2uUYYfqaEFQ33DDJZoPlVAk6Q0IYixx4JXgVnGtb+eU/0G76mgXnA7r45QZNJfdUCtBbmO3OO/ZON63+PVLXh77Vlmd/Ic66xTRcB+Il5vAhh2FSPhYmZnTQrfbjjXsZ3jwp+g3/J8d+SOHrAsIo7T1L9TCtmYpbosQ5dgGUWID3EAoLZ5QQAagATgwkEvgETTMNVBMWmBNVtBQBdARMFVQLGmQEQUMIOxVAAyjdbwMFkwMIAicISUsDCDwCGBwj4wcBSwFbAwgBFyA3//nMEAPMVAvEJADEYF8U7wDrXATIlBsH8mMDFOGX/wVsFAGkpC6YGAKsuF8IF/2DVATE59zP+nlthBvyPCACSPNNzXsABaEOMwcJRDAS2RhNoVkzAwwBij4d5wgkAL5L0xO03CgCUZIycwsc4lQcAoGcWBcBkzwGcdIxZmwUTBOp25P////s6/8QG///Awf/ABFcWxjGB8MIq/ztgRpPAEwArkOTqKvuAwWBpCABlVAP5Bv5EBgB5mkPBwFESACue3jPrOMSEUMIOADGkGzP6ODDAwDcTAPOs2gb8//39/v8F//sG/8P/wcD+wwBgfvsi/wcA03wXYfwLAF2/YMEFwPs6/P/7DwCqDZZwBMLFwcLAwDj7+NABMsrQ/EE5//mC/8P+wMDABcHEOwUAg86GxAYIBGnOGsFawP/JAI8VEUz/wMDCBB0AxoLUgKYYAN/h+Qz6xf3A/v05//kH/v/Awf/ABcDF4AQAVeRawDnDAMYt+nf+ww/VygZRY//Cw6PDO/zA3hENBv3BxEXA+Tn9/fxdRMAG/vs6BRCTCYapwxCsypePxQcQm8oQWfsHEGYUYsM6XxHWKh8Ax2fAO/r59m7/wP77/MEQVOVdSwYQAib0d8fVEe8vscjAnk7GAsTCwP7/wjgRFcAuT/7/IcAE/lIE/FcFEH40rMf+OwIQFTUi/ckQzPCRwGnAxsEDUA3Wv0CXwnDDdAYUvkLn+v7+wsAQiYUN/MfAEhAiSr4N+sPCZI6iBcD/xoVaAxD7Ng==', '0', '1', '0', '0455134200004', '2014-05-15 10:59:05', null, null, null, null, null, null, null);
INSERT INTO `template` VALUES ('4', '5', 'TQ9TUzIxAAAETFAECAUHCc7QAAAcTWkBAAAAhPEnNkwxABcPSwD3AJBDHgA5AIAPXQBDTKgPaABxAFoPhkxzAKkPzwBnAKBDeQCnAKcP4wCuTIYP7ACzAHsPVUy7AJwPZwADAKxDGwDHAHEPKADfTCMOQwDqAFMPXEzvAKEPzQArAKdCPwD0AIoPIwD/TCwM0wAFAdkN5kwLAaML7QDPAatALwAUAXwPFQAfTTgN3AAbAfYM8kwcAa8N2ADhAThAngAvATUOhAA2TZAO2AAyAXkNE0w1AR4OyAD9Ab5B6wA5AbMPagA5TasNIgBEAeQOwExFASkNpgCNAbJCYgBLAZsOsgBITRoO2Zlthkp7dTJnFx/38Wk/7xuvLgA7D2MXIPsTtsb7WQ+vAOYG9qUKhx8DLgGPD6dOSxgL7Cvznx0G0hYf+fbS97IUaUKvASoL4uzq3gOaq/TKgR5m+Q/1uT4caQ1VG2v7EcbwewYByPco605dnIyBgMERtHhVvajm3PepeKiIVEOAhAEBLu0f9VouSQto/w0eZfvIsxyRgQFVf7QPEEfpB4WAaY1Eg1IzmxVacW53QIAxSNQJif0+pqePEN+4AEESXfm0e5pINPR1deWCzAVO3SqkJHPlgvxvrUCAg8aM6YFHD28z6IFjZE/oVCCELy8gOgECVdqwAkxzDSfAwFTXAFRDH/9VwFhmnP37XgFFEB5H/5j/WCFGDgBqFR76wGw4QAoALxgW+cH7jEcGAJ0YKa7AAUylGCRlDgChGSSzRME+w8A+3ABCUB3//8BKRQTAxYzBwP//Wv+VBARvHxDA/wgAUh4jLlsMAIcfKQXAXbNs/gUA3aD1wsaKCwCPISJDBcBvRQELLwAi/5YUBHouEP4+/2UFamGzRxUANDQXOv9DjMFKwcFaPgQJBE9D/f/+/cA7WxNMA0oDPDf+BUZijVnBSgwAB5YDNLJSPgsABV3FMzt0FwADZAM7+0ZSP8HB/z0XAMBuB3z/VEzAdMGV//ixDQBrdCREsWjFWwEGdgnB/fs7WCbBWv0tCgDCgQez/P9GPhcAzYoHsv9BRcBZUQT//7L++gcABJQ4/fp3AgDzlF7I0QAF7fEbwT/+wAVkxIzDHAMABKY4/ghMeqope2tUBRIESK/9/sD8Pjr/xD/BwUkTAANy/fux////QHvAoRoMTAjA/fw7RsMA5ZUoVMADAOkMJMdeAQTZ5/38Of77s8P+wP/BwAT/+LEFAPHcFyjAAOmTKMP+/wYAxuTjsf39/g8ABi/wxLb8/8D+/8AFwfqP/AUAOfOTAZcATN7zNHYDADz3LYwPEAgA7f8+/vux/1z/VwYQEgkYsvw1DhAIDSz9+rApWGsMEA3W7fhtWcA+CBAT2ADFtvxcBBDbH/X+x0gR9yE3JgjVCiZcRMT4wwQQ3jMTjP4EEBJCKQfEAFzJRSfCygbVNlhKev8EEMpeRnD59m7/wP4=', '0', '1', '0', '0455134200004', '2014-05-15 10:59:05', null, null, null, null, null, null, null);
INSERT INTO `template` VALUES ('5', '10', 'SgdTUzIxAAADREcECAUHCc7QAAAbRWkBAAAAg+kZj0QpAAQPtQCEAItLNABaAHwPIQBvRI0PgQBxAEQP20R4AA8PkwC6AAtLyACMAIwPggCoRPoPqgCvAEIOrES9AA4OwAAHAI5KWQDEAHEPTADQRH4P+ADmAFMPqET4AIgPpQDIAYFKjAAaAQAOZAAZRYEOdQAjAZoPrEQwAYkOmwD5AWlKWgBCAdoPcwBKRX4PiwBPATIPlDjGh7v/k4Te/yA/Ev8/g/N7kH1RR5YHHXrj+wJ/UsHMhh+LFYpigYA60H7SA1MEa4ycO2Z/LXgZ/x78JsyAgd6TgYFUC7hGu3gf9QfvK3PgvCb5gwtjE0aLRk1HCVqLTQQM/WrDgIHxo22DCACCxSj7FWN+ht/jZmYsDqELAeKwdimoh36z5Y/fcPqVUH+LZY6Ge6pwA9SBASAsAQEzHRpAAZshg38DxaQoQv4LAGYu+jr9ZrtWBwBhMgkHN8NBAVc0ff9dygAxegFYwS9t//ITA0tL/VL//8E6RVcPwxIABlnwOMD+gD5MwFhGA8UwWT7BEwAJZQZW/MIDwVD//8FdzwB/NIHAa3jAFMULdrBVVMBDVMD7wQdE2noMNQQAU4AAexcADIzwPQVUWHk2VMAWAAlf7UgQRv9J//7B88AVRA2m7Uo2OI9UVwgJAEyq9zUE//xTAQe46cD/g/9Hu0JE/8A+CMVUwTPC/8DBhxDFXMe+wf1VTFhEzQBVjHFrwXYZAMfO33VWQcA9WD4F/sC5GQAI0+IvBf38hE1i/f7BS6ANA8HSgMDDfv9MbBZE9eOawv/CB/3ChsHAwlnCakYPA+z0kMN1g8EHwlFKAaf7g2fCBv/AOEgJAK/8EAU3wbr8CxCgDIMEwsKHwMPBwP8Z1BMO3mtiwHT+mQHAwzXAgRgRDBZZwcK7cWLCwsDCBcNOIMAXEQkgmgf+woX+eMDBk8Ksc8JAESohWoQD1XEmJsMJEKovjAbCxoXDKhUQ+jJTwVaFbcGCwnzB6QQT0j1tfxMQ9oWaeIX+joDDwsIF+v5AEbJFiasP1e9G13L/fcDGwqENE6JLicH8h8IDwsG5/gMQskt6BQMTjVp6/wAAAAAAAAA=', '0', '1', '0', '0455134200004', '2014-05-15 17:57:12', null, null, null, null, null, null, null);
INSERT INTO `template` VALUES ('6', '9', 'TTFTUzIxAAAEcnMECAUHCc7QAAAcc2kBAAAAhJ8prHIrAJcPcwDrAIl9ZAA8AIAPWgBDcpcPewBTAEMPSnJsAGoPqwCwAJR9PwCFAO0PbACPcpMPbACgAM8PgnKtAIoP5QB5ABZ9QgC9AOYPCQDEcicPagDPAMQOhXLRAIIO1AAXAKF9wgDTAJ0PagDYcpcPhwDiALkNDXPvAJsPYQA0AOt9eADzAAkO/QDzct0PjAD3AEwOJ3L/AF8O8AA7AKJ87gAFAZgOJQAOcxkOgQAhAcgNbHIiAe8OkADjAYp/pQAoAToNAQAvcx4LSQA1AZMNnHI5AaIMVwD+AeZ5owA8ATQMJgBDc6IMZwBKATkNqXJRAZcLNgCuDUthVA0uCgv5JPUmjlYaugBeEuYImnqz+u7ycX/G5e+cNAKn/IsPqIevl9IKsQZTCN5vjfG6CN8liYP7CVb56O7Jb9Z2a+eHr1r6iYGtiZAWhfCbhcoVMQlEgk4LWAiNgbWXJPixC1AK2fri8gtyvY889mF2TvSPBUv3lOpe8YJ2RIFR/PweGX6i7qr7hfOsDq4KFYbec095IRJBkcYMafZZ8s/9XYBNdQP7hfLwIpZsjeYoD/rrgIAh7flVZJnJ34wJ5uTS7kd/OQYXaUZd7HNIFb0bMIy96nn3GZcVj3iefodbDlqPIWwfqUYOlWZL9KYsWAGIVr8B5T0FcGMekgYAbsAPPLMHAIMFF/+ZBgQbCBPAQgsAmw0UNlllCwBdFdVMV42JCABOGwbuwE53ATsiAz0DxawqaP4KAC4w9Dv+M438wg8ANjcxKkCNXMFDBQBh+YOPdwGjShp0EMUvSZ8wwPxVYGTNAHMikZ3CewoAulUXssHA/mv+CMV0UvvDkIQFAEqqbXp+Aadxk8FuB8GEsw0ApHaQwAWIhPAGAK95GmQF/w1yQITtwSP9nA0E142QwIbDwQXAwI3ABwCtjhwH/nRnASOV4sBD5ET6jV3/gggAcFoJ+o1FwAYAg6pMwcDlGAAZu9o4Ojv7WkXAS8HD/JAGBJu/FsD//P/NANSxJsF4VxIAF8+mGG7Aw8TAwgdpxX4BqtqXwMBdw8UKDACD3oPDAcUys2/EGQAY4AX8+4w/L/z9/8E4wMWMw/7/wHcZxSDgpEU9/f/7/vjAxIzDwGVXAwDZ5VqyAgCC5YDDzwCPlxL/RVD+EMU886xSwPoewML3wRByJ/rcXTv+Of36s/5QwMDAA8U1/yXBFwDs/aIEwcSzUcDDwMLFB2nFPcMEAB7/ZLIDFFYDXsEDEPTHFvp2EeQJDCgE1eIKbkYGEGgf4jj6J3QRbCTt//k++RRiwCekwlzEOsjHJIkDEKgpNAUEFLoqHD0DEMPqK8B/EUsw4GL7PvzEjP/9/gQRBvVWXXQRkzSexcYFxBdiDTkrwcLFBML4t0/8gP5dGdX4PcjJ/nD/wMGoxcW3wlvCwP6VwxCVMKbHxagWEPREwo/+NzT9+/87wzaw/v35BBBog+kSdhFrS/0mAAAAAAAAAA==', '0', '1', '0', '0455134200004', '2014-05-15 18:03:09', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `userid` int(11) NOT NULL auto_increment,
  `badgenumber` varchar(20) NOT NULL,
  `defaultdeptid` int(11) default NULL,
  `name` varchar(40) default NULL,
  `Password` varchar(20) default NULL,
  `Card` varchar(20) default NULL,
  `Privilege` int(11) default NULL,
  `AccGroup` int(11) default NULL,
  `TimeZones` varchar(20) default NULL,
  `Gender` varchar(2) default NULL,
  `Birthday` datetime default NULL,
  `street` varchar(40) default NULL,
  `zip` varchar(6) default NULL,
  `ophone` varchar(20) default NULL,
  `FPHONE` varchar(20) default NULL,
  `pager` varchar(20) default NULL,
  `minzu` varchar(8) default NULL,
  `title` varchar(20) default NULL,
  `SN` varchar(20) default NULL,
  `SSN` varchar(20) default NULL,
  `UTime` datetime default NULL,
  `State` varchar(2) default NULL,
  `City` varchar(2) default NULL,
  `SECURITYFLAGS` smallint(6) default NULL,
  `DelTag` smallint(6) NOT NULL,
  `RegisterOT` int(11) default NULL,
  `AutoSchPlan` int(11) default NULL,
  `MinAutoSchInterval` int(11) default NULL,
  `Image_id` int(11) default NULL,
  `email` varchar(150) default NULL,
  `status` tinyint(4) default NULL,
  `company_id` int(11) default NULL,
  `pass` varchar(150) default NULL,
  `access_level` tinyint(4) default NULL,
  PRIMARY KEY  (`userid`),
  KEY `userinfo_defaultdeptid` (`defaultdeptid`),
  KEY `userinfo_SN` (`SN`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('18', '000000008', '1', ' Yinka Davis', '', null, null, null, null, null, null, null, null, null, null, null, null, null, '0421142600037', null, '2015-11-17 08:53:41', null, null, null, '0', '1', null, null, null, 'admin@aol.com', '1', '100', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '1');
INSERT INTO `userinfo` VALUES ('19', '000000023', '1', 'sugun ige', '', '0', '0', '1', '0001000100000000', null, null, null, null, null, null, null, null, null, '0421142600037', null, '2015-11-17 09:39:29', null, null, null, '0', '1', null, null, null, null, null, '100', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', null);
INSERT INTO `userinfo` VALUES ('9', '3201', '1', 'Andrea Sepotero', '', '0', null, '1', '0000000000000000', null, null, '', '', '', '', '', '', '', '0455134200004', null, '2014-05-15 18:03:09', null, null, null, '0', '1', null, null, null, null, null, '100', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', null);
INSERT INTO `userinfo` VALUES ('10', '000000222', '1', 'Karlen Kirakosyan', '', '0', null, '1', '0000000000000000', null, null, '', '', '', '', '', '', '', '0455134200004', null, '2014-05-16 10:30:22', null, null, null, '0', '1', null, null, null, null, null, '100', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', null);
INSERT INTO `userinfo` VALUES ('12', '000000201', '1', 'Paul Azeez', '2000', '', '14', null, '0000000000000000', null, null, '', '', '', '', '', '', '', '0455134200004', null, '2014-05-16 10:30:22', null, null, null, '0', '1', null, null, null, null, null, '100', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', null);
DROP TRIGGER IF EXISTS `trigger_Update_checkinout`;
DELIMITER ;;
CREATE TRIGGER `trigger_Update_checkinout` AFTER INSERT ON `checkinout` FOR EACH ROW INSERT INTO ci.checkinout(
id,
userid,
checktime,
checktype,
verifycode,
SN,
sensorid,
WorkCode,
Reserved
)
SELECT id,
userid,
checktime,
checktype,
verifycode,
SN,
sensorid,
WorkCode,
Reserved
FROM adms_db.checkinout ORDER BY id DESC LIMIT 1
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_Insert_devices`;
DELIMITER ;;
CREATE TRIGGER `trigger_Insert_devices` AFTER INSERT ON `iclock` FOR EACH ROW INSERT INTO ci.devices(
sn,
state,
last_activity,
trans_times,
trans_interval,
log_stamp,
op_log_stamp,
photo_stamp,
alias,
dept_id,
update_db,
style,
firmware_version,
fp_count,
transaction_count,
user_count,
main_time,
max_finger_count,
max_att_log_count,
device_name,
alg_ver,
flash_size,
free_flash_size,
language,
volume,
dt_fmt,
ip_address,
is_tft,
platform,
brightness,
backup_dev,
oem_vendor,
city,
acc_fun,
tz_adj,
del_tag,
fp_version,
push_version

)
SELECT
SN,
State,
LastActivity,
TransTimes,
TransInterval,
LogStamp,
OpLogStamp,
PhotoStamp,
Alias,
DeptID,
UpdateDB,
Style,
FWVersion,
FPCount,
TransactionCount,
UserCount,
MainTime,
MaxFingerCount,
MaxAttLogCount,
DeviceName,
AlgVer,
FlashSize,
FreeFlashSize,
Language,
VOLUME,
DtFmt,
IPAddress,
IsTFT,
Platform,
Brightness,
BackupDev,
OEMVendor,
City,
AccFun,
TZAdj,
DelTag,
FPVersion,
PushVersion
FROM adms_db.iclock ORDER BY LastActivity LIMIT 1
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_Insert_userinfo`;
DELIMITER ;;
CREATE TRIGGER `trigger_Insert_userinfo` AFTER INSERT ON `userinfo` FOR EACH ROW INSERT INTO ci.attendance_user_info(
user_id,
id_user,
dept_id,
name,
password,
card,
priviledge,
acc_group,
time_zones,
sn,
u_time,
del_tag,
register_ot,
auto_sch_plan,
min_auto_sch_interval,
image_id
)
SELECT userid,
badgenumber,
defaultdeptid,
name,
Password,
Card,
Privilege,
AccGroup,
TimeZones,
SN,
UTime,
DelTag,
RegisterOT,
AutoSchPlan,
MinAutoSchInterval,
Image_id
FROM adms_db.userinfo ORDER BY userid DESC LIMIT 1
;;
DELIMITER ;
