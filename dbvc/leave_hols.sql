-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2016 at 08:56 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hrs_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `leave_hols`
--

CREATE TABLE IF NOT EXISTS `leave_hols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hol_name` varchar(50) NOT NULL,
  `hol_date` datetime NOT NULL,
  `repeat_annual` varchar(10) NOT NULL,
  `day_length` varchar(10) NOT NULL,
  `company_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `leave_hols`
--

INSERT INTO `leave_hols` (`id`, `hol_name`, `hol_date`, `repeat_annual`, `day_length`, `company_id`, `creator_id`) VALUES
(4, 'Test Holiday', '2016-09-12 00:00:00', 'No', 'fullday', 100, 18);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
