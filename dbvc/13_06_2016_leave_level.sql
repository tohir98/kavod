CREATE TABLE IF NOT EXISTS `leave_level` (
  `leave_level_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `leave_type_id` int(11) DEFAULT NULL,
  `no_of_days` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `leave_level`
--
ALTER TABLE `leave_level`
  ADD PRIMARY KEY (`leave_level_id`);

--
-- AUTO_INCREMENT for table `leave_level`
--
ALTER TABLE `leave_level`
  MODIFY `leave_level_id` int(11) NOT NULL AUTO_INCREMENT;