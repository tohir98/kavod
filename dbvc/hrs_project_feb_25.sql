-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 25, 2016 at 09:27 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrs_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `access_level_id` int(10) unsigned NOT NULL,
  `access_level` varchar(150) DEFAULT NULL,
  `default_values` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `account_type_id` int(10) unsigned NOT NULL,
  `account_type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) unsigned NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendance_shifts`
--

CREATE TABLE `attendance_shifts` (
  `attendance_shift_id` int(10) unsigned NOT NULL,
  `shift_name` varchar(150) DEFAULT NULL,
  `clock_in` datetime DEFAULT NULL,
  `clock_out` datetime DEFAULT NULL,
  `break_in` varchar(150) DEFAULT NULL,
  `break_out` varchar(150) DEFAULT NULL,
  `overtime` varchar(150) DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `lateness` varchar(150) DEFAULT NULL,
  `created_by` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_message`
--

CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add session', 1, 'add_session'),
(2, 'Can change session', 1, 'change_session'),
(3, 'Can delete session', 1, 'delete_session'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add message', 5, 'add_message'),
(14, 'Can change message', 5, 'change_message'),
(15, 'Can delete message', 5, 'delete_message'),
(16, 'Can add content type', 6, 'add_contenttype'),
(17, 'Can change content type', 6, 'change_contenttype'),
(18, 'Can delete content type', 6, 'delete_contenttype'),
(19, 'Can add log entry', 7, 'add_logentry'),
(20, 'Can change log entry', 7, 'change_logentry'),
(21, 'Can delete log entry', 7, 'delete_logentry'),
(22, 'Can add department', 8, 'add_department'),
(23, 'Can change department', 8, 'change_department'),
(24, 'Can delete department', 8, 'delete_department'),
(25, 'Can add device', 9, 'add_iclock'),
(26, 'Can change device', 9, 'change_iclock'),
(27, 'Can delete device', 9, 'delete_iclock'),
(28, 'Pause device', 9, 'pause_iclock'),
(29, 'Resume a resumed device', 9, 'resume_iclock'),
(30, 'Upgrade firmware', 9, 'upgradefw_iclock'),
(31, 'Copy data between device', 9, 'copyudata_iclock'),
(32, 'Upload data again', 9, 'reloaddata_iclock'),
(33, 'Upload transactions again', 9, 'reloadlogdata_iclock'),
(34, 'Refresh device information', 9, 'info_iclock'),
(35, 'Reboot device', 9, 'reboot_iclock'),
(36, 'Upload new data', 9, 'loaddata_iclock'),
(37, 'Clear data in device', 9, 'cleardata_iclock'),
(38, 'Clear transactions in device', 9, 'clearlog_iclock'),
(39, 'Set options of device', 9, 'devoption_iclock'),
(40, 'Reset Password in device', 9, 'resetPwd_iclock'),
(41, 'Restore employee to device', 9, 'restoreData_iclock'),
(42, 'Output unlock signal', 9, 'unlock_iclock'),
(43, 'Terminate alarm signal', 9, 'unalarm_iclock'),
(44, 'Clear all employee', 9, 'clear_all_employee'),
(45, 'Upgrade by u-pack', 9, 'upgrade_by_u-pack'),
(46, 'Can add admin granted department', 10, 'add_deptadmin'),
(47, 'Can change admin granted department', 10, 'change_deptadmin'),
(48, 'Can delete admin granted department', 10, 'delete_deptadmin'),
(49, 'Can add employee', 11, 'add_employee'),
(50, 'Can change employee', 11, 'change_employee'),
(51, 'Can delete employee', 11, 'delete_employee'),
(52, 'Transfer employee to the device', 11, 'toDev_employee'),
(53, 'Transfer to the device templately', 11, 'toDevWithin_employee'),
(54, 'Delete employee from the device', 11, 'delDev_employee'),
(55, 'Employee leave', 11, 'empLeave_employee'),
(56, 'Move employee to a new device', 11, 'mvToDev_employee'),
(57, 'Change employee''s department', 11, 'toDepart_employee'),
(58, 'Enroll employee''s fingerprint', 11, 'enroll_employee'),
(59, 'DataBase', 11, 'optionsDatabase_employee'),
(60, 'Can add fingerprint', 12, 'add_fptemp'),
(61, 'Can change fingerprint', 12, 'change_fptemp'),
(62, 'Can delete fingerprint', 12, 'delete_fptemp'),
(63, 'Can add transaction', 13, 'add_transaction'),
(64, 'Can change transaction', 13, 'change_transaction'),
(65, 'Can delete transaction', 13, 'delete_transaction'),
(66, 'Clear Obsolete Data', 13, 'clearObsoleteData_transaction'),
(67, 'Can add device operation log', 14, 'add_oplog'),
(68, 'Can change device operation log', 14, 'change_oplog'),
(69, 'Can delete device operation log', 14, 'delete_oplog'),
(70, 'Transaction Monitor', 14, 'monitor_oplog'),
(71, 'Can add data from device', 15, 'add_devlog'),
(72, 'Can change data from device', 15, 'change_devlog'),
(73, 'Can delete data from device', 15, 'delete_devlog'),
(74, 'Can add command to device', 16, 'add_devcmds'),
(75, 'Can change command to device', 16, 'change_devcmds'),
(76, 'Can delete command to device', 16, 'delete_devcmds'),
(77, 'Can add public information', 17, 'add_messages'),
(78, 'Can change public information', 17, 'change_messages'),
(79, 'Can delete public information', 17, 'delete_messages'),
(80, 'Can add information subscription', 18, 'add_iclockmsg'),
(81, 'Can change information subscription', 18, 'change_iclockmsg'),
(82, 'Can delete information subscription', 18, 'delete_iclockmsg'),
(83, 'Can add administration log', 19, 'add_adminlog'),
(84, 'Can change administration log', 19, 'change_adminlog'),
(85, 'Can delete administration log', 19, 'delete_adminlog'),
(86, 'Can browse ContentType', 6, 'browse_contenttype'),
(87, 'Can browse DeptAdmin', 10, 'browse_deptadmin'),
(88, 'Can browse Group', 3, 'browse_group'),
(89, 'Can browse IclockMsg', 18, 'browse_iclockmsg'),
(90, 'Can browse Permission', 2, 'browse_permission'),
(91, 'Can browse User', 4, 'browse_user'),
(92, 'Can browse adminLog', 19, 'browse_adminlog'),
(93, 'Can browse department', 8, 'browse_department'),
(94, 'Can browse devcmds', 16, 'browse_devcmds'),
(95, 'Can browse devlog', 15, 'browse_devlog'),
(96, 'Can browse employee', 11, 'browse_employee'),
(97, 'Can browse fptemp', 12, 'browse_fptemp'),
(98, 'Can browse iclock', 9, 'browse_iclock'),
(99, 'Can browse messages', 17, 'browse_messages'),
(100, 'Can browse oplog', 14, 'browse_oplog'),
(101, 'Can browse transaction', 13, 'browse_transaction'),
(102, 'Init database', 13, 'init_database');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`) VALUES
(1, 'admin', '', '', 'fae_service@zkteco.com', 'sha1$3eb60$103b53a937f36a3809544315291b474ec66469b1', 1, 1, 1, '2015-11-17 08:43:39', '2013-06-18 16:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_type`
--

CREATE TABLE `bank_account_type` (
  `account_type_id` int(10) unsigned NOT NULL,
  `account_type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checkinout`
--

CREATE TABLE `checkinout` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `checktime` datetime NOT NULL,
  `checktype` varchar(1) NOT NULL,
  `verifycode` int(11) NOT NULL,
  `SN` varchar(20) DEFAULT NULL,
  `sensorid` varchar(5) DEFAULT NULL,
  `WorkCode` varchar(20) DEFAULT NULL,
  `Reserved` varchar(20) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `checkinout`
--

INSERT INTO `checkinout` (`id`, `userid`, `checktime`, `checktype`, `verifycode`, `SN`, `sensorid`, `WorkCode`, `Reserved`) VALUES
(1, 1, '2013-10-16 23:00:09', '0', 1, '0455134200004', '1', '', ''),
(2, 1, '2013-10-16 23:15:35', '0', 1, '0455134200004', '1', '', ''),
(3, 2, '2014-05-12 16:30:47', '0', 1, '0455134200004', '1', '', ''),
(4, 2, '2014-05-12 09:32:22', '0', 1, '0455134200004', '1', '', ''),
(5, 2, '2014-05-12 09:35:31', '0', 1, '0455134200004', '1', '', ''),
(6, 2, '2014-05-12 10:16:22', '0', 1, '0455134200004', '1', '', ''),
(7, 2, '2014-05-12 12:06:44', '0', 1, '0455134200004', '1', '', ''),
(8, 2, '2014-05-14 15:44:13', '0', 1, '0455134200004', '1', '', ''),
(9, 4, '2014-05-14 15:49:36', '0', 1, '0455134200004', '1', '', ''),
(10, 5, '2014-05-14 15:57:04', '0', 1, '0455134200004', '1', '', ''),
(11, 2, '2014-05-14 16:51:09', '0', 1, '0455134200004', '1', '', ''),
(12, 2, '2014-05-14 16:51:26', '1', 1, '0455134200004', '1', '', ''),
(13, 2, '2014-05-14 16:52:11', '3', 1, '0455134200004', '1', '', ''),
(14, 4, '2014-05-14 16:53:21', '2', 1, '0455134200004', '1', '', ''),
(15, 2, '2014-05-14 16:53:44', '4', 1, '0455134200004', '1', '', ''),
(16, 4, '2014-05-14 21:10:56', '0', 1, '0455134200004', '1', '', ''),
(17, 2, '2014-05-14 21:11:41', '0', 1, '0455134200004', '1', '', ''),
(18, 5, '2014-05-14 21:12:32', '0', 1, '0455134200004', '1', '', ''),
(19, 4, '2014-05-14 21:14:11', '0', 1, '0455134200004', '1', '', ''),
(20, 4, '2014-05-14 21:41:43', '0', 1, '0455134200004', '1', '', ''),
(21, 5, '2014-05-14 21:42:16', '0', 1, '0455134200004', '1', '', ''),
(22, 2, '2014-05-14 21:52:25', '3', 1, '0455134200004', '1', '', ''),
(23, 2, '2014-05-15 00:15:11', '0', 1, '0455134200004', '1', '', ''),
(24, 2, '2014-05-15 00:15:21', '1', 1, '0455134200004', '1', '', ''),
(25, 10, '2014-05-16 00:57:03', '0', 1, '0455134200004', '1', '', ''),
(26, 10, '2014-05-16 00:58:48', '1', 1, '0455134200004', '1', '', ''),
(27, 10, '2014-05-15 18:01:48', '1', 1, '0455134200004', '1', '', ''),
(28, 9, '2014-05-15 18:03:06', '0', 1, '0455134200004', '1', '', ''),
(29, 9, '2014-05-15 18:08:33', '1', 1, '0455134200004', '1', '', ''),
(30, 9, '2014-05-15 18:36:24', '0', 1, '0455134200004', '1', '', ''),
(31, 10, '2014-05-15 18:36:36', '0', 1, '0455134200004', '1', '', ''),
(32, 10, '2014-05-16 10:30:06', '0', 1, '0455134200004', '1', '', ''),
(33, 10, '2014-05-16 10:30:19', '0', 1, '0455134200004', '1', '', ''),
(34, 9, '2014-05-16 10:30:51', '0', 1, '0455134200004', '1', '', ''),
(35, 9, '2014-05-16 10:32:21', '0', 1, '0455134200004', '1', '', '');

--
-- Triggers `checkinout`
--
DELIMITER $$
CREATE TRIGGER `trigger_Update_checkinout` AFTER INSERT ON `checkinout`
 FOR EACH ROW INSERT INTO ci.checkinout(
id,
userid,
checktime,
checktype,
verifycode,
SN,
sensorid,
WorkCode,
Reserved
)
SELECT id,
userid,
checktime,
checktype,
verifycode,
SN,
sensorid,
WorkCode,
Reserved
FROM adms_db.checkinout ORDER BY id DESC LIMIT 1
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `company_id` int(10) unsigned NOT NULL,
  `company_name` varchar(150) DEFAULT NULL,
  `ref_code` varchar(45) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `user_count` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `account_type_id` int(10) unsigned DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `company_name`, `ref_code`, `email`, `first_name`, `last_name`, `user_count`, `password`, `account_type_id`, `status`, `date_created`, `date_updated`) VALUES
(100, 'Zend Solutions', NULL, 'info@zendsolutions.com', 'Zend', 'Solutions', '500', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, '1', '2016-02-22 23:30:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_modules`
--

CREATE TABLE `company_modules` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_modules`
--

INSERT INTO `company_modules` (`id`, `module_id`, `company_id`, `status`) VALUES
(1, 1, 100, 1),
(2, 2, 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(3) unsigned NOT NULL,
  `country` varchar(200) DEFAULT NULL,
  `iso_alpha2` varchar(2) DEFAULT NULL,
  `iso_alpha3` varchar(3) DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `currency_code` char(3) DEFAULT NULL,
  `currency_name` varchar(32) DEFAULT NULL,
  `currrency_symbol` varchar(3) DEFAULT NULL,
  `flag` varchar(6) DEFAULT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country`, `iso_alpha2`, `iso_alpha3`, `iso_numeric`, `currency_code`, `currency_name`, `currrency_symbol`, `flag`, `code`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 4, 'AFN', 'Afghani', '؋', 'AF.png', 93),
(2, 'Albania', 'AL', 'ALB', 8, 'ALL', 'Lek', 'Lek', 'AL.png', 355),
(3, 'Algeria', 'DZ', 'DZA', 12, 'DZD', 'Dinar', NULL, 'DZ.png', 213),
(4, 'American Samoa', 'AS', 'ASM', 16, 'USD', 'Dollar', '$', 'AS.png', NULL),
(5, 'Andorra', 'AD', 'AND', 20, 'EUR', 'Euro', '€', 'AD.png', NULL),
(6, 'Angola', 'AO', 'AGO', 24, 'AOA', 'Kwanza', 'Kz', 'AO.png', 244),
(7, 'Anguilla', 'AI', 'AIA', 660, 'XCD', 'Dollar', '$', 'AI.png', NULL),
(8, 'Antarctica', 'AQ', 'ATA', 10, '', '', NULL, 'AQ.png', NULL),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 28, 'XCD', 'Dollar', '$', 'AG.png', 1268),
(10, 'Argentina', 'AR', 'ARG', 32, 'ARS', 'Peso', '$', 'AR.png', 54),
(11, 'Armenia', 'AM', 'ARM', 51, 'AMD', 'Dram', NULL, 'AM.png', 374),
(12, 'Aruba', 'AW', 'ABW', 533, 'AWG', 'Guilder', 'ƒ', 'AW.png', NULL),
(13, 'Australia', 'AU', 'AUS', 36, 'AUD', 'Dollar', '$', 'AU.png', NULL),
(14, 'Austria', 'AT', 'AUT', 40, 'EUR', 'Euro', '€', 'AT.png', 43),
(15, 'Azerbaijan', 'AZ', 'AZE', 31, 'AZN', 'Manat', 'ман', 'AZ.png', 994),
(16, 'Bahamas', 'BS', 'BHS', 44, 'BSD', 'Dollar', '$', 'BS.png', NULL),
(17, 'Bahrain', 'BH', 'BHR', 48, 'BHD', 'Dinar', NULL, 'BH.png', 973),
(18, 'Bangladesh', 'BD', 'BGD', 50, 'BDT', 'Taka', NULL, 'BD.png', 880),
(19, 'Barbados', 'BB', 'BRB', 52, 'BBD', 'Dollar', '$', 'BB.png', NULL),
(20, 'Belarus', 'BY', 'BLR', 112, 'BYR', 'Ruble', 'p.', 'BY.png', 1246),
(21, 'Belgium', 'BE', 'BEL', 56, 'EUR', 'Euro', '€', 'BE.png', 32),
(22, 'Belize', 'BZ', 'BLZ', 84, 'BZD', 'Dollar', 'BZ$', 'BZ.png', 501),
(23, 'Benin', 'BJ', 'BEN', 204, 'XOF', 'Franc', NULL, 'BJ.png', 229),
(24, 'Bermuda', 'BM', 'BMU', 60, 'BMD', 'Dollar', '$', 'BM.png', NULL),
(25, 'Bhutan', 'BT', 'BTN', 64, 'BTN', 'Ngultrum', NULL, 'BT.png', 975),
(26, 'Bolivia', 'BO', 'BOL', 68, 'BOB', 'Boliviano', '$b', 'BO.png', 591),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', 70, 'BAM', 'Marka', 'KM', 'BA.png', 387),
(28, 'Botswana', 'BW', 'BWA', 72, 'BWP', 'Pula', 'P', 'BW.png', 267),
(29, 'Bouvet Island', 'BV', 'BVT', 74, 'NOK', 'Krone', 'kr', 'BV.png', NULL),
(30, 'Brazil', 'BR', 'BRA', 76, 'BRL', 'Real', 'R$', 'BR.png', 55),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 86, 'USD', 'Dollar', '$', 'IO.png', NULL),
(32, 'British Virgin Islands', 'VG', 'VGB', 92, 'USD', 'Dollar', '$', 'VG.png', NULL),
(33, 'Brunei', 'BN', 'BRN', 96, 'BND', 'Dollar', '$', 'BN.png', NULL),
(34, 'Bulgaria', 'BG', 'BGR', 100, 'BGN', 'Lev', 'лв', 'BG.png', 359),
(35, 'Burkina Faso', 'BF', 'BFA', 854, 'XOF', 'Franc', NULL, 'BF.png', 226),
(36, 'Burundi', 'BI', 'BDI', 108, 'BIF', 'Franc', NULL, 'BI.png', 257),
(37, 'Cambodia', 'KH', 'KHM', 116, 'KHR', 'Riels', '៛', 'KH.png', 855),
(38, 'Cameroon', 'CM', 'CMR', 120, 'XAF', 'Franc', 'FCF', 'CM.png', 237),
(39, 'Canada', 'CA', 'CAN', 124, 'CAD', 'Dollar', '$', 'CA.png', 1),
(40, 'Cape Verde', 'CV', 'CPV', 132, 'CVE', 'Escudo', NULL, 'CV.png', 238),
(41, 'Cayman Islands', 'KY', 'CYM', 136, 'KYD', 'Dollar', '$', 'KY.png', NULL),
(42, 'Central African Republic', 'CF', 'CAF', 140, 'XAF', 'Franc', 'FCF', 'CF.png', 236),
(43, 'Chad', 'TD', 'TCD', 148, 'XAF', 'Franc', NULL, 'TD.png', 235),
(44, 'Chile', 'CL', 'CHL', 152, 'CLP', 'Peso', NULL, 'CL.png', 56),
(45, 'China', 'CN', 'CHN', 156, 'CNY', 'Yuan Renminbi', '¥', 'CN.png', 86),
(46, 'Christmas Island', 'CX', 'CXR', 162, 'AUD', 'Dollar', '$', 'CX.png', NULL),
(47, 'Cocos Islands', 'CC', 'CCK', 166, 'AUD', 'Dollar', '$', 'CC.png', NULL),
(48, 'Colombia', 'CO', 'COL', 170, 'COP', 'Peso', '$', 'CO.png', 57),
(49, 'Comoros', 'KM', 'COM', 174, 'KMF', 'Franc', NULL, 'KM.png', 269),
(50, 'Cook Islands', 'CK', 'COK', 184, 'NZD', 'Dollar', '$', 'CK.png', NULL),
(51, 'Costa Rica', 'CR', 'CRI', 188, 'CRC', 'Colon', '₡', 'CR.png', 506),
(52, 'Croatia', 'HR', 'HRV', 191, 'HRK', 'Kuna', 'kn', 'HR.png', 385),
(53, 'Cuba', 'CU', 'CUB', 192, 'CUP', 'Peso', '₱', 'CU.png', NULL),
(54, 'Cyprus', 'CY', 'CYP', 196, 'CYP', 'Pound', NULL, 'CY.png', NULL),
(55, 'Czech Republic', 'CZ', 'CZE', 203, 'CZK', 'Koruna', 'Kč', 'CZ.png', 420),
(56, 'Democratic Republic of the Congo', 'CD', 'COD', 180, 'CDF', 'Franc', NULL, 'CD.png', NULL),
(57, 'Denmark', 'DK', 'DNK', 208, 'DKK', 'Krone', 'kr', 'DK.png', 45),
(58, 'Djibouti', 'DJ', 'DJI', 262, 'DJF', 'Franc', NULL, 'DJ.png', 253),
(59, 'Dominica', 'DM', 'DMA', 212, 'XCD', 'Dollar', '$', 'DM.png', 1767),
(60, 'Dominican Republic', 'DO', 'DOM', 214, 'DOP', 'Peso', 'RD$', 'DO.png', 1809),
(61, 'East Timor', 'TL', 'TLS', 626, 'USD', 'Dollar', '$', 'TL.png', NULL),
(62, 'Ecuador', 'EC', 'ECU', 218, 'USD', 'Dollar', '$', 'EC.png', 593),
(63, 'Egypt', 'EG', 'EGY', 818, 'EGP', 'Pound', '£', 'EG.png', 20),
(64, 'El Salvador', 'SV', 'SLV', 222, 'SVC', 'Colone', '$', 'SV.png', 503),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 226, 'XAF', 'Franc', 'FCF', 'GQ.png', 240),
(66, 'Eritrea', 'ER', 'ERI', 232, 'ERN', 'Nakfa', 'Nfk', 'ER.png', 291),
(67, 'Estonia', 'EE', 'EST', 233, 'EEK', 'Kroon', 'kr', 'EE.png', 372),
(68, 'Ethiopia', 'ET', 'ETH', 231, 'ETB', 'Birr', NULL, 'ET.png', 251),
(69, 'Falkland Islands', 'FK', 'FLK', 238, 'FKP', 'Pound', '£', 'FK.png', NULL),
(70, 'Faroe Islands', 'FO', 'FRO', 234, 'DKK', 'Krone', 'kr', 'FO.png', NULL),
(71, 'Fiji', 'FJ', 'FJI', 242, 'FJD', 'Dollar', '$', 'FJ.png', 679),
(72, 'Finland', 'FI', 'FIN', 246, 'EUR', 'Euro', '€', 'FI.png', 358),
(73, 'France', 'FR', 'FRA', 250, 'EUR', 'Euro', '€', 'FR.png', 33),
(74, 'French Guiana', 'GF', 'GUF', 254, 'EUR', 'Euro', '€', 'GF.png', NULL),
(75, 'French Polynesia', 'PF', 'PYF', 258, 'XPF', 'Franc', NULL, 'PF.png', NULL),
(76, 'French Southern Territories', 'TF', 'ATF', 260, 'EUR', 'Euro  ', '€', 'TF.png', NULL),
(77, 'Gabon', 'GA', 'GAB', 266, 'XAF', 'Franc', 'FCF', 'GA.png', 241),
(78, 'Gambia', 'GM', 'GMB', 270, 'GMD', 'Dalasi', 'D', 'GM.png', 220),
(79, 'Georgia', 'GE', 'GEO', 268, 'GEL', 'Lari', NULL, 'GE.png', 995),
(80, 'Germany', 'DE', 'DEU', 276, 'EUR', 'Euro', '€', 'DE.png', 49),
(81, 'Ghana', 'GH', 'GHA', 288, 'GHC', 'Cedi', '¢', 'GH.png', 233),
(82, 'Gibraltar', 'GI', 'GIB', 292, 'GIP', 'Pound', '£', 'GI.png', NULL),
(83, 'Greece', 'GR', 'GRC', 300, 'EUR', 'Euro', '€', 'GR.png', 30),
(84, 'Greenland', 'GL', 'GRL', 304, 'DKK', 'Krone', 'kr', 'GL.png', NULL),
(85, 'Grenada', 'GD', 'GRD', 308, 'XCD', 'Dollar', '$', 'GD.png', 1473),
(86, 'Guadeloupe', 'GP', 'GLP', 312, 'EUR', 'Euro', '€', 'GP.png', NULL),
(87, 'Guam', 'GU', 'GUM', 316, 'USD', 'Dollar', '$', 'GU.png', NULL),
(88, 'Guatemala', 'GT', 'GTM', 320, 'GTQ', 'Quetzal', 'Q', 'GT.png', 502),
(89, 'Guinea', 'GN', 'GIN', 324, 'GNF', 'Franc', NULL, 'GN.png', 224),
(90, 'Guinea-Bissau', 'GW', 'GNB', 624, 'XOF', 'Franc', NULL, 'GW.png', 245),
(91, 'Guyana', 'GY', 'GUY', 328, 'GYD', 'Dollar', '$', 'GY.png', 592),
(92, 'Haiti', 'HT', 'HTI', 332, 'HTG', 'Gourde', 'G', 'HT.png', 509),
(93, 'Heard Island and McDonald Islands', 'HM', 'HMD', 334, 'AUD', 'Dollar', '$', 'HM.png', NULL),
(94, 'Honduras', 'HN', 'HND', 340, 'HNL', 'Lempira', 'L', 'HN.png', 504),
(95, 'Hong Kong', 'HK', 'HKG', 344, 'HKD', 'Dollar', '$', 'HK.png', NULL),
(96, 'Hungary', 'HU', 'HUN', 348, 'HUF', 'Forint', 'Ft', 'HU.png', 36),
(97, 'Iceland', 'IS', 'ISL', 352, 'ISK', 'Krona', 'kr', 'IS.png', 354),
(98, 'India', 'IN', 'IND', 356, 'INR', 'Rupee', '₹', 'IN.png', 91),
(99, 'Indonesia', 'ID', 'IDN', 360, 'IDR', 'Rupiah', 'Rp', 'ID.png', 62),
(100, 'Iran', 'IR', 'IRN', 364, 'IRR', 'Rial', '﷼', 'IR.png', 98),
(101, 'Iraq', 'IQ', 'IRQ', 368, 'IQD', 'Dinar', NULL, 'IQ.png', 964),
(102, 'Ireland', 'IE', 'IRL', 372, 'EUR', 'Euro', '€', 'IE.png', NULL),
(103, 'Israel', 'IL', 'ISR', 376, 'ILS', 'Shekel', '₪', 'IL.png', 972),
(104, 'Italy', 'IT', 'ITA', 380, 'EUR', 'Euro', '€', 'IT.png', 39),
(105, 'Ivory Coast', 'CI', 'CIV', 384, 'XOF', 'Franc', NULL, 'CI.png', NULL),
(106, 'Jamaica', 'JM', 'JAM', 388, 'JMD', 'Dollar', '$', 'JM.png', 1876),
(107, 'Japan', 'JP', 'JPN', 392, 'JPY', 'Yen', '¥', 'JP.png', 81),
(108, 'Jordan', 'JO', 'JOR', 400, 'JOD', 'Dinar', NULL, 'JO.png', 962),
(109, 'Kazakhstan', 'KZ', 'KAZ', 398, 'KZT', 'Tenge', 'лв', 'KZ.png', 7),
(110, 'Kenya', 'KE', 'KEN', 404, 'KES', 'Shilling', NULL, 'KE.png', 254),
(111, 'Kiribati', 'KI', 'KIR', 296, 'AUD', 'Dollar', '$', 'KI.png', 686),
(112, 'Kuwait', 'KW', 'KWT', 414, 'KWD', 'Dinar', NULL, 'KW.png', 965),
(113, 'Kyrgyzstan', 'KG', 'KGZ', 417, 'KGS', 'Som', 'лв', 'KG.png', NULL),
(114, 'Laos', 'LA', 'LAO', 418, 'LAK', 'Kip', '₭', 'LA.png', NULL),
(115, 'Latvia', 'LV', 'LVA', 428, 'LVL', 'Lat', 'Ls', 'LV.png', 371),
(116, 'Lebanon', 'LB', 'LBN', 422, 'LBP', 'Pound', '£', 'LB.png', 961),
(117, 'Lesotho', 'LS', 'LSO', 426, 'LSL', 'Loti', 'L', 'LS.png', 266),
(118, 'Liberia', 'LR', 'LBR', 430, 'LRD', 'Dollar', '$', 'LR.png', 231),
(119, 'Libya', 'LY', 'LBY', 434, 'LYD', 'Dinar', NULL, 'LY.png', 218),
(120, 'Liechtenstein', 'LI', 'LIE', 438, 'CHF', 'Franc', 'CHF', 'LI.png', NULL),
(121, 'Lithuania', 'LT', 'LTU', 440, 'LTL', 'Litas', 'Lt', 'LT.png', 370),
(122, 'Luxembourg', 'LU', 'LUX', 442, 'EUR', 'Euro', '€', 'LU.png', 352),
(123, 'Macao', 'MO', 'MAC', 446, 'MOP', 'Pataca', 'MOP', 'MO.png', NULL),
(124, 'Macedonia', 'MK', 'MKD', 807, 'MKD', 'Denar', 'ден', 'MK.png', NULL),
(125, 'Madagascar', 'MG', 'MDG', 450, 'MGA', 'Ariary', NULL, 'MG.png', 261),
(126, 'Malawi', 'MW', 'MWI', 454, 'MWK', 'Kwacha', 'MK', 'MW.png', 265),
(127, 'Malaysia', 'MY', 'MYS', 458, 'MYR', 'Ringgit', 'RM', 'MY.png', 60),
(128, 'Maldives', 'MV', 'MDV', 462, 'MVR', 'Rufiyaa', 'Rf', 'MV.png', 960),
(129, 'Mali', 'ML', 'MLI', 466, 'XOF', 'Franc', NULL, 'ML.png', 223),
(130, 'Malta', 'MT', 'MLT', 470, 'MTL', 'Lira', NULL, 'MT.png', NULL),
(131, 'Marshall Islands', 'MH', 'MHL', 584, 'USD', 'Dollar', '$', 'MH.png', 692),
(132, 'Martinique', 'MQ', 'MTQ', 474, 'EUR', 'Euro', '€', 'MQ.png', NULL),
(133, 'Mauritania', 'MR', 'MRT', 478, 'MRO', 'Ouguiya', 'UM', 'MR.png', 222),
(134, 'Mauritius', 'MU', 'MUS', 480, 'MUR', 'Rupee', '₨', 'MU.png', 230),
(135, 'Mayotte', 'YT', 'MYT', 175, 'EUR', 'Euro', '€', 'YT.png', NULL),
(136, 'Mexico', 'MX', 'MEX', 484, 'MXN', 'Peso', '$', 'MX.png', 52),
(137, 'Micronesia', 'FM', 'FSM', 583, 'USD', 'Dollar', '$', 'FM.png', NULL),
(138, 'Moldova', 'MD', 'MDA', 498, 'MDL', 'Leu', NULL, 'MD.png', 373),
(139, 'Monaco', 'MC', 'MCO', 492, 'EUR', 'Euro', '€', 'MC.png', NULL),
(140, 'Mongolia', 'MN', 'MNG', 496, 'MNT', 'Tugrik', '₮', 'MN.png', 976),
(141, 'Montserrat', 'MS', 'MSR', 500, 'XCD', 'Dollar', '$', 'MS.png', NULL),
(142, 'Morocco', 'MA', 'MAR', 504, 'MAD', 'Dirham', NULL, 'MA.png', 212),
(143, 'Mozambique', 'MZ', 'MOZ', 508, 'MZN', 'Meticail', 'MT', 'MZ.png', 258),
(144, 'Myanmar', 'MM', 'MMR', 104, 'MMK', 'Kyat', 'K', 'MM.png', 0),
(145, 'Namibia', 'NA', 'NAM', 516, 'NAD', 'Dollar', '$', 'NA.png', 264),
(146, 'Nauru', 'NR', 'NRU', 520, 'AUD', 'Dollar', '$', 'NR.png', NULL),
(147, 'Nepal', 'NP', 'NPL', 524, 'NPR', 'Rupee', '₨', 'NP.png', 977),
(148, 'Netherlands', 'NL', 'NLD', 528, 'EUR', 'Euro', '€', 'NL.png', 31),
(149, 'Netherlands Antilles', 'AN', 'ANT', 530, 'ANG', 'Guilder', 'ƒ', 'AN.png', NULL),
(150, 'New Caledonia', 'NC', 'NCL', 540, 'XPF', 'Franc', NULL, 'NC.png', NULL),
(151, 'New Zealand', 'NZ', 'NZL', 554, 'NZD', 'Dollar', '$', 'NZ.png', NULL),
(152, 'Nicaragua', 'NI', 'NIC', 558, 'NIO', 'Cordoba', 'C$', 'NI.png', 505),
(153, 'Niger', 'NE', 'NER', 562, 'XOF', 'Franc', NULL, 'NE.png', 227),
(154, 'Nigeria', 'NG', 'NGA', 566, 'NGN', 'Naira', '₦', 'NG.png', 234),
(155, 'Niue', 'NU', 'NIU', 570, 'NZD', 'Dollar', '$', 'NU.png', NULL),
(156, 'Norfolk Island', 'NF', 'NFK', 574, 'AUD', 'Dollar', '$', 'NF.png', NULL),
(157, 'North Korea', 'KP', 'PRK', 408, 'KPW', 'Won', '₩', 'KP.png', NULL),
(158, 'Northern Mariana Islands', 'MP', 'MNP', 580, 'USD', 'Dollar', '$', 'MP.png', NULL),
(159, 'Norway', 'NO', 'NOR', 578, 'NOK', 'Krone', 'kr', 'NO.png', 47),
(160, 'Oman', 'OM', 'OMN', 512, 'OMR', 'Rial', '﷼', 'OM.png', 968),
(161, 'Pakistan', 'PK', 'PAK', 586, 'PKR', 'Rupee', '₨', 'PK.png', 92),
(162, 'Palau', 'PW', 'PLW', 585, 'USD', 'Dollar', '$', 'PW.png', 680),
(163, 'Palestinian Territory', 'PS', 'PSE', 275, 'ILS', 'Shekel', '₪', 'PS.png', NULL),
(164, 'Panama', 'PA', 'PAN', 591, 'PAB', 'Balboa', 'B/.', 'PA.png', 507),
(165, 'Papua New Guinea', 'PG', 'PNG', 598, 'PGK', 'Kina', NULL, 'PG.png', 675),
(166, 'Paraguay', 'PY', 'PRY', 600, 'PYG', 'Guarani', 'Gs', 'PY.png', 595),
(167, 'Peru', 'PE', 'PER', 604, 'PEN', 'Sol', 'S/.', 'PE.png', 51),
(168, 'Philippines', 'PH', 'PHL', 608, 'PHP', 'Peso', 'Php', 'PH.png', 63),
(169, 'Pitcairn', 'PN', 'PCN', 612, 'NZD', 'Dollar', '$', 'PN.png', NULL),
(170, 'Poland', 'PL', 'POL', 616, 'PLN', 'Zloty', 'zł', 'PL.png', 48),
(171, 'Portugal', 'PT', 'PRT', 620, 'EUR', 'Euro', '€', 'PT.png', 351),
(172, 'Puerto Rico', 'PR', 'PRI', 630, 'USD', 'Dollar', '$', 'PR.png', NULL),
(173, 'Qatar', 'QA', 'QAT', 634, 'QAR', 'Rial', '﷼', 'QA.png', 974),
(174, 'Republic of the Congo', 'CG', 'COG', 178, 'XAF', 'Franc', 'FCF', 'CG.png', NULL),
(175, 'Reunion', 'RE', 'REU', 638, 'EUR', 'Euro', '€', 'RE.png', NULL),
(176, 'Romania', 'RO', 'ROU', 642, 'RON', 'Leu', 'lei', 'RO.png', 40),
(177, 'Russia', 'RU', 'RUS', 643, 'RUB', 'Ruble', 'руб', 'RU.png', 7),
(178, 'Rwanda', 'RW', 'RWA', 646, 'RWF', 'Franc', NULL, 'RW.png', 250),
(179, 'Saint Helena', 'SH', 'SHN', 654, 'SHP', 'Pound', '£', 'SH.png', NULL),
(180, 'Saint Kitts and Nevis', 'KN', 'KNA', 659, 'XCD', 'Dollar', '$', 'KN.png', NULL),
(181, 'Saint Lucia', 'LC', 'LCA', 662, 'XCD', 'Dollar', '$', 'LC.png', NULL),
(184, 'Samoa', 'WS', 'WSM', 882, 'WST', 'Tala', 'WS$', 'WS.png', 685),
(185, 'San Marino', 'SM', 'SMR', 674, 'EUR', 'Euro', '€', 'SM.png', NULL),
(186, 'Sao Tome and Principe', 'ST', 'STP', 678, 'STD', 'Dobra', 'Db', 'ST.png', 239),
(187, 'Saudi Arabia', 'SA', 'SAU', 682, 'SAR', 'Rial', '﷼', 'SA.png', NULL),
(188, 'Senegal', 'SN', 'SEN', 686, 'XOF', 'Franc', NULL, 'SN.png', 221),
(189, 'Serbia and Montenegro', 'CS', 'SCG', 891, 'RSD', 'Dinar', 'Дин', 'CS.png', NULL),
(190, 'Seychelles', 'SC', 'SYC', 690, 'SCR', 'Rupee', '₨', 'SC.png', 248),
(191, 'Sierra Leone', 'SL', 'SLE', 694, 'SLL', 'Leone', 'Le', 'SL.png', 232),
(192, 'Singapore', 'SG', 'SGP', 702, 'SGD', 'Dollar', '$', 'SG.png', 65),
(193, 'Slovakia', 'SK', 'SVK', 703, 'SKK', 'Koruna', 'Sk', 'SK.png', NULL),
(194, 'Slovenia', 'SI', 'SVN', 705, 'EUR', 'Euro', '€', 'SI.png', 386),
(195, 'Solomon Islands', 'SB', 'SLB', 90, 'SBD', 'Dollar', '$', 'SB.png', 677),
(196, 'Somalia', 'SO', 'SOM', 706, 'SOS', 'Shilling', 'S', 'SO.png', 252),
(197, 'South Africa', 'ZA', 'ZAF', 710, 'ZAR', 'Rand', 'R', 'ZA.png', 27),
(199, 'South Korea', 'KR', 'KOR', 410, 'KRW', 'Won', '₩', 'KR.png', NULL),
(200, 'Spain', 'ES', 'ESP', 724, 'EUR', 'Euro', '€', 'ES.png', 34),
(201, 'Sri Lanka', 'LK', 'LKA', 144, 'LKR', 'Rupee', '₨', 'LK.png', 94),
(202, 'Sudan', 'SD', 'SDN', 736, 'SDD', 'Dinar', NULL, 'SD.png', 249),
(203, 'Suriname', 'SR', 'SUR', 740, 'SRD', 'Dollar', '$', 'SR.png', 597),
(204, 'Svalbard and Jan Mayen', 'SJ', 'SJM', 744, 'NOK', 'Krone', 'kr', 'SJ.png', NULL),
(205, 'Swaziland', 'SZ', 'SWZ', 748, 'SZL', 'Lilangeni', NULL, 'SZ.png', 268),
(206, 'Sweden', 'SE', 'SWE', 752, 'SEK', 'Krona', 'kr', 'SE.png', 46),
(207, 'Switzerland', 'CH', 'CHE', 756, 'CHF', 'Franc', 'CHF', 'CH.png', 41),
(208, 'Syria', 'SY', 'SYR', 760, 'SYP', 'Pound', '£', 'SY.png', 963),
(209, 'Taiwan', 'TW', 'TWN', 158, 'TWD', 'Dollar', 'NT$', 'TW.png', NULL),
(210, 'Tajikistan', 'TJ', 'TJK', 762, 'TJS', 'Somoni', NULL, 'TJ.png', 992),
(211, 'Tanzania', 'TZ', 'TZA', 834, 'TZS', 'Shilling', NULL, 'TZ.png', 255),
(212, 'Thailand', 'TH', 'THA', 764, 'THB', 'Baht', '฿', 'TH.png', 66),
(213, 'Togo', 'TG', 'TGO', 768, 'XOF', 'Franc', NULL, 'TG.png', 228),
(214, 'Tokelau', 'TK', 'TKL', 772, 'NZD', 'Dollar', '$', 'TK.png', NULL),
(215, 'Tonga', 'TO', 'TON', 776, 'TOP', 'Pa''anga', 'T$', 'TO.png', 676),
(216, 'Trinidad and Tobago', 'TT', 'TTO', 780, 'TTD', 'Dollar', 'TT$', 'TT.png', 1868),
(217, 'Tunisia', 'TN', 'TUN', 788, 'TND', 'Dinar', NULL, 'TN.png', 216),
(218, 'Turkey', 'TR', 'TUR', 792, 'TRY', 'Lira', 'YTL', 'TR.png', NULL),
(219, 'Turkmenistan', 'TM', 'TKM', 795, 'TMM', 'Manat', 'm', 'TM.png', 993),
(221, 'Tuvalu', 'TV', 'TUV', 798, 'AUD', 'Dollar', '$', 'TV.png', 688),
(222, 'U.S. Virgin Islands', 'VI', 'VIR', 850, 'USD', 'Dollar', '$', 'VI.png', NULL),
(223, 'Uganda', 'UG', 'UGA', 800, 'UGX', 'Shilling', NULL, 'UG.png', 256),
(224, 'Ukraine', 'UA', 'UKR', 804, 'UAH', 'Hryvnia', '₴', 'UA.png', 380),
(225, 'United Arab Emirates', 'AE', 'ARE', 784, 'AED', 'Dirham', NULL, 'AE.png', 971),
(226, 'United Kingdom', 'GB', 'GBR', 826, 'GBP', 'Pound', '£', 'GB.png', 44),
(227, 'United States', 'US', 'USA', 840, 'USD', 'Dollar', '$', 'US.png', NULL),
(229, 'Uruguay', 'UY', 'URY', 858, 'UYU', 'Peso', '$U', 'UY.png', 598),
(230, 'Uzbekistan', 'UZ', 'UZB', 860, 'UZS', 'Som', 'лв', 'UZ.png', 998),
(231, 'Vanuatu', 'VU', 'VUT', 548, 'VUV', 'Vatu', 'Vt', 'VU.png', 678),
(232, 'Vatican', 'VA', 'VAT', 336, 'EUR', 'Euro', '€', 'VA.png', NULL),
(233, 'Venezuela', 'VE', 'VEN', 862, 'VEF', 'Bolivar', 'Bs', 'VE.png', 58),
(234, 'Vietnam', 'VN', 'VNM', 704, 'VND', 'Dong', '₫', 'VN.png', 84),
(235, 'Wallis and Futuna', 'WF', 'WLF', 876, 'XPF', 'Franc', NULL, 'WF.png', NULL),
(236, 'Western Sahara', 'EH', 'ESH', 732, 'MAD', 'Dirham', NULL, 'EH.png', NULL),
(237, 'Yemen', 'YE', 'YEM', 887, 'YER', 'Rial', '﷼', 'YE.png', 967),
(238, 'Zambia', 'ZM', 'ZMB', 894, 'ZMK', 'Kwacha', 'ZK', 'ZM.png', 260),
(239, 'Zimbabwe', 'ZW', 'ZWE', 716, 'ZWD', 'Dollar', 'Z$', 'ZW.png', 263);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` int(10) unsigned NOT NULL,
  `department` varchar(150) DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department`, `company_id`, `date_created`, `date_updated`, `creator_id`, `parent_id`) VALUES
(1, 'Corporate', 100, '2016-02-24 00:00:00', NULL, 18, 0),
(3, 'Admin', 100, '2016-02-25 00:00:00', NULL, 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `devcmds`
--

CREATE TABLE `devcmds` (
  `id` int(11) NOT NULL,
  `SN_id` varchar(20) NOT NULL,
  `CmdContent` longtext NOT NULL,
  `CmdCommitTime` datetime NOT NULL,
  `CmdTransTime` datetime DEFAULT NULL,
  `CmdOverTime` datetime DEFAULT NULL,
  `CmdReturn` int(11) DEFAULT NULL,
  `User_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `devcmds`
--

INSERT INTO `devcmds` (`id`, `SN_id`, `CmdContent`, `CmdCommitTime`, `CmdTransTime`, `CmdOverTime`, `CmdReturn`, `User_id`) VALUES
(33, '0455134200004', 'DATA DEL_USER PIN=178', '2015-11-03 10:55:59', NULL, NULL, NULL, NULL),
(34, '0455134200004', 'DATA USER PIN=222	Name=Karlen Kirakosyan	Pri=0		Card=[0000000000]	TZ=0000000000000000	Grp=1', '2015-11-03 10:55:59', NULL, NULL, NULL, NULL),
(35, '0455134200004', 'DATA FP PIN=222	FID=0	Size=1124	Valid=1	TMP=SgdTUzIxAAADREcECAUHCc7QAAAbRWkBAAAAg+kZj0QpAAQPtQCEAItLNABaAHwPIQBvRI0PgQBxAEQP20R4AA8PkwC6AAtLyACMAIwPggCoRPoPqgCvAEIOrES9AA4OwAAHAI5KWQDEAHEPTADQRH4P+ADmAFMPqET4AIgPpQDIAYFKjAAaAQAOZAAZRYEOdQAjAZoPrEQwAYkOmwD5AWlKWgBCAdoPcwBKRX4PiwBPATIPlDjGh7v/k4Te/yA/Ev8/g/N7kH1RR5YHHXrj+wJ/UsHMhh+LFYpigYA60H7SA1MEa4ycO2Z/LXgZ/x78JsyAgd6TgYFUC7hGu3gf9QfvK3PgvCb5gwtjE0aLRk1HCVqLTQQM/WrDgIHxo22DCACCxSj7FWN+ht/jZmYsDqELAeKwdimoh36z5Y/fcPqVUH+LZY6Ge6pwA9SBASAsAQEzHRpAAZshg38DxaQoQv4LAGYu+jr9ZrtWBwBhMgkHN8NBAVc0ff9dygAxegFYwS9t//ITA0tL/VL//8E6RVcPwxIABlnwOMD+gD5MwFhGA8UwWT7BEwAJZQZW/MIDwVD//8FdzwB/NIHAa3jAFMULdrBVVMBDVMD7wQdE2noMNQQAU4AAexcADIzwPQVUWHk2VMAWAAlf7UgQRv9J//7B88AVRA2m7Uo2OI9UVwgJAEyq9zUE//xTAQe46cD/g/9Hu0JE/8A+CMVUwTPC/8DBhxDFXMe+wf1VTFhEzQBVjHFrwXYZAMfO33VWQcA9WD4F/sC5GQAI0+IvBf38hE1i/f7BS6ANA8HSgMDDfv9MbBZE9eOawv/CB/3ChsHAwlnCakYPA+z0kMN1g8EHwlFKAaf7g2fCBv/AOEgJAK/8EAU3wbr8CxCgDIMEwsKHwMPBwP8Z1BMO3mtiwHT+mQHAwzXAgRgRDBZZwcK7cWLCwsDCBcNOIMAXEQkgmgf+woX+eMDBk8Ksc8JAESohWoQD1XEmJsMJEKovjAbCxoXDKhUQ+jJTwVaFbcGCwnzB6QQT0j1tfxMQ9oWaeIX+joDDwsIF+v5AEbJFiasP1e9G13L/fcDGwqENE6JLicH8h8IDwsG5/gMQskt6BQMTjVp6/wAAAAAAAAA=', '2015-11-03 10:55:59', NULL, NULL, NULL, NULL),
(36, '0455134200004', 'DATA DEL_USER PIN=10', '2015-11-03 11:04:12', NULL, NULL, NULL, NULL),
(37, '0455134200004', 'DATA USER PIN=11	Name=Andrea Sepotero	Pri=0		Card=[0000000000]	TZ=0000000000000000	Grp=1', '2015-11-03 11:04:12', NULL, NULL, NULL, NULL),
(38, '0455134200004', 'DATA FP PIN=11	FID=0	Size=1528	Valid=1	TMP=TTFTUzIxAAAEcnMECAUHCc7QAAAcc2kBAAAAhJ8prHIrAJcPcwDrAIl9ZAA8AIAPWgBDcpcPewBTAEMPSnJsAGoPqwCwAJR9PwCFAO0PbACPcpMPbACgAM8PgnKtAIoP5QB5ABZ9QgC9AOYPCQDEcicPagDPAMQOhXLRAIIO1AAXAKF9wgDTAJ0PagDYcpcPhwDiALkNDXPvAJsPYQA0AOt9eADzAAkO/QDzct0PjAD3AEwOJ3L/AF8O8AA7AKJ87gAFAZgOJQAOcxkOgQAhAcgNbHIiAe8OkADjAYp/pQAoAToNAQAvcx4LSQA1AZMNnHI5AaIMVwD+AeZ5owA8ATQMJgBDc6IMZwBKATkNqXJRAZcLNgCuDUthVA0uCgv5JPUmjlYaugBeEuYImnqz+u7ycX/G5e+cNAKn/IsPqIevl9IKsQZTCN5vjfG6CN8liYP7CVb56O7Jb9Z2a+eHr1r6iYGtiZAWhfCbhcoVMQlEgk4LWAiNgbWXJPixC1AK2fri8gtyvY889mF2TvSPBUv3lOpe8YJ2RIFR/PweGX6i7qr7hfOsDq4KFYbec095IRJBkcYMafZZ8s/9XYBNdQP7hfLwIpZsjeYoD/rrgIAh7flVZJnJ34wJ5uTS7kd/OQYXaUZd7HNIFb0bMIy96nn3GZcVj3iefodbDlqPIWwfqUYOlWZL9KYsWAGIVr8B5T0FcGMekgYAbsAPPLMHAIMFF/+ZBgQbCBPAQgsAmw0UNlllCwBdFdVMV42JCABOGwbuwE53ATsiAz0DxawqaP4KAC4w9Dv+M438wg8ANjcxKkCNXMFDBQBh+YOPdwGjShp0EMUvSZ8wwPxVYGTNAHMikZ3CewoAulUXssHA/mv+CMV0UvvDkIQFAEqqbXp+Aadxk8FuB8GEsw0ApHaQwAWIhPAGAK95GmQF/w1yQITtwSP9nA0E142QwIbDwQXAwI3ABwCtjhwH/nRnASOV4sBD5ET6jV3/gggAcFoJ+o1FwAYAg6pMwcDlGAAZu9o4Ojv7WkXAS8HD/JAGBJu/FsD//P/NANSxJsF4VxIAF8+mGG7Aw8TAwgdpxX4BqtqXwMBdw8UKDACD3oPDAcUys2/EGQAY4AX8+4w/L/z9/8E4wMWMw/7/wHcZxSDgpEU9/f/7/vjAxIzDwGVXAwDZ5VqyAgCC5YDDzwCPlxL/RVD+EMU886xSwPoewML3wRByJ/rcXTv+Of36s/5QwMDAA8U1/yXBFwDs/aIEwcSzUcDDwMLFB2nFPcMEAB7/ZLIDFFYDXsEDEPTHFvp2EeQJDCgE1eIKbkYGEGgf4jj6J3QRbCTt//k++RRiwCekwlzEOsjHJIkDEKgpNAUEFLoqHD0DEMPqK8B/EUsw4GL7PvzEjP/9/gQRBvVWXXQRkzSexcYFxBdiDTkrwcLFBML4t0/8gP5dGdX4PcjJ/nD/wMGoxcW3wlvCwP6VwxCVMKbHxagWEPREwo/+NzT9+/87wzaw/v35BBBog+kSdhFrS/0mAAAAAAAAAA==', '2015-11-03 11:04:12', NULL, NULL, NULL, NULL),
(39, '0421142600037', 'CLEAR DATA', '2015-11-17 09:13:39', '2015-11-17 09:13:39', '2015-11-17 09:13:40', 0, NULL),
(40, '0421142600037', 'DATA USER PIN=23	Name=sugun ige	Pri=null	Card=[0000000000]	Grp=1', '2015-11-17 09:38:04', '2015-11-17 09:39:20', '2015-11-17 09:39:21', 0, NULL),
(41, '0421142600037', 'CHECK', '2015-11-17 09:38:04', '2015-11-17 09:39:20', '2015-11-17 09:39:21', 0, NULL),
(42, '0421142600037', 'DATA USER PIN=15	Name=ADEDOKUN OPEYEMI	Pri=null	Card=[0000000000]	Grp=1', '2015-11-18 12:24:26', NULL, NULL, NULL, NULL),
(43, '0421142600037', 'CHECK', '2015-11-18 12:24:26', NULL, NULL, NULL, NULL),
(44, '0421142600037', 'DATA USER PIN=20	Name=taiwo Oko-osi	Pri=null	Card=[0000000000]	Grp=1', '2015-11-18 12:24:27', NULL, NULL, NULL, NULL),
(45, '0421142600037', 'CHECK', '2015-11-18 12:24:27', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `devlog`
--

CREATE TABLE `devlog` (
  `id` int(11) NOT NULL,
  `SN_id` varchar(20) NOT NULL,
  `OP` varchar(8) NOT NULL,
  `Object` varchar(20) DEFAULT NULL,
  `Cnt` int(11) NOT NULL,
  `ECnt` int(11) NOT NULL,
  `OpTime` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `devlog`
--

INSERT INTO `devlog` (`id`, `SN_id`, `OP`, `Object`, `Cnt`, `ECnt`, `OpTime`) VALUES
(1, '0455134200004', 'USERDATA', '000000003  ', 3, 4, '2014-05-15 10:46:13'),
(2, '0455134200004', 'USERDATA', '000000004  ', 1, 7, '2014-05-15 10:46:13'),
(3, '0455134200004', 'TRANSACT', '1	2013-10-16 23:00:0', 24, 0, '2014-05-15 10:46:17'),
(4, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 10:46:17'),
(5, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 10:49:47'),
(6, '0455134200004', 'USERDATA', '000000003 John Mikel', 7, 0, '2014-05-15 10:59:05'),
(7, '0455134200004', 'USERDATA', '000000009 Jeki Surma', 8, 0, '2014-05-15 10:59:05'),
(8, '0455134200004', 'TRANSACT', '1	2013-10-16 23:00:0', 24, 0, '2014-05-15 10:59:09'),
(9, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 10:59:09'),
(10, '0455134200004', 'USERDATA', 'None', 40, 0, '2014-05-15 11:13:25'),
(11, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 11:14:26'),
(12, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 11:14:56'),
(13, '0455134200004', 'USERDATA', 'None', 10, 0, '2014-05-15 11:16:36'),
(14, '0455134200004', 'USERDATA', '000000398 Smith', 2, 0, '2014-05-15 14:02:44'),
(15, '0455134200004', 'TRANSACT', '', 0, 0, '2014-05-15 14:02:45'),
(16, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 14:02:50'),
(17, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 17:50:28'),
(18, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 17:50:29'),
(19, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 17:51:31'),
(20, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 17:51:31'),
(21, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-15 17:55:41'),
(22, '0455134200004', 'USERDATA', 'None', 9, 0, '2014-05-15 17:57:12'),
(23, '0455134200004', 'TRANSACT', '17	2014-05-16 00:57:', 1, 0, '2014-05-15 17:57:22'),
(24, '0455134200004', 'TRANSACT', '17	2014-05-16 00:58:', 1, 0, '2014-05-15 17:58:52'),
(25, '0455134200004', 'USERDATA', '000000017 Karlen Kir', 83, 0, '2014-05-15 18:00:02'),
(26, '0455134200004', 'USERDATA', 'None', 200, 0, '2014-05-15 18:00:13'),
(27, '0455134200004', 'USERDATA', 'None', 142, 0, '2014-05-15 18:00:23'),
(28, '0455134200004', 'TRANSACT', '', 0, 0, '2014-05-15 18:00:23'),
(29, '0455134200004', 'USERDATA', 'None', 142, 0, '2014-05-15 18:00:23'),
(30, '0455134200004', 'USERDATA', 'None', 152, 0, '2014-05-15 18:00:49'),
(31, '0455134200004', 'TRANSACT', '', 0, 0, '2014-05-15 18:00:49'),
(32, '0455134200004', 'TRANSACT', '17	2014-05-15 18:01:', 1, 0, '2014-05-15 18:01:53'),
(33, '0455134200004', 'USERDATA', '000000010 Andrea Sep', 4, 0, '2014-05-15 18:03:09'),
(34, '0455134200004', 'USERDATA', 'None', 4, 0, '2014-05-15 18:03:09'),
(35, '0455134200004', 'TRANSACT', '10	2014-05-15 18:03:', 1, 0, '2014-05-15 18:03:19'),
(36, '0455134200004', 'TRANSACT', '10	2014-05-15 18:08:', 1, 0, '2014-05-15 18:08:39'),
(37, '0455134200004', 'TRANSACT', '10	2014-05-15 18:36:', 1, 0, '2014-05-15 18:36:28'),
(38, '0455134200004', 'TRANSACT', '17	2014-05-15 18:36:', 1, 0, '2014-05-15 18:36:46'),
(39, '0455134200004', 'USERDATA', 'None', 68, 0, '2014-05-16 10:30:22'),
(40, '0455134200004', 'TRANSACT', '17	2014-05-16 10:30:', 1, 0, '2014-05-16 10:30:22'),
(41, '0455134200004', 'TRANSACT', '17	2014-05-16 10:30:', 1, 0, '2014-05-16 10:30:22'),
(42, '0455134200004', 'TRANSACT', '10	2014-05-16 10:30:', 1, 0, '2014-05-16 10:31:02'),
(43, '0455134200004', 'TRANSACT', '10	2014-05-16 10:32:', 1, 0, '2014-05-16 10:32:33'),
(44, '0455134200004', 'USERDATA', 'None', 2, 0, '2014-05-16 11:12:49'),
(45, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-16 11:14:29'),
(46, '0455134200004', 'USERDATA', 'None', 1, 0, '2014-05-16 11:25:43'),
(47, '0421142600037', 'TRANSACT', '8	2015-11-16 20:22:1', 2, 0, '2015-11-17 08:53:42'),
(48, '0421142600037', 'USERDATA', '000000023 sugun ige', 1, 0, '2015-11-17 09:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'session', 'sessions', 'session'),
(2, 'permission', 'auth', 'permission'),
(3, 'group', 'auth', 'group'),
(4, 'user', 'auth', 'user'),
(5, 'message', 'auth', 'message'),
(6, 'content type', 'contenttypes', 'contenttype'),
(7, 'log entry', 'admin', 'logentry'),
(8, 'department', 'iclock', 'department'),
(9, 'device', 'iclock', 'iclock'),
(10, 'admin granted department', 'iclock', 'deptadmin'),
(11, 'employee', 'iclock', 'employee'),
(12, 'fingerprint', 'iclock', 'fptemp'),
(13, 'transaction', 'iclock', 'transaction'),
(14, 'device operation log', 'iclock', 'oplog'),
(15, 'data from device', 'iclock', 'devlog'),
(16, 'command to device', 'iclock', 'devcmds'),
(17, 'public information', 'iclock', 'messages'),
(18, 'information subscription', 'iclock', 'iclockmsg'),
(19, 'administration log', 'iclock', 'adminlog');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `userid` int(10) unsigned NOT NULL,
  `staff_id` varchar(20) NOT NULL,
  `employment_date` date NOT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`userid`, `staff_id`, `employment_date`, `company_id`, `department_id`, `location_id`, `role_id`, `level_id`, `date_created`, `date_updated`) VALUES
(1, '99488', '2016-01-31', NULL, 1, 1, 1, 1, '2016-02-25 04:25:08', '0000-00-00 00:00:00'),
(18, '88943', '2016-02-01', 100, 3, 1, 2, 2, '2016-02-25 04:28:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_bank_details`
--

CREATE TABLE `employee_bank_details` (
  `id` int(10) unsigned NOT NULL,
  `bank_id` int(10) unsigned DEFAULT NULL,
  `account_type_id` int(10) unsigned DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `account_number` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_kin_details`
--

CREATE TABLE `employee_kin_details` (
  `employee_kin_detail_id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned DEFAULT NULL,
  `kin_first_name` varchar(150) DEFAULT NULL,
  `kin_last_name` varchar(150) DEFAULT NULL,
  `relationship_id` int(10) unsigned DEFAULT NULL,
  `kin_email` varchar(45) DEFAULT NULL,
  `kin_phone` varchar(45) DEFAULT NULL,
  `kin_address` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `gender_id` int(25) NOT NULL,
  `gender` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`gender_id`, `gender`) VALUES
(1, 'Male'),
(2, 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `gender_id` int(10) unsigned NOT NULL,
  `gender` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iclock`
--

CREATE TABLE `iclock` (
  `SN` varchar(20) NOT NULL,
  `State` int(11) NOT NULL,
  `LastActivity` datetime DEFAULT NULL,
  `TransTimes` varchar(50) DEFAULT NULL,
  `TransInterval` int(11) NOT NULL,
  `LogStamp` varchar(20) DEFAULT NULL,
  `OpLogStamp` varchar(20) DEFAULT NULL,
  `PhotoStamp` varchar(20) DEFAULT NULL,
  `Alias` varchar(20) NOT NULL,
  `DeptID` int(11) DEFAULT NULL,
  `UpdateDB` varchar(10) NOT NULL,
  `Style` varchar(20) DEFAULT NULL,
  `FWVersion` varchar(30) DEFAULT NULL,
  `FPCount` int(11) DEFAULT NULL,
  `TransactionCount` int(11) DEFAULT NULL,
  `UserCount` int(11) DEFAULT NULL,
  `MainTime` varchar(20) DEFAULT NULL,
  `MaxFingerCount` int(11) DEFAULT NULL,
  `MaxAttLogCount` int(11) DEFAULT NULL,
  `DeviceName` varchar(30) DEFAULT NULL,
  `AlgVer` varchar(30) DEFAULT NULL,
  `FlashSize` varchar(10) DEFAULT NULL,
  `FreeFlashSize` varchar(10) DEFAULT NULL,
  `Language` varchar(30) DEFAULT NULL,
  `VOLUME` varchar(10) DEFAULT NULL,
  `DtFmt` varchar(10) DEFAULT NULL,
  `IPAddress` varchar(20) DEFAULT NULL,
  `IsTFT` varchar(5) DEFAULT NULL,
  `Platform` varchar(20) DEFAULT NULL,
  `Brightness` varchar(5) DEFAULT NULL,
  `BackupDev` varchar(30) DEFAULT NULL,
  `OEMVendor` varchar(30) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `AccFun` smallint(6) NOT NULL,
  `TZAdj` smallint(6) NOT NULL,
  `DelTag` smallint(6) NOT NULL,
  `FPVersion` varchar(10) DEFAULT NULL,
  `PushVersion` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `iclock`
--

INSERT INTO `iclock` (`SN`, `State`, `LastActivity`, `TransTimes`, `TransInterval`, `LogStamp`, `OpLogStamp`, `PhotoStamp`, `Alias`, `DeptID`, `UpdateDB`, `Style`, `FWVersion`, `FPCount`, `TransactionCount`, `UserCount`, `MainTime`, `MaxFingerCount`, `MaxAttLogCount`, `DeviceName`, `AlgVer`, `FlashSize`, `FreeFlashSize`, `Language`, `VOLUME`, `DtFmt`, `IPAddress`, `IsTFT`, `Platform`, `Brightness`, `BackupDev`, `OEMVendor`, `City`, `AccFun`, `TZAdj`, `DelTag`, `FPVersion`, `PushVersion`) VALUES
('0455134200004', 1, '2014-05-16 11:28:05', '00:00;14:05', 1, '462018741', '462021871', '', '192.168.1.201', 1, '1111111100', 'F7', 'Ver 6.5.4(build 130)', 2, 35, 4, '1970-01-01 00:00:09', 3000, 100000, '  ', 'BioFinger VX', '105472', '72672', '69', '67', '0', '192.168.1.201', '1', 'ZEM510_TFT', '80', '', '  ', '', 0, 1, 0, '10', '0.0'),
('0577363558474', 1, '2015-10-28 07:27:04', '00:00; 12:10', 1, '86473643', '987466', NULL, '192.168.1.214', 1, '111001111', 'F7', 'Ver 5.0', 3, 250, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL),
('0421142600037', 1, '2015-11-17 10:11:41', '00:00;14:05', 1, '9999', '9999', NULL, '192.168.1.201', 1, '1111111100', 'F7', 'Ver 1.0.0-20140512', 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '192.168.1.201', NULL, NULL, NULL, NULL, NULL, NULL, 0, 8, 0, '10', '0.0');

--
-- Triggers `iclock`
--
DELIMITER $$
CREATE TRIGGER `trigger_Insert_devices` AFTER INSERT ON `iclock`
 FOR EACH ROW INSERT INTO ci.devices(
sn,
state,
last_activity,
trans_times,
trans_interval,
log_stamp,
op_log_stamp,
photo_stamp,
alias,
dept_id,
update_db,
style,
firmware_version,
fp_count,
transaction_count,
user_count,
main_time,
max_finger_count,
max_att_log_count,
device_name,
alg_ver,
flash_size,
free_flash_size,
language,
volume,
dt_fmt,
ip_address,
is_tft,
platform,
brightness,
backup_dev,
oem_vendor,
city,
acc_fun,
tz_adj,
del_tag,
fp_version,
push_version

)
SELECT
SN,
State,
LastActivity,
TransTimes,
TransInterval,
LogStamp,
OpLogStamp,
PhotoStamp,
Alias,
DeptID,
UpdateDB,
Style,
FWVersion,
FPCount,
TransactionCount,
UserCount,
MainTime,
MaxFingerCount,
MaxAttLogCount,
DeviceName,
AlgVer,
FlashSize,
FreeFlashSize,
Language,
VOLUME,
DtFmt,
IPAddress,
IsTFT,
Platform,
Brightness,
BackupDev,
OEMVendor,
City,
AccFun,
TZAdj,
DelTag,
FPVersion,
PushVersion
FROM adms_db.iclock ORDER BY LastActivity LIMIT 1
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `iclock_adminlog`
--

CREATE TABLE `iclock_adminlog` (
  `id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `User_id` int(11) DEFAULT NULL,
  `model` varchar(40) DEFAULT NULL,
  `action` varchar(40) NOT NULL,
  `object` varchar(40) DEFAULT NULL,
  `count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `iclock_adminlog`
--

INSERT INTO `iclock_adminlog` (`id`, `time`, `User_id`, `model`, `action`, `object`, `count`) VALUES
(1, '2014-05-15 10:47:08', 1, NULL, 'LOGOUT', '127.0.0.1', 1),
(2, '2014-05-15 10:47:15', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(3, '2014-05-15 17:59:39', 1, 'iclock', 'Modify', '0455134200004(192.168.1.201)', 1),
(4, '2014-05-15 18:00:34', 1, 'iclock', 'Modify', '0455134200004(192.168.1.201)', 1),
(5, '2014-05-16 10:34:10', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(6, '2014-05-16 10:38:02', 1, 'employee', 'del', '11', 1),
(7, '2014-05-16 16:29:03', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(8, '2014-05-16 16:30:27', 1, NULL, 'LOGOUT', '127.0.0.1', 1),
(9, '2015-04-16 23:04:30', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(10, '2015-10-07 17:45:01', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(11, '2015-10-10 13:58:01', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(12, '2015-10-10 13:59:14', 1, 'employee', 'Modify', '000000178 Karlen Kirakosyan', 1),
(13, '2015-10-13 08:47:01', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(14, '2015-11-03 10:53:25', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(15, '2015-11-03 10:55:59', 1, 'employee', 'Modify', '000000222 Karlen Kirakosyan', 1),
(16, '2015-11-03 11:04:12', 1, 'employee', 'Modify', '000000011 Andrea Sepotero', 1),
(17, '2015-11-04 00:53:11', 1, 'employee', 'Modify', '000000201', 1),
(18, '2015-11-17 08:43:39', 1, 'None', 'LOGIN', '127.0.0.1', 1),
(19, '2015-11-17 09:08:35', 1, 'iclock', 'clearlog', '0421142600037', 1),
(20, '2015-11-17 09:13:39', 1, 'iclock', 'cleardata', '0421142600037', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iclock_deptadmin`
--

CREATE TABLE `iclock_deptadmin` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `iclock_iclockmsg`
--

CREATE TABLE `iclock_iclockmsg` (
  `id` int(11) NOT NULL,
  `SN_id` varchar(20) NOT NULL,
  `MsgType` int(11) NOT NULL,
  `StartTime` datetime NOT NULL,
  `EndTime` datetime DEFAULT NULL,
  `MsgParam` varchar(32) DEFAULT NULL,
  `MsgContent` varchar(200) DEFAULT NULL,
  `LastTime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `iclock_messages`
--

CREATE TABLE `iclock_messages` (
  `id` int(11) NOT NULL,
  `MsgType` int(11) NOT NULL,
  `StartTime` datetime NOT NULL,
  `EndTime` datetime DEFAULT NULL,
  `MsgContent` longtext,
  `MsgImage` varchar(64) DEFAULT NULL,
  `DeptID_id` int(11) DEFAULT NULL,
  `MsgParam` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `iclock_oplog`
--

CREATE TABLE `iclock_oplog` (
  `id` int(11) NOT NULL,
  `SN` varchar(20) DEFAULT NULL,
  `admin` int(11) NOT NULL,
  `OP` smallint(6) NOT NULL,
  `OPTime` datetime NOT NULL,
  `Object` int(11) DEFAULT NULL,
  `Param1` int(11) DEFAULT NULL,
  `Param2` int(11) DEFAULT NULL,
  `Param3` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=363 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `iclock_oplog`
--

INSERT INTO `iclock_oplog` (`id`, `SN`, `admin`, `OP`, `OPTime`, `Object`, `Param1`, `Param2`, `Param3`) VALUES
(1, '0455134200004', 0, 0, '2014-05-15 17:58:56', 0, 0, 0, 0),
(2, '0455134200004', 2, 4, '2014-05-15 18:09:41', 0, 0, 0, 0),
(3, '0455134200004', 2, 9, '2014-05-15 18:10:21', 4, 0, 0, 0),
(4, '0455134200004', 2, 9, '2014-05-15 18:10:39', 5, 0, 0, 0),
(5, '0455134200004', 2, 9, '2014-05-15 18:10:46', 6, 0, 0, 0),
(6, '0455134200004', 2, 9, '2014-05-15 18:10:54', 7, 0, 0, 0),
(7, '0455134200004', 2, 9, '2014-05-15 18:10:55', 0, 0, 0, 0),
(8, '0455134200004', 2, 9, '2014-05-15 18:11:05', 8, 0, 0, 0),
(9, '0455134200004', 2, 9, '2014-05-15 18:11:06', 0, 0, 0, 0),
(10, '0455134200004', 2, 9, '2014-05-15 18:11:07', 0, 0, 0, 0),
(11, '0455134200004', 2, 9, '2014-05-15 18:11:11', 9, 0, 0, 0),
(12, '0455134200004', 2, 9, '2014-05-15 18:11:12', 0, 0, 0, 0),
(13, '0455134200004', 2, 9, '2014-05-15 18:11:13', 0, 0, 0, 0),
(14, '0455134200004', 2, 9, '2014-05-15 18:11:14', 0, 0, 0, 0),
(15, '0455134200004', 2, 9, '2014-05-15 18:11:19', 10, 0, 0, 0),
(16, '0455134200004', 2, 9, '2014-05-15 18:11:20', 0, 0, 0, 0),
(17, '0455134200004', 2, 9, '2014-05-15 18:11:21', 0, 0, 0, 0),
(18, '0455134200004', 2, 9, '2014-05-15 18:11:22', 0, 0, 0, 0),
(19, '0455134200004', 2, 9, '2014-05-15 18:11:23', 0, 0, 0, 0),
(20, '0455134200004', 2, 9, '2014-05-15 18:11:25', 11, 0, 0, 0),
(21, '0455134200004', 2, 9, '2014-05-15 18:11:26', 0, 0, 0, 0),
(22, '0455134200004', 2, 9, '2014-05-15 18:11:27', 0, 0, 0, 0),
(23, '0455134200004', 2, 9, '2014-05-15 18:11:28', 0, 0, 0, 0),
(24, '0455134200004', 2, 9, '2014-05-15 18:11:29', 0, 0, 0, 0),
(25, '0455134200004', 2, 9, '2014-05-15 18:11:30', 0, 0, 0, 0),
(26, '0455134200004', 2, 9, '2014-05-15 18:11:37', 1, 0, 0, 0),
(27, '0455134200004', 2, 9, '2014-05-15 18:11:38', 0, 0, 0, 0),
(28, '0455134200004', 2, 9, '2014-05-15 18:11:39', 0, 0, 0, 0),
(29, '0455134200004', 2, 9, '2014-05-15 18:11:40', 0, 0, 0, 0),
(30, '0455134200004', 2, 9, '2014-05-15 18:11:41', 0, 0, 0, 0),
(31, '0455134200004', 2, 9, '2014-05-15 18:11:42', 0, 0, 0, 0),
(32, '0455134200004', 2, 9, '2014-05-15 18:11:43', 0, 0, 0, 0),
(33, '0455134200004', 2, 7, '2014-05-15 18:13:11', 398, 0, 0, 0),
(34, '0455134200004', 2, 71, '2014-05-15 18:13:12', 3, 0, 0, 0),
(35, '0455134200004', 2, 9, '2014-05-15 18:13:13', 0, 0, 0, 0),
(36, '0455134200004', 2, 9, '2014-05-15 18:13:14', 0, 0, 0, 0),
(37, '0455134200004', 2, 9, '2014-05-15 18:13:15', 0, 0, 0, 0),
(38, '0455134200004', 2, 9, '2014-05-15 18:13:16', 0, 0, 0, 0),
(39, '0455134200004', 2, 9, '2014-05-15 18:13:17', 0, 0, 0, 0),
(40, '0455134200004', 2, 9, '2014-05-15 18:13:18', 0, 0, 0, 0),
(41, '0455134200004', 3, 4, '2014-05-15 18:13:30', 0, 0, 0, 0),
(42, '0455134200004', 2, 4, '2014-05-15 18:14:29', 0, 0, 0, 0),
(43, '0455134200004', 2, 4, '2014-05-15 18:15:05', 0, 0, 0, 0),
(44, '0455134200004', 2, 30, '2014-05-15 18:15:52', 2000, 0, 0, 0),
(45, '0455134200004', 2, 9, '2014-05-15 18:16:23', 2, 0, 0, 0),
(46, '0455134200004', 2, 9, '2014-05-15 18:16:24', 0, 0, 0, 0),
(47, '0455134200004', 2, 9, '2014-05-15 18:16:25', 0, 0, 0, 0),
(48, '0455134200004', 2, 9, '2014-05-15 18:16:26', 0, 0, 0, 0),
(49, '0455134200004', 2, 9, '2014-05-15 18:16:27', 0, 0, 0, 0),
(50, '0455134200004', 2, 9, '2014-05-15 18:16:28', 0, 0, 0, 0),
(51, '0455134200004', 2, 9, '2014-05-15 18:16:29', 0, 0, 0, 0),
(52, '0455134200004', 0, 0, '2014-05-15 21:02:38', 0, 0, 0, 0),
(53, '0455134200004', 0, 0, '2014-05-16 00:50:16', 0, 0, 0, 0),
(54, '0455134200004', 1, 4, '2014-05-16 00:50:56', 0, 0, 0, 0),
(55, '0455134200004', 1, 4, '2014-05-16 00:54:35', 0, 0, 0, 0),
(56, '0455134200004', 1, 4, '2014-05-16 00:56:05', 0, 0, 0, 0),
(57, '0455134200004', 1, 6, '2014-05-16 00:56:51', 17, 0, 0, 836),
(58, '0455134200004', 1, 9, '2014-05-16 00:56:52', 0, 0, 0, 0),
(59, '0455134200004', 1, 9, '2014-05-16 00:56:53', 0, 0, 0, 0),
(60, '0455134200004', 1, 9, '2014-05-16 00:56:54', 0, 0, 0, 0),
(61, '0455134200004', 1, 9, '2014-05-16 00:56:55', 0, 0, 0, 0),
(62, '0455134200004', 1, 9, '2014-05-16 00:56:56', 0, 0, 0, 0),
(63, '0455134200004', 0, 13, '2013-10-14 18:59:42', 0, 0, 0, 0),
(64, '0455134200004', 0, 0, '2013-10-14 19:00:03', 0, 0, 0, 0),
(65, '0455134200004', 0, 4, '2013-10-14 19:00:28', 0, 0, 0, 0),
(66, '0455134200004', 0, 4, '2013-10-14 19:01:24', 0, 0, 0, 0),
(67, '0455134200004', 0, 0, '2013-10-16 22:41:03', 0, 0, 0, 0),
(68, '0455134200004', 0, 1, '2013-10-16 22:41:45', 0, 0, 0, 0),
(69, '0455134200004', 0, 0, '2013-10-16 22:42:03', 0, 0, 0, 0),
(70, '0455134200004', 0, 4, '2013-10-16 22:42:48', 0, 0, 0, 0),
(71, '0455134200004', 0, 5, '2013-10-16 22:43:09', 132, 0, 0, 0),
(72, '0455134200004', 0, 5, '2013-10-16 22:43:10', 186, 0, 0, 0),
(73, '0455134200004', 0, 5, '2013-10-16 22:43:11', 299, 0, 0, 0),
(74, '0455134200004', 0, 5, '2013-10-16 22:43:12', 300, 0, 0, 0),
(75, '0455134200004', 0, 5, '2013-10-16 22:43:13', 301, 0, 0, 0),
(76, '0455134200004', 0, 5, '2013-10-16 22:43:14', 302, 0, 0, 0),
(77, '0455134200004', 0, 5, '2013-10-16 22:43:15', 303, 0, 0, 0),
(78, '0455134200004', 0, 5, '2013-10-16 22:43:16', 305, 0, 0, 0),
(79, '0455134200004', 0, 5, '2013-10-16 22:43:17', 313, 0, 0, 0),
(80, '0455134200004', 0, 5, '2013-10-16 22:43:18', 330, 0, 0, 0),
(81, '0455134200004', 0, 5, '2013-10-16 22:43:19', 347, 0, 0, 0),
(82, '0455134200004', 0, 5, '2013-10-16 22:43:55', 186, 0, 0, 0),
(83, '0455134200004', 0, 5, '2013-10-16 22:43:56', 299, 0, 0, 0),
(84, '0455134200004', 0, 5, '2013-10-16 22:43:57', 300, 0, 0, 0),
(85, '0455134200004', 0, 5, '2013-10-16 22:43:58', 301, 0, 0, 0),
(86, '0455134200004', 0, 5, '2013-10-16 22:43:59', 302, 0, 0, 0),
(87, '0455134200004', 0, 5, '2013-10-16 22:44:00', 303, 0, 0, 0),
(88, '0455134200004', 0, 5, '2013-10-16 22:44:01', 305, 0, 0, 0),
(89, '0455134200004', 0, 5, '2013-10-16 22:44:02', 313, 0, 0, 0),
(90, '0455134200004', 0, 5, '2013-10-16 22:44:03', 330, 0, 0, 0),
(91, '0455134200004', 0, 5, '2013-10-16 22:44:04', 347, 0, 0, 0),
(92, '0455134200004', 0, 5, '2013-10-16 22:44:05', 348, 0, 0, 0),
(93, '0455134200004', 0, 1, '2013-10-16 22:44:10', 0, 0, 0, 0),
(94, '0455134200004', 0, 0, '2013-10-16 22:44:30', 0, 0, 0, 0),
(95, '0455134200004', 0, 1, '2013-10-16 22:48:43', 0, 0, 0, 0),
(96, '0455134200004', 0, 0, '2013-10-16 22:54:03', 0, 0, 0, 0),
(97, '0455134200004', 0, 4, '2013-10-16 22:57:04', 0, 0, 0, 0),
(98, '0455134200004', 0, 4, '2013-10-16 22:59:49', 0, 0, 0, 0),
(99, '0455134200004', 0, 30, '2013-10-16 23:00:04', 2000, 0, 0, 0),
(100, '0455134200004', 0, 6, '2013-10-16 23:00:05', 2000, 0, 0, 1258),
(101, '0455134200004', 0, 1, '2013-10-16 23:07:35', 0, 0, 0, 0),
(102, '0455134200004', 0, 0, '2013-10-16 23:07:53', 0, 0, 0, 0),
(103, '0455134200004', 0, 4, '2013-10-16 23:13:24', 0, 0, 0, 0),
(104, '0455134200004', 0, 1, '2013-10-16 23:18:05', 0, 0, 0, 0),
(105, '0455134200004', 0, 0, '2014-04-06 15:28:54', 0, 0, 0, 0),
(106, '0455134200004', 0, 4, '2014-04-06 15:29:10', 0, 0, 0, 0),
(107, '0455134200004', 0, 4, '2014-04-06 15:30:19', 0, 0, 0, 0),
(108, '0455134200004', 0, 4, '2014-04-06 15:32:15', 0, 0, 0, 0),
(109, '0455134200004', 0, 4, '2014-04-06 15:34:09', 0, 0, 0, 0),
(110, '0455134200004', 0, 5, '2014-04-06 15:34:27', 186, 0, 0, 0),
(111, '0455134200004', 0, 5, '2014-04-06 15:34:28', 299, 0, 0, 0),
(112, '0455134200004', 0, 5, '2014-04-06 15:34:29', 300, 0, 0, 0),
(113, '0455134200004', 0, 5, '2014-04-06 15:34:30', 301, 0, 0, 0),
(114, '0455134200004', 0, 5, '2014-04-06 15:34:31', 302, 0, 0, 0),
(115, '0455134200004', 0, 5, '2014-04-06 15:34:32', 303, 0, 0, 0),
(116, '0455134200004', 0, 5, '2014-04-06 15:34:33', 305, 0, 0, 0),
(117, '0455134200004', 0, 5, '2014-04-06 15:34:34', 313, 0, 0, 0),
(118, '0455134200004', 0, 5, '2014-04-06 15:34:35', 330, 0, 0, 0),
(119, '0455134200004', 0, 5, '2014-04-06 15:34:36', 347, 0, 0, 0),
(120, '0455134200004', 0, 5, '2014-04-06 15:34:37', 348, 0, 0, 0),
(121, '0455134200004', 0, 1, '2014-04-06 15:38:24', 0, 0, 0, 0),
(122, '0455134200004', 0, 0, '2014-05-12 16:28:32', 0, 0, 0, 0),
(123, '0455134200004', 0, 4, '2014-05-12 16:28:50', 0, 0, 0, 0),
(124, '0455134200004', 0, 4, '2014-05-12 16:29:29', 0, 0, 0, 0),
(125, '0455134200004', 0, 30, '2014-05-12 16:30:34', 17, 0, 0, 0),
(126, '0455134200004', 0, 6, '2014-05-12 16:30:35', 17, 0, 0, 886),
(127, '0455134200004', 2, 4, '2014-05-12 16:31:17', 0, 0, 0, 0),
(128, '0455134200004', 2, 21, '2014-05-12 09:31:55', 114, 4, 12, 279),
(129, '0455134200004', 2, 5, '2014-05-12 09:31:56', 186, 0, 0, 0),
(130, '0455134200004', 2, 5, '2014-05-12 09:31:57', 299, 0, 0, 0),
(131, '0455134200004', 2, 5, '2014-05-12 09:31:58', 300, 0, 0, 0),
(132, '0455134200004', 2, 5, '2014-05-12 09:31:59', 301, 0, 0, 0),
(133, '0455134200004', 2, 5, '2014-05-12 09:32:00', 302, 0, 0, 0),
(134, '0455134200004', 2, 5, '2014-05-12 09:32:01', 303, 0, 0, 0),
(135, '0455134200004', 2, 5, '2014-05-12 09:32:02', 305, 0, 0, 0),
(136, '0455134200004', 2, 5, '2014-05-12 09:32:03', 313, 0, 0, 0),
(137, '0455134200004', 2, 5, '2014-05-12 09:32:04', 330, 0, 0, 0),
(138, '0455134200004', 2, 5, '2014-05-12 09:32:05', 347, 0, 0, 0),
(139, '0455134200004', 2, 4, '2014-05-12 09:32:45', 0, 0, 0, 0),
(140, '0455134200004', 2, 7, '2014-05-12 09:33:45', 17, 0, 0, 0),
(141, '0455134200004', 2, 9, '2014-05-12 09:33:46', 0, 65532, 0, 0),
(142, '0455134200004', 2, 9, '2014-05-12 09:33:47', 0, 65532, 0, 0),
(143, '0455134200004', 2, 9, '2014-05-12 09:33:48', 0, 65532, 0, 0),
(144, '0455134200004', 2, 9, '2014-05-12 09:33:49', 0, 65532, 0, 0),
(145, '0455134200004', 2, 9, '2014-05-12 09:33:50', 0, 65532, 0, 0),
(146, '0455134200004', 2, 9, '2014-05-12 09:33:51', 0, 65532, 0, 0),
(147, '0455134200004', 2, 4, '2014-05-12 09:39:52', 0, 0, 0, 0),
(148, '0455134200004', 2, 4, '2014-05-12 09:44:16', 0, 0, 0, 0),
(149, '0455134200004', 2, 4, '2014-05-12 09:45:50', 0, 0, 0, 0),
(150, '0455134200004', 2, 5, '2014-05-12 09:46:41', 186, 0, 0, 0),
(151, '0455134200004', 2, 5, '2014-05-12 09:46:42', 299, 0, 0, 0),
(152, '0455134200004', 2, 5, '2014-05-12 09:46:43', 300, 0, 0, 0),
(153, '0455134200004', 2, 5, '2014-05-12 09:46:44', 301, 0, 0, 0),
(154, '0455134200004', 2, 5, '2014-05-12 09:46:45', 302, 0, 0, 0),
(155, '0455134200004', 2, 5, '2014-05-12 09:46:46', 303, 0, 0, 0),
(156, '0455134200004', 2, 5, '2014-05-12 09:46:47', 305, 0, 0, 0),
(157, '0455134200004', 2, 5, '2014-05-12 09:46:48', 313, 0, 0, 0),
(158, '0455134200004', 2, 5, '2014-05-12 09:46:49', 330, 0, 0, 0),
(159, '0455134200004', 2, 5, '2014-05-12 09:46:50', 347, 0, 0, 0),
(160, '0455134200004', 2, 5, '2014-05-12 09:46:51', 348, 0, 0, 0),
(161, '0455134200004', 2, 5, '2014-05-12 09:47:03', 186, 0, 0, 0),
(162, '0455134200004', 2, 5, '2014-05-12 09:47:04', 299, 0, 0, 0),
(163, '0455134200004', 2, 5, '2014-05-12 09:47:05', 300, 0, 0, 0),
(164, '0455134200004', 2, 5, '2014-05-12 09:47:06', 301, 0, 0, 0),
(165, '0455134200004', 2, 5, '2014-05-12 09:47:07', 302, 0, 0, 0),
(166, '0455134200004', 2, 5, '2014-05-12 09:47:08', 303, 0, 0, 0),
(167, '0455134200004', 2, 5, '2014-05-12 09:47:09', 305, 0, 0, 0),
(168, '0455134200004', 2, 5, '2014-05-12 09:47:10', 313, 0, 0, 0),
(169, '0455134200004', 2, 5, '2014-05-12 09:47:11', 330, 0, 0, 0),
(170, '0455134200004', 2, 5, '2014-05-12 09:47:12', 347, 0, 0, 0),
(171, '0455134200004', 2, 5, '2014-05-12 09:47:13', 348, 0, 0, 0),
(172, '0455134200004', 2, 4, '2014-05-12 08:54:53', 0, 0, 0, 0),
(173, '0455134200004', 2, 4, '2014-05-12 08:57:09', 0, 0, 0, 0),
(174, '0455134200004', 0, 1, '2014-05-12 09:08:35', 0, 0, 0, 0),
(175, '0455134200004', 0, 0, '2014-05-12 09:27:03', 0, 0, 0, 0),
(176, '0455134200004', 2, 4, '2014-05-12 09:27:14', 0, 0, 0, 0),
(177, '0455134200004', 2, 5, '2014-05-12 09:27:36', 186, 0, 0, 0),
(178, '0455134200004', 2, 5, '2014-05-12 09:27:37', 299, 0, 0, 0),
(179, '0455134200004', 2, 5, '2014-05-12 09:27:38', 300, 0, 0, 0),
(180, '0455134200004', 2, 5, '2014-05-12 09:27:39', 301, 0, 0, 0),
(181, '0455134200004', 2, 5, '2014-05-12 09:27:40', 302, 0, 0, 0),
(182, '0455134200004', 2, 5, '2014-05-12 09:27:41', 303, 0, 0, 0),
(183, '0455134200004', 2, 5, '2014-05-12 09:27:42', 305, 0, 0, 0),
(184, '0455134200004', 2, 5, '2014-05-12 09:27:43', 330, 0, 0, 0),
(185, '0455134200004', 2, 5, '2014-05-12 09:27:44', 347, 0, 0, 0),
(186, '0455134200004', 2, 5, '2014-05-12 09:27:45', 348, 0, 0, 0),
(187, '0455134200004', 2, 5, '2014-05-12 09:27:46', 349, 0, 0, 0),
(188, '0455134200004', 2, 4, '2014-05-12 09:44:41', 0, 0, 0, 0),
(189, '0455134200004', 2, 5, '2014-05-12 09:45:05', 186, 0, 0, 0),
(190, '0455134200004', 2, 5, '2014-05-12 09:45:06', 299, 0, 0, 0),
(191, '0455134200004', 2, 5, '2014-05-12 09:45:07', 300, 0, 0, 0),
(192, '0455134200004', 2, 5, '2014-05-12 09:45:08', 301, 0, 0, 0),
(193, '0455134200004', 2, 5, '2014-05-12 09:45:09', 302, 0, 0, 0),
(194, '0455134200004', 2, 5, '2014-05-12 09:45:10', 303, 0, 0, 0),
(195, '0455134200004', 2, 5, '2014-05-12 09:45:11', 305, 0, 0, 0),
(196, '0455134200004', 2, 5, '2014-05-12 09:45:12', 313, 0, 0, 0),
(197, '0455134200004', 2, 5, '2014-05-12 09:45:13', 330, 0, 0, 0),
(198, '0455134200004', 2, 5, '2014-05-12 09:45:14', 347, 0, 0, 0),
(199, '0455134200004', 2, 5, '2014-05-12 09:45:15', 348, 0, 0, 0),
(200, '0455134200004', 2, 4, '2014-05-12 10:16:35', 0, 0, 0, 0),
(201, '0455134200004', 2, 5, '2014-05-12 10:17:09', 186, 0, 0, 0),
(202, '0455134200004', 2, 5, '2014-05-12 10:17:10', 299, 0, 0, 0),
(203, '0455134200004', 2, 5, '2014-05-12 10:17:11', 300, 0, 0, 0),
(204, '0455134200004', 2, 5, '2014-05-12 10:17:12', 301, 0, 0, 0),
(205, '0455134200004', 2, 5, '2014-05-12 10:17:13', 302, 0, 0, 0),
(206, '0455134200004', 2, 5, '2014-05-12 10:17:14', 303, 0, 0, 0),
(207, '0455134200004', 2, 5, '2014-05-12 10:17:15', 305, 0, 0, 0),
(208, '0455134200004', 2, 5, '2014-05-12 10:17:16', 313, 0, 0, 0),
(209, '0455134200004', 2, 5, '2014-05-12 10:17:17', 330, 0, 0, 0),
(210, '0455134200004', 2, 5, '2014-05-12 10:17:18', 347, 0, 0, 0),
(211, '0455134200004', 2, 5, '2014-05-12 10:17:19', 348, 0, 0, 0),
(212, '0455134200004', 2, 4, '2014-05-12 10:34:49', 0, 0, 0, 0),
(213, '0455134200004', 2, 5, '2014-05-12 10:35:06', 186, 0, 0, 0),
(214, '0455134200004', 2, 5, '2014-05-12 10:35:07', 299, 0, 0, 0),
(215, '0455134200004', 2, 5, '2014-05-12 10:35:08', 300, 0, 0, 0),
(216, '0455134200004', 2, 5, '2014-05-12 10:35:09', 301, 0, 0, 0),
(217, '0455134200004', 2, 5, '2014-05-12 10:35:10', 302, 0, 0, 0),
(218, '0455134200004', 2, 5, '2014-05-12 10:35:11', 303, 0, 0, 0),
(219, '0455134200004', 2, 5, '2014-05-12 10:35:12', 305, 0, 0, 0),
(220, '0455134200004', 2, 5, '2014-05-12 10:35:13', 313, 0, 0, 0),
(221, '0455134200004', 2, 5, '2014-05-12 10:35:14', 330, 0, 0, 0),
(222, '0455134200004', 2, 5, '2014-05-12 10:35:15', 347, 0, 0, 0),
(223, '0455134200004', 2, 5, '2014-05-12 10:35:16', 348, 0, 0, 0),
(224, '0455134200004', 0, 0, '2014-05-12 10:47:12', 0, 0, 0, 0),
(225, '0455134200004', 0, 1, '2014-05-12 10:54:56', 0, 0, 0, 0),
(226, '0455134200004', 0, 0, '2014-05-12 11:25:04', 0, 0, 0, 0),
(227, '0455134200004', 2, 4, '2014-05-12 11:26:52', 0, 0, 0, 0),
(228, '0455134200004', 2, 5, '2014-05-12 11:27:21', 186, 0, 0, 0),
(229, '0455134200004', 2, 5, '2014-05-12 11:27:22', 299, 0, 0, 0),
(230, '0455134200004', 2, 5, '2014-05-12 11:27:23', 300, 0, 0, 0),
(231, '0455134200004', 2, 5, '2014-05-12 11:27:24', 301, 0, 0, 0),
(232, '0455134200004', 2, 5, '2014-05-12 11:27:25', 302, 0, 0, 0),
(233, '0455134200004', 2, 5, '2014-05-12 11:27:26', 303, 0, 0, 0),
(234, '0455134200004', 2, 5, '2014-05-12 11:27:27', 305, 0, 0, 0),
(235, '0455134200004', 2, 5, '2014-05-12 11:27:28', 313, 0, 0, 0),
(236, '0455134200004', 2, 5, '2014-05-12 11:27:29', 330, 0, 0, 0),
(237, '0455134200004', 2, 5, '2014-05-12 11:27:30', 347, 0, 0, 0),
(238, '0455134200004', 2, 5, '2014-05-12 11:27:31', 348, 0, 0, 0),
(239, '0455134200004', 2, 4, '2014-05-12 11:36:07', 0, 0, 0, 0),
(240, '0455134200004', 2, 30, '2014-05-12 11:36:43', 10, 0, 0, 0),
(241, '0455134200004', 2, 6, '2014-05-12 11:36:44', 10, 0, 0, 1222),
(242, '0455134200004', 2, 4, '2014-05-12 11:42:27', 0, 0, 0, 0),
(243, '0455134200004', 2, 4, '2014-05-12 11:49:21', 0, 0, 0, 0),
(244, '0455134200004', 0, 0, '2014-05-12 11:56:17', 0, 0, 0, 0),
(245, '0455134200004', 2, 4, '2014-05-12 11:57:27', 0, 0, 0, 0),
(246, '0455134200004', 2, 4, '2014-05-12 11:59:17', 0, 0, 0, 0),
(247, '0455134200004', 2, 4, '2014-05-12 12:00:49', 0, 0, 0, 0),
(248, '0455134200004', 0, 0, '2014-05-12 12:03:03', 0, 0, 0, 0),
(249, '0455134200004', 2, 4, '2014-05-12 12:06:14', 0, 0, 0, 0),
(250, '0455134200004', 2, 4, '2014-05-12 12:06:54', 0, 0, 0, 0),
(251, '0455134200004', 0, 0, '2014-05-12 12:29:26', 0, 0, 0, 0),
(252, '0455134200004', 0, 0, '2014-05-13 08:22:10', 0, 0, 0, 0),
(253, '0455134200004', 2, 4, '2014-05-13 08:22:27', 0, 0, 0, 0),
(254, '0455134200004', 2, 5, '2014-05-13 08:23:01', 186, 0, 0, 0),
(255, '0455134200004', 2, 5, '2014-05-13 08:23:02', 299, 0, 0, 0),
(256, '0455134200004', 2, 5, '2014-05-13 08:23:03', 300, 0, 0, 0),
(257, '0455134200004', 2, 5, '2014-05-13 08:23:04', 301, 0, 0, 0),
(258, '0455134200004', 2, 5, '2014-05-13 08:23:05', 302, 0, 0, 0),
(259, '0455134200004', 2, 5, '2014-05-13 08:23:06', 303, 0, 0, 0),
(260, '0455134200004', 2, 5, '2014-05-13 08:23:07', 305, 0, 0, 0),
(261, '0455134200004', 2, 5, '2014-05-13 08:23:08', 313, 0, 0, 0),
(262, '0455134200004', 2, 5, '2014-05-13 08:23:09', 330, 0, 0, 0),
(263, '0455134200004', 2, 5, '2014-05-13 08:23:10', 347, 0, 0, 0),
(264, '0455134200004', 2, 5, '2014-05-13 08:23:11', 348, 0, 0, 0),
(265, '0455134200004', 2, 4, '2014-05-13 08:51:24', 0, 0, 0, 0),
(266, '0455134200004', 2, 5, '2014-05-13 08:51:56', 186, 0, 0, 0),
(267, '0455134200004', 2, 5, '2014-05-13 08:51:57', 299, 0, 0, 0),
(268, '0455134200004', 2, 5, '2014-05-13 08:51:58', 300, 0, 0, 0),
(269, '0455134200004', 2, 5, '2014-05-13 08:51:59', 301, 0, 0, 0),
(270, '0455134200004', 2, 5, '2014-05-13 08:52:00', 302, 0, 0, 0),
(271, '0455134200004', 2, 5, '2014-05-13 08:52:01', 303, 0, 0, 0),
(272, '0455134200004', 2, 5, '2014-05-13 08:52:02', 305, 0, 0, 0),
(273, '0455134200004', 2, 5, '2014-05-13 08:52:03', 313, 0, 0, 0),
(274, '0455134200004', 2, 5, '2014-05-13 08:52:04', 330, 0, 0, 0),
(275, '0455134200004', 2, 5, '2014-05-13 08:52:05', 347, 0, 0, 0),
(276, '0455134200004', 2, 5, '2014-05-13 08:52:06', 348, 0, 0, 0),
(277, '0455134200004', 2, 4, '2014-05-13 09:58:27', 0, 0, 0, 0),
(278, '0455134200004', 2, 5, '2014-05-13 09:58:49', 186, 0, 0, 0),
(279, '0455134200004', 2, 5, '2014-05-13 09:58:50', 299, 0, 0, 0),
(280, '0455134200004', 2, 5, '2014-05-13 09:58:51', 300, 0, 0, 0),
(281, '0455134200004', 2, 5, '2014-05-13 09:58:52', 301, 0, 0, 0),
(282, '0455134200004', 2, 5, '2014-05-13 09:58:53', 302, 0, 0, 0),
(283, '0455134200004', 2, 5, '2014-05-13 09:58:54', 303, 0, 0, 0),
(284, '0455134200004', 2, 5, '2014-05-13 09:58:55', 305, 0, 0, 0),
(285, '0455134200004', 2, 5, '2014-05-13 09:58:56', 313, 0, 0, 0),
(286, '0455134200004', 2, 5, '2014-05-13 09:58:57', 330, 0, 0, 0),
(287, '0455134200004', 2, 5, '2014-05-13 09:58:58', 347, 0, 0, 0),
(288, '0455134200004', 2, 5, '2014-05-13 09:58:59', 348, 0, 0, 0),
(289, '0455134200004', 0, 1, '2014-05-13 10:11:41', 0, 0, 0, 0),
(290, '0455134200004', 0, 0, '2014-05-13 10:14:07', 0, 0, 0, 0),
(291, '0455134200004', 2, 4, '2014-05-13 10:23:15', 0, 0, 0, 0),
(292, '0455134200004', 2, 5, '2014-05-13 10:23:59', 186, 0, 0, 0),
(293, '0455134200004', 2, 5, '2014-05-13 10:24:00', 299, 0, 0, 0),
(294, '0455134200004', 2, 5, '2014-05-13 10:24:01', 300, 0, 0, 0),
(295, '0455134200004', 2, 5, '2014-05-13 10:24:02', 301, 0, 0, 0),
(296, '0455134200004', 2, 5, '2014-05-13 10:24:03', 302, 0, 0, 0),
(297, '0455134200004', 2, 5, '2014-05-13 10:24:04', 303, 0, 0, 0),
(298, '0455134200004', 2, 5, '2014-05-13 10:24:05', 305, 0, 0, 0),
(299, '0455134200004', 2, 5, '2014-05-13 10:24:06', 313, 0, 0, 0),
(300, '0455134200004', 2, 5, '2014-05-13 10:24:07', 330, 0, 0, 0),
(301, '0455134200004', 2, 5, '2014-05-13 10:24:08', 347, 0, 0, 0),
(302, '0455134200004', 2, 5, '2014-05-13 10:24:09', 348, 0, 0, 0),
(303, '0455134200004', 2, 4, '2014-05-13 10:43:48', 0, 0, 0, 0),
(304, '0455134200004', 2, 5, '2014-05-13 10:44:08', 186, 0, 0, 0),
(305, '0455134200004', 2, 5, '2014-05-13 10:44:09', 299, 0, 0, 0),
(306, '0455134200004', 2, 5, '2014-05-13 10:44:10', 300, 0, 0, 0),
(307, '0455134200004', 2, 5, '2014-05-13 10:44:11', 301, 0, 0, 0),
(308, '0455134200004', 2, 5, '2014-05-13 10:44:12', 302, 0, 0, 0),
(309, '0455134200004', 2, 5, '2014-05-13 10:44:13', 303, 0, 0, 0),
(310, '0455134200004', 2, 5, '2014-05-13 10:44:14', 305, 0, 0, 0),
(311, '0455134200004', 2, 5, '2014-05-13 10:44:15', 313, 0, 0, 0),
(312, '0455134200004', 2, 5, '2014-05-13 10:44:16', 330, 0, 0, 0),
(313, '0455134200004', 2, 5, '2014-05-13 10:44:17', 347, 0, 0, 0),
(314, '0455134200004', 2, 5, '2014-05-13 10:44:18', 348, 0, 0, 0),
(315, '0455134200004', 2, 4, '2014-05-13 11:00:12', 0, 0, 0, 0),
(316, '0455134200004', 2, 5, '2014-05-13 11:00:34', 186, 0, 0, 0),
(317, '0455134200004', 2, 5, '2014-05-13 11:00:35', 299, 0, 0, 0),
(318, '0455134200004', 2, 5, '2014-05-13 11:00:36', 300, 0, 0, 0),
(319, '0455134200004', 2, 5, '2014-05-13 11:00:37', 301, 0, 0, 0),
(320, '0455134200004', 2, 5, '2014-05-13 11:00:38', 302, 0, 0, 0),
(321, '0455134200004', 2, 5, '2014-05-13 11:00:39', 303, 0, 0, 0),
(322, '0455134200004', 2, 5, '2014-05-13 11:00:40', 305, 0, 0, 0),
(323, '0455134200004', 2, 5, '2014-05-13 11:00:41', 313, 0, 0, 0),
(324, '0455134200004', 2, 5, '2014-05-13 11:00:42', 330, 0, 0, 0),
(325, '0455134200004', 2, 5, '2014-05-13 11:00:43', 347, 0, 0, 0),
(326, '0455134200004', 2, 5, '2014-05-13 11:00:44', 348, 0, 0, 0),
(327, '0455134200004', 0, 0, '2014-05-13 13:05:08', 0, 0, 0, 0),
(328, '0455134200004', 0, 0, '2014-05-14 07:42:31', 0, 0, 0, 0),
(329, '0455134200004', 0, 0, '2014-05-14 21:10:50', 0, 0, 0, 0),
(330, '0455134200004', 0, 1, '2014-05-14 21:20:23', 0, 0, 0, 0),
(331, '0455134200004', 0, 0, '2014-05-14 21:41:18', 0, 0, 0, 0),
(332, '0455134200004', 0, 1, '2014-05-14 22:21:34', 0, 0, 0, 0),
(333, '0455134200004', 0, 0, '2014-05-14 23:04:16', 0, 0, 0, 0),
(334, '0455134200004', 0, 1, '2014-05-14 23:04:49', 0, 0, 0, 0),
(335, '0455134200004', 0, 0, '2014-05-15 00:14:39', 0, 0, 0, 0),
(336, '0455134200004', 2, 4, '2014-05-15 00:55:34', 0, 0, 0, 0),
(337, '0455134200004', 0, 1, '2014-05-15 00:56:23', 0, 0, 0, 0),
(338, '0455134200004', 0, 0, '2014-05-15 00:56:54', 0, 0, 0, 0),
(339, '0455134200004', 2, 4, '2014-05-15 00:57:26', 0, 0, 0, 0),
(340, '0455134200004', 2, 4, '2014-05-15 01:04:04', 0, 0, 0, 0),
(341, '0455134200004', 0, 1, '2014-05-15 01:22:30', 0, 0, 0, 0),
(342, '0455134200004', 0, 0, '2014-05-15 01:45:49', 0, 0, 0, 0),
(343, '0455134200004', 2, 4, '2014-05-15 01:46:01', 0, 0, 0, 0),
(344, '0455134200004', 0, 1, '2014-05-15 01:47:04', 0, 0, 0, 0),
(345, '0455134200004', 0, 0, '2014-05-15 16:16:37', 0, 0, 0, 0),
(346, '0455134200004', 2, 4, '2014-05-15 16:21:17', 0, 0, 0, 0),
(347, '0455134200004', 2, 4, '2014-05-15 16:42:04', 0, 0, 0, 0),
(348, '0455134200004', 2, 4, '2014-05-15 16:46:54', 0, 0, 0, 0),
(349, '0455134200004', 0, 1, '2000-01-01 00:00:00', 0, 0, 0, 0),
(350, '0455134200004', 1, 4, '2014-05-15 18:02:14', 0, 0, 0, 0),
(351, '0455134200004', 1, 6, '2014-05-15 18:02:58', 10, 0, 0, 1138),
(352, '0455134200004', 1, 9, '2014-05-15 18:02:59', 0, 0, 0, 0),
(353, '0455134200004', 1, 9, '2014-05-15 18:03:00', 0, 0, 0, 0),
(354, '0455134200004', 1, 9, '2014-05-15 18:03:01', 0, 0, 0, 0),
(355, '0455134200004', 1, 9, '2014-05-15 18:03:02', 0, 0, 0, 0),
(356, '0455134200004', 0, 1, '2014-05-15 18:21:36', 0, 0, 0, 0),
(357, '0455134200004', 0, 1, '2014-05-15 18:44:48', 0, 0, 0, 0),
(358, '0455134200004', 0, 0, '2014-05-16 10:27:36', 0, 0, 0, 0),
(359, '0455134200004', 0, 1, '2014-05-16 10:33:46', 0, 0, 0, 0),
(360, '0455134200004', 0, 0, '2014-05-16 11:12:38', 0, 0, 0, 0),
(361, '0455134200004', 1, 4, '2014-05-16 11:13:18', 0, 0, 0, 0),
(362, '0455134200004', 1, 4, '2014-05-16 11:24:31', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `level_id` int(10) unsigned NOT NULL,
  `level` varchar(150) DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`level_id`, `level`, `company_id`, `date_created`, `date_updated`, `creator_id`) VALUES
(1, 'Level 1', 100, '2016-02-25 00:00:00', NULL, 18),
(2, 'Level 2', 100, '2016-02-25 00:00:00', NULL, 18);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `location_id` int(10) unsigned NOT NULL,
  `location_tag` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`location_id`, `location_tag`, `address`, `city`, `state_id`, `company_id`, `date_created`, `date_updated`, `creator_id`) VALUES
(1, 'Lagos HQ', 'Ahmadu bello', 'VI', 25, 100, '2016-02-25 00:00:00', NULL, 18);

-- --------------------------------------------------------

--
-- Table structure for table `marital_statuses`
--

CREATE TABLE `marital_statuses` (
  `marital_status_id` int(10) unsigned NOT NULL,
  `marital_status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `subject`, `id_string`, `status`, `requires_login`, `menu_order`) VALUES
(1, 'Staff Records', 'employee', 1, 1, 100),
(2, 'Attendance', 'attendance', 1, 1, 200),
(3, 'Leave', 'leave', 1, 1, 300),
(4, 'Communication', 'communication', 1, 1, 400),
(5, 'Payroll', 'payroll', 1, 1, 500),
(6, 'Events', 'event', 1, 1, 600);

-- --------------------------------------------------------

--
-- Table structure for table `module_perms`
--

CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100',
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module_perms`
--

INSERT INTO `module_perms` (`perm_id`, `module_id`, `subject`, `id_string`, `in_menu`, `status`, `menu_order`, `company_id`) VALUES
(1, 1, 'Staff Directory', 'staff_directory', 1, 1, 100, 100),
(2, 2, 'Attendance Record', 'attendance_record', 1, 1, 100, 100),
(21, 2, 'Settings', 'settings', 1, 1, 100, 100),
(23, 1, 'Settings', 'settings', 1, 1, 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) unsigned NOT NULL,
  `role` varchar(150) DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role`, `company_id`, `date_created`, `date_updated`, `creator_id`) VALUES
(1, 'Vice president', 100, '2016-02-25 00:00:00', NULL, 18),
(2, 'President', 100, '2016-02-25 00:00:00', NULL, 18);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(255) unsigned NOT NULL,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state`, `id_country`) VALUES
(1, 'Abia', 154),
(2, 'Abuja', 154),
(3, 'Adamawa', 154),
(4, 'Akwa Ibom', 154),
(5, 'Anambara', 154),
(6, 'Bauchi', 154),
(7, 'Bayelsa', 154),
(8, 'Benue', 154),
(9, 'Borno', 154),
(10, 'Cross River', 154),
(11, 'Delta', 154),
(12, 'Ebonyi', 154),
(13, 'Edo', 154),
(14, 'Ekiti', 154),
(15, 'Enugu', 154),
(16, 'Gombe', 154),
(17, 'Imo', 154),
(18, 'Jigawa', 154),
(19, 'Kaduna', 154),
(20, 'Kano', 154),
(21, 'Katsina', 154),
(22, 'Kebbi', 154),
(23, 'Kogi', 154),
(24, 'Kwara', 154),
(25, 'Lagos', 154),
(26, 'Nassarawa', 154),
(27, 'Niger', 154),
(28, 'Ogun', 154),
(29, 'Ondo', 154),
(30, 'Osun', 154),
(31, 'Oyo', 154),
(32, 'Plateau', 154),
(33, 'Rivers', 154),
(34, 'Sokoto', 154),
(35, 'Taraba', 154),
(36, 'Yobe', 154),
(37, 'Zamfara', 154);

-- --------------------------------------------------------

--
-- Table structure for table `states_copy`
--

CREATE TABLE `states_copy` (
  `state_id` int(255) unsigned NOT NULL,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states_copy`
--

INSERT INTO `states_copy` (`state_id`, `state`, `id_country`) VALUES
(1, 'Abia', 154),
(2, 'Abuja', 154),
(3, 'Adamawa', 154),
(4, 'Akwa Ibom', 154),
(5, 'Anambara', 154),
(6, 'Bauchi', 154),
(7, 'Bayelsa', 154),
(8, 'Benue', 154),
(9, 'Borno', 154),
(10, 'Cross River', 154),
(11, 'Delta', 154),
(12, 'Ebonyi', 154),
(13, 'Edo', 154),
(14, 'Ekiti', 154),
(15, 'Enugu', 154),
(16, 'Gombe', 154),
(17, 'Imo', 154),
(18, 'Jigawa', 154),
(19, 'Kaduna', 154),
(20, 'Kano', 154),
(21, 'Katsina', 154),
(22, 'Kebbi', 154),
(23, 'Kogi', 154),
(24, 'Kwara', 154),
(25, 'Lagos', 154),
(26, 'Nassarawa', 154),
(27, 'Niger', 154),
(28, 'Ogun', 154),
(29, 'Ondo', 154),
(30, 'Osun', 154),
(31, 'Oyo', 154),
(32, 'Plateau', 154),
(33, 'Rivers', 154),
(34, 'Sokoto', 154),
(35, 'Taraba', 154),
(36, 'Yobe', 154),
(37, 'Zamfara', 154);

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `templateid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `Template` longtext NOT NULL,
  `FingerID` smallint(6) NOT NULL,
  `Valid` smallint(6) NOT NULL,
  `DelTag` smallint(6) NOT NULL,
  `SN` varchar(20) DEFAULT NULL,
  `UTime` datetime DEFAULT NULL,
  `BITMAPPICTURE` longtext,
  `BITMAPPICTURE2` longtext,
  `BITMAPPICTURE3` longtext,
  `BITMAPPICTURE4` longtext,
  `USETYPE` smallint(6) DEFAULT NULL,
  `Template2` longtext,
  `Template3` longtext
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`templateid`, `userid`, `Template`, `FingerID`, `Valid`, `DelTag`, `SN`, `UTime`, `BITMAPPICTURE`, `BITMAPPICTURE2`, `BITMAPPICTURE3`, `BITMAPPICTURE4`, `USETYPE`, `Template2`, `Template3`) VALUES
(1, 1, 'TalTUzIxAAAE6usECAUHCc7QAAAc62kBAAAAhBcysupEAJoPfwCDAHrl5QBKALENJwBT6qsNPgBfAIANQupeANUNrgCnAJrlVgBlAGANUwBp6pMPwgBvAOoPP+pwAMEMPQC/AEHnPgCJAEwOtwCI6kEOpgCRAP4Pc+qWADIO+wBSACrlfgCpALIOcQCu6kEPiQCyAHwOU+qyADQPKAB9AE3l2AC5ADkPvgC+6rcOOQDDAPoPf+rJAK0OhQAIANfk7wDPADQPkQDY6iQOGgDdAIwPp+rlAGcPKgAgADvlBgHnAK8OKgDt6jgNSQDxAOwOhuryAIcP5wA/AO3nOgD8AC4MlgAE66YM0AAFASYPSeoLASEMJgDVATTl3AAXAfkOrQAe64oPFwAmAfMOgOoxAXYPOQD+AaPlNABBAZgPHgBM6/UPewBLAZkONhYfb+/oLu+uIR8IFQtXGC+FWfs6lGYQPHKBhYDnKHi4dQEV+XKUD6rurI2pH5KgGPcasPNn6ZI6hsOX/JSEfZXt9Iaw+Pnp8AjtjvYLtRN2emuLcf/2qmOH+B18gXp4t/Ra/gNt7fsZ/H2BKAdW4LuLZAsxBOQPvh9XgZISUQ1DAnLlawTa+isMyAixFpQE2fkuDacd7DWU+K33QCucHXH1jP/u/G6FdP6aa/vpTQ0GAtsOjgu3lre81fmsE1byTHpyfT7JXFFBZAQHvf2BhegG7su/trbfabHACuIaOAVtDlmK9YuFa2B6xe6N/kevMJX89hn6qf/0B074LBpWEMsES2yeDhMVJQtikq+iXfWn7cvzMBK3co+SwfKKZp9pivxD+VYP8evX0Wq3XjDYmbKCAyD7AQaHHEADALcV0v8H6sEkHsAGAFIZCBX/NwsAjh7JwPmq/2gVAIch1cD7FfptWv3CwDrB+isuBQB9Jgw6QRDqayoJwP//hTLErWVzCQBkNTgp+8QJAOc6N8I6/cYpwQUAe0ODSAQEaUYTPQQAd419iO4B9lQ0bgzFPl8p+P0n//8xwADgsSj6khoAsqMtVaE6wPrD/cA6wfkUwf79+v/+2wCWgZunxonD/kn/xSnAw8XDwsMEiMUowwwANW9MkFjGugkAxnEp/gTAVOwBwXIxwMCPDgTTfUz/wMDABVz7K//EBgAhhZ/CYu0BIZRWwXIFAwT+hlD/BwA7SUz7K8U6EACkjGjCwCrFwsjCi8I6lQHqQI1G/4YExXGWrKMDAHeSFz0PBECSNCn//fs6/fgX/CcNAKWU+P80Efz7/Pw1BMX/nc00BQD5mzCtEwS8r8Aw/PobOPzEzRMLAIyuw2DNwSnFwcsHAN15MPkr+ykWANi9+P/EEf7+wMBYODn8xRf9PgsAJ76MwMapiMUPAGjEdf/6F/77+fr8JOMEBN/GQ34fADsCOsSXwsGnkMHBAcDGKHfCb8LEktgASrHq/v3B+/72//sqwf/+O/3/OsD7FP7+wfwEADfSLbUFAFjfJ6XNAFYKMMLGp8MHxRnmrF5KAwAn6IbAD+ot6Df/wcIEw8colQMA8+wnBggUJghkYlQOEcoPkijGwXLBwVEEAxTjE0PABBAk0Tql7hHiFvfA+M4RBPd8/8LCh8CSCRQdKH2Mav4H1fs0nYVaAxD7Nq7+APo4QwlcBBASSn5zCRAcTwPCOMHE8QUQPl/9UQAAAAAAAA==', 0, 1, 0, '0455134200004', '2014-05-15 10:59:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 'SjVTUzIxAAADdnQECAUHCc7QAAAbd2kBAAAAg5sZbHYkAIQPmAD7AIV5kwBGAAoPewBmdo8POgBrAL4Ph3aGAIUP5QBQAJB5lQCXAA4PHACcdhcPygCtAFcPOHa7APkPpwADAI14qwDSABYOfADYdpYOhQDcAEEPo3YAAY0P8ADCAZ95lgAYAYgOvgAjdwsOlgAnAU8OH3YqAVsPZQDuAWJ5mgA9AY8NTQBHd24NoABMAdcNFHYefFsLVH9WBByMUYcXiJN+SoaUfBr/R4cX+yb7HgzGf+f7UXyXAZBxFIqngYN/iITO89KL0X3SAZMHSPWad3N3KHvQ+114IYuBgOKWRICOfUMP8nLm+uL4RnA/Bs77gweuE4D9PADJ/m2DIKqC9WyHuQQt/kSDJIkiUzPvDVr/9xyuJHxlIakGwOEiKOfkIYT9puOKATYwASAxAQLtIQFyAVYLAz0HxVIRcEhMBACoH9Z/CHZWGgb/wF2dwQB2NykA/wsAmSsDPVz/XQoAG/UDa7dGwQsAFTTVxMM8WcIUABE81cT8Kv/Awf7+wjpQ/S8IAJw+DMGOwDt4AQpFDMJrBf9WtkIKAJVGDJJUSmMBBlAGdFSdQ2OIwWwWAAdhMf3Bi8BV/8BDVIBaEXYFbPT/PlKFYMJIBQA2bH1p0gAEC/XA/cP+TP3B/bf+Z//9WRfFBoWBwUfAQ/3BkUZYHAsAf4aDYrRWBnaZmAz/VRjFBZyfKsA+wEovO2XDFgQA3aETTN0AC9HwT1P/PVGOwEm3/xcACbPtgf/9tjxM/1NMwAUJA0i49/9ARBjFCMKfQjNH/0ZKgcBacAFLzHTBXN0AAaTlQE//wP6OQk8ywAwAgdiGvMHDGcHCFQAB3SJE/ItVRsDCIsCRDAP/3Aw2/1L/kAwD99+DdcJiwq8VA3To3v1M/zWDwDw6wRcQAwDcOv1JtsH8L8D9wTr/wojD//8MEJzEk4v/w8B3AxD0zh78fRGSFonBkgeDwG4RByXX/sA6/mNHMf9UwMBgzBBqX/b7wP5DwcYQYlxqxBcQCysnZCyIOv3/TMBFmQQTEy5kbQgQl/+TwLTHwcP9FRDZQtQ5PS5G/v9VtgcT9EVxwsP7LcIQnz+bw8TDmQ/VO0il/v/9/TT/OcH8GRUQ40yT/jrAwrbBwcOIk4cBAxMSXgDBAAA7SUz7Kw==', 0, 1, 0, '0455134200004', '2014-05-15 10:59:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 4, 'TYVTUzIxAAAExsQECAUHCc7QAAAcx2kBAAAAhGstkMYdAJYPYADkAInJqAAqABEPlQArxn8PjgA4AFYPaMZHAIYPPwCkAHXJnQBjAI0PWgBwxooP6wB7ANIPZcaTAAcP7gBRAJzJfACbAIcP/gC1xuUPzwC2AFoOZca9AAEPwAB4AJzIfAC+AH4POgDDxhUOqwDMAFYPg8bQAH0OCQEdAB/IWADgAOsPtgDlxgQPjADkAEAOBsfnAJkOIAAqAIDJLgD5AHoOzwH/xh0NzQAEAVIPgcYMAQYOlwDIAXvIKQAOAXoOaAAKx5IPagARASgPLsYZAekOUwDaAVrJJgAjAf0N0gAhxwQOOgAmAUEOh8YyAWEN7gD2AZnJLgBDAb0PswBBx+YMNQBPASYPAUA0B2sJVRHvCg87CH7egLuHIPEmP1INtf/ahJsNns/v9a76++0a9T+pLASq/4sKdABTw6r7pYK/eyqPgUW7C98kpYKO+BtAhIM6CVaLb+aHGsb6UQhODwYIgUHLGQ4e2fiUB1vZMAGFf8b5XPmuuvp11f9qGZMSPT+wAWl7uYTUCaLHmOte7YtwRIJlTfwfsQmBgif2uUYYfqaEFQ33DDJZoPlVAk6Q0IYixx4JXgVnGtb+eU/0G76mgXnA7r45QZNJfdUCtBbmO3OO/ZON63+PVLXh77Vlmd/Ic66xTRcB+Il5vAhh2FSPhYmZnTQrfbjjXsZ3jwp+g3/J8d+SOHrAsIo7T1L9TCtmYpbosQ5dgGUWID3EAoLZ5QQAagATgwkEvgETTMNVBMWmBNVtBQBdARMFVQLGmQEQUMIOxVAAyjdbwMFkwMIAicISUsDCDwCGBwj4wcBSwFbAwgBFyA3//nMEAPMVAvEJADEYF8U7wDrXATIlBsH8mMDFOGX/wVsFAGkpC6YGAKsuF8IF/2DVATE59zP+nlthBvyPCACSPNNzXsABaEOMwcJRDAS2RhNoVkzAwwBij4d5wgkAL5L0xO03CgCUZIycwsc4lQcAoGcWBcBkzwGcdIxZmwUTBOp25P////s6/8QG///Awf/ABFcWxjGB8MIq/ztgRpPAEwArkOTqKvuAwWBpCABlVAP5Bv5EBgB5mkPBwFESACue3jPrOMSEUMIOADGkGzP6ODDAwDcTAPOs2gb8//39/v8F//sG/8P/wcD+wwBgfvsi/wcA03wXYfwLAF2/YMEFwPs6/P/7DwCqDZZwBMLFwcLAwDj7+NABMsrQ/EE5//mC/8P+wMDABcHEOwUAg86GxAYIBGnOGsFawP/JAI8VEUz/wMDCBB0AxoLUgKYYAN/h+Qz6xf3A/v05//kH/v/Awf/ABcDF4AQAVeRawDnDAMYt+nf+ww/VygZRY//Cw6PDO/zA3hENBv3BxEXA+Tn9/fxdRMAG/vs6BRCTCYapwxCsypePxQcQm8oQWfsHEGYUYsM6XxHWKh8Ax2fAO/r59m7/wP77/MEQVOVdSwYQAib0d8fVEe8vscjAnk7GAsTCwP7/wjgRFcAuT/7/IcAE/lIE/FcFEH40rMf+OwIQFTUi/ckQzPCRwGnAxsEDUA3Wv0CXwnDDdAYUvkLn+v7+wsAQiYUN/MfAEhAiSr4N+sPCZI6iBcD/xoVaAxD7Ng==', 0, 1, 0, '0455134200004', '2014-05-15 10:59:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 5, 'TQ9TUzIxAAAETFAECAUHCc7QAAAcTWkBAAAAhPEnNkwxABcPSwD3AJBDHgA5AIAPXQBDTKgPaABxAFoPhkxzAKkPzwBnAKBDeQCnAKcP4wCuTIYP7ACzAHsPVUy7AJwPZwADAKxDGwDHAHEPKADfTCMOQwDqAFMPXEzvAKEPzQArAKdCPwD0AIoPIwD/TCwM0wAFAdkN5kwLAaML7QDPAatALwAUAXwPFQAfTTgN3AAbAfYM8kwcAa8N2ADhAThAngAvATUOhAA2TZAO2AAyAXkNE0w1AR4OyAD9Ab5B6wA5AbMPagA5TasNIgBEAeQOwExFASkNpgCNAbJCYgBLAZsOsgBITRoO2Zlthkp7dTJnFx/38Wk/7xuvLgA7D2MXIPsTtsb7WQ+vAOYG9qUKhx8DLgGPD6dOSxgL7Cvznx0G0hYf+fbS97IUaUKvASoL4uzq3gOaq/TKgR5m+Q/1uT4caQ1VG2v7EcbwewYByPco605dnIyBgMERtHhVvajm3PepeKiIVEOAhAEBLu0f9VouSQto/w0eZfvIsxyRgQFVf7QPEEfpB4WAaY1Eg1IzmxVacW53QIAxSNQJif0+pqePEN+4AEESXfm0e5pINPR1deWCzAVO3SqkJHPlgvxvrUCAg8aM6YFHD28z6IFjZE/oVCCELy8gOgECVdqwAkxzDSfAwFTXAFRDH/9VwFhmnP37XgFFEB5H/5j/WCFGDgBqFR76wGw4QAoALxgW+cH7jEcGAJ0YKa7AAUylGCRlDgChGSSzRME+w8A+3ABCUB3//8BKRQTAxYzBwP//Wv+VBARvHxDA/wgAUh4jLlsMAIcfKQXAXbNs/gUA3aD1wsaKCwCPISJDBcBvRQELLwAi/5YUBHouEP4+/2UFamGzRxUANDQXOv9DjMFKwcFaPgQJBE9D/f/+/cA7WxNMA0oDPDf+BUZijVnBSgwAB5YDNLJSPgsABV3FMzt0FwADZAM7+0ZSP8HB/z0XAMBuB3z/VEzAdMGV//ixDQBrdCREsWjFWwEGdgnB/fs7WCbBWv0tCgDCgQez/P9GPhcAzYoHsv9BRcBZUQT//7L++gcABJQ4/fp3AgDzlF7I0QAF7fEbwT/+wAVkxIzDHAMABKY4/ghMeqope2tUBRIESK/9/sD8Pjr/xD/BwUkTAANy/fux////QHvAoRoMTAjA/fw7RsMA5ZUoVMADAOkMJMdeAQTZ5/38Of77s8P+wP/BwAT/+LEFAPHcFyjAAOmTKMP+/wYAxuTjsf39/g8ABi/wxLb8/8D+/8AFwfqP/AUAOfOTAZcATN7zNHYDADz3LYwPEAgA7f8+/vux/1z/VwYQEgkYsvw1DhAIDSz9+rApWGsMEA3W7fhtWcA+CBAT2ADFtvxcBBDbH/X+x0gR9yE3JgjVCiZcRMT4wwQQ3jMTjP4EEBJCKQfEAFzJRSfCygbVNlhKev8EEMpeRnD59m7/wP4=', 0, 1, 0, '0455134200004', '2014-05-15 10:59:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 10, 'SgdTUzIxAAADREcECAUHCc7QAAAbRWkBAAAAg+kZj0QpAAQPtQCEAItLNABaAHwPIQBvRI0PgQBxAEQP20R4AA8PkwC6AAtLyACMAIwPggCoRPoPqgCvAEIOrES9AA4OwAAHAI5KWQDEAHEPTADQRH4P+ADmAFMPqET4AIgPpQDIAYFKjAAaAQAOZAAZRYEOdQAjAZoPrEQwAYkOmwD5AWlKWgBCAdoPcwBKRX4PiwBPATIPlDjGh7v/k4Te/yA/Ev8/g/N7kH1RR5YHHXrj+wJ/UsHMhh+LFYpigYA60H7SA1MEa4ycO2Z/LXgZ/x78JsyAgd6TgYFUC7hGu3gf9QfvK3PgvCb5gwtjE0aLRk1HCVqLTQQM/WrDgIHxo22DCACCxSj7FWN+ht/jZmYsDqELAeKwdimoh36z5Y/fcPqVUH+LZY6Ge6pwA9SBASAsAQEzHRpAAZshg38DxaQoQv4LAGYu+jr9ZrtWBwBhMgkHN8NBAVc0ff9dygAxegFYwS9t//ITA0tL/VL//8E6RVcPwxIABlnwOMD+gD5MwFhGA8UwWT7BEwAJZQZW/MIDwVD//8FdzwB/NIHAa3jAFMULdrBVVMBDVMD7wQdE2noMNQQAU4AAexcADIzwPQVUWHk2VMAWAAlf7UgQRv9J//7B88AVRA2m7Uo2OI9UVwgJAEyq9zUE//xTAQe46cD/g/9Hu0JE/8A+CMVUwTPC/8DBhxDFXMe+wf1VTFhEzQBVjHFrwXYZAMfO33VWQcA9WD4F/sC5GQAI0+IvBf38hE1i/f7BS6ANA8HSgMDDfv9MbBZE9eOawv/CB/3ChsHAwlnCakYPA+z0kMN1g8EHwlFKAaf7g2fCBv/AOEgJAK/8EAU3wbr8CxCgDIMEwsKHwMPBwP8Z1BMO3mtiwHT+mQHAwzXAgRgRDBZZwcK7cWLCwsDCBcNOIMAXEQkgmgf+woX+eMDBk8Ksc8JAESohWoQD1XEmJsMJEKovjAbCxoXDKhUQ+jJTwVaFbcGCwnzB6QQT0j1tfxMQ9oWaeIX+joDDwsIF+v5AEbJFiasP1e9G13L/fcDGwqENE6JLicH8h8IDwsG5/gMQskt6BQMTjVp6/wAAAAAAAAA=', 0, 1, 0, '0455134200004', '2014-05-15 17:57:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 9, 'TTFTUzIxAAAEcnMECAUHCc7QAAAcc2kBAAAAhJ8prHIrAJcPcwDrAIl9ZAA8AIAPWgBDcpcPewBTAEMPSnJsAGoPqwCwAJR9PwCFAO0PbACPcpMPbACgAM8PgnKtAIoP5QB5ABZ9QgC9AOYPCQDEcicPagDPAMQOhXLRAIIO1AAXAKF9wgDTAJ0PagDYcpcPhwDiALkNDXPvAJsPYQA0AOt9eADzAAkO/QDzct0PjAD3AEwOJ3L/AF8O8AA7AKJ87gAFAZgOJQAOcxkOgQAhAcgNbHIiAe8OkADjAYp/pQAoAToNAQAvcx4LSQA1AZMNnHI5AaIMVwD+AeZ5owA8ATQMJgBDc6IMZwBKATkNqXJRAZcLNgCuDUthVA0uCgv5JPUmjlYaugBeEuYImnqz+u7ycX/G5e+cNAKn/IsPqIevl9IKsQZTCN5vjfG6CN8liYP7CVb56O7Jb9Z2a+eHr1r6iYGtiZAWhfCbhcoVMQlEgk4LWAiNgbWXJPixC1AK2fri8gtyvY889mF2TvSPBUv3lOpe8YJ2RIFR/PweGX6i7qr7hfOsDq4KFYbec095IRJBkcYMafZZ8s/9XYBNdQP7hfLwIpZsjeYoD/rrgIAh7flVZJnJ34wJ5uTS7kd/OQYXaUZd7HNIFb0bMIy96nn3GZcVj3iefodbDlqPIWwfqUYOlWZL9KYsWAGIVr8B5T0FcGMekgYAbsAPPLMHAIMFF/+ZBgQbCBPAQgsAmw0UNlllCwBdFdVMV42JCABOGwbuwE53ATsiAz0DxawqaP4KAC4w9Dv+M438wg8ANjcxKkCNXMFDBQBh+YOPdwGjShp0EMUvSZ8wwPxVYGTNAHMikZ3CewoAulUXssHA/mv+CMV0UvvDkIQFAEqqbXp+Aadxk8FuB8GEsw0ApHaQwAWIhPAGAK95GmQF/w1yQITtwSP9nA0E142QwIbDwQXAwI3ABwCtjhwH/nRnASOV4sBD5ET6jV3/gggAcFoJ+o1FwAYAg6pMwcDlGAAZu9o4Ojv7WkXAS8HD/JAGBJu/FsD//P/NANSxJsF4VxIAF8+mGG7Aw8TAwgdpxX4BqtqXwMBdw8UKDACD3oPDAcUys2/EGQAY4AX8+4w/L/z9/8E4wMWMw/7/wHcZxSDgpEU9/f/7/vjAxIzDwGVXAwDZ5VqyAgCC5YDDzwCPlxL/RVD+EMU886xSwPoewML3wRByJ/rcXTv+Of36s/5QwMDAA8U1/yXBFwDs/aIEwcSzUcDDwMLFB2nFPcMEAB7/ZLIDFFYDXsEDEPTHFvp2EeQJDCgE1eIKbkYGEGgf4jj6J3QRbCTt//k++RRiwCekwlzEOsjHJIkDEKgpNAUEFLoqHD0DEMPqK8B/EUsw4GL7PvzEjP/9/gQRBvVWXXQRkzSexcYFxBdiDTkrwcLFBML4t0/8gP5dGdX4PcjJ/nD/wMGoxcW3wlvCwP6VwxCVMKbHxagWEPREwo/+NzT9+/87wzaw/v35BBBog+kSdhFrS/0mAAAAAAAAAA==', 0, 1, 0, '0455134200004', '2014-05-15 18:03:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `userid` int(11) NOT NULL,
  `badgenumber` varchar(20) NOT NULL,
  `defaultdeptid` int(11) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `Card` varchar(20) DEFAULT NULL,
  `Privilege` int(11) DEFAULT NULL,
  `AccGroup` int(11) DEFAULT NULL,
  `TimeZones` varchar(20) DEFAULT NULL,
  `Gender` varchar(2) DEFAULT NULL,
  `Birthday` datetime DEFAULT NULL,
  `street` varchar(40) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `ophone` varchar(20) DEFAULT NULL,
  `FPHONE` varchar(20) DEFAULT NULL,
  `pager` varchar(20) DEFAULT NULL,
  `minzu` varchar(8) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `SN` varchar(20) DEFAULT NULL,
  `SSN` varchar(20) DEFAULT NULL,
  `UTime` datetime DEFAULT NULL,
  `State` varchar(2) DEFAULT NULL,
  `City` varchar(150) DEFAULT NULL,
  `SECURITYFLAGS` smallint(6) DEFAULT NULL,
  `DelTag` smallint(6) NOT NULL,
  `RegisterOT` int(11) DEFAULT NULL,
  `AutoSchPlan` int(11) DEFAULT NULL,
  `MinAutoSchInterval` int(11) DEFAULT NULL,
  `Image_id` int(11) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `pass` varchar(150) DEFAULT NULL,
  `access_level` tinyint(4) DEFAULT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `gender_id` tinyint(4) NOT NULL,
  `address` varchar(200) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`userid`, `badgenumber`, `defaultdeptid`, `name`, `Password`, `Card`, `Privilege`, `AccGroup`, `TimeZones`, `Gender`, `Birthday`, `street`, `zip`, `ophone`, `FPHONE`, `pager`, `minzu`, `title`, `SN`, `SSN`, `UTime`, `State`, `City`, `SECURITYFLAGS`, `DelTag`, `RegisterOT`, `AutoSchPlan`, `MinAutoSchInterval`, `Image_id`, `email`, `status`, `company_id`, `pass`, `access_level`, `first_name`, `last_name`, `gender_id`, `address`, `state_id`, `country_id`) VALUES
(18, '000000008', 1, ' Yinka Davis', '', NULL, NULL, NULL, NULL, NULL, '1970-01-01 00:00:00', '10 Bola Shadipe Street, Off Adelabu, Sur', NULL, NULL, NULL, '0800998873', NULL, NULL, '0421142600037', NULL, '2015-11-17 08:53:41', NULL, 'Su', NULL, 0, 1, NULL, NULL, NULL, 'admin@aol.com', 1, 100, '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 'Adekunle', 'Smith', 1, '', 25, 154),
(19, '000000023', 1, 'sugun ige', '', '0', 0, 1, '0001000100000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0421142600037', NULL, '2015-11-17 09:39:29', NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 100, '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL, '', '', 0, '', 0, 0),
(9, '3201', 1, 'Andrea Sepotero', '', '0', NULL, 1, '0000000000000000', NULL, NULL, '', '', '', '', '', '', '', '0455134200004', NULL, '2014-05-15 18:03:09', NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 100, '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL, '', '', 0, '', 0, 0),
(10, '000000222', 1, 'Karlen Kirakosyan', '', '0', NULL, 1, '0000000000000000', NULL, NULL, '', '', '', '', '', '', '', '0455134200004', NULL, '2014-05-16 10:30:22', NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 100, '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL, '', '', 0, '', 0, 0),
(12, '000000201', 1, 'Paul Azeez', '2000', '', 14, NULL, '0000000000000000', NULL, NULL, '', '', '', '', '', '', '', '0455134200004', NULL, '2014-05-16 10:30:22', NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 100, '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL, '', '', 0, '', 0, 0);

--
-- Triggers `userinfo`
--
DELIMITER $$
CREATE TRIGGER `trigger_Insert_userinfo` AFTER INSERT ON `userinfo`
 FOR EACH ROW INSERT INTO ci.attendance_user_info(
user_id,
id_user,
dept_id,
name,
password,
card,
priviledge,
acc_group,
time_zones,
sn,
u_time,
del_tag,
register_ot,
auto_sch_plan,
min_auto_sch_interval,
image_id
)
SELECT userid,
badgenumber,
defaultdeptid,
name,
Password,
Card,
Privilege,
AccGroup,
TimeZones,
SN,
UTime,
DelTag,
RegisterOT,
AutoSchPlan,
MinAutoSchInterval,
Image_id
FROM adms_db.userinfo ORDER BY userid DESC LIMIT 1
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_perms`
--

CREATE TABLE `user_perms` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_perms`
--

INSERT INTO `user_perms` (`id`, `user_id`, `module_id`, `perm_id`, `company_id`) VALUES
(1, 18, 1, 1, 100),
(2, 18, 2, 2, 100),
(3, 18, 2, 21, 100),
(4, 18, 1, 23, 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`access_level_id`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`account_type_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `attendance_shifts`
--
ALTER TABLE `attendance_shifts`
  ADD PRIMARY KEY (`attendance_shift_id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissions_group_id` (`group_id`),
  ADD KEY `auth_group_permissions_permission_id` (`permission_id`);

--
-- Indexes for table `auth_message`
--
ALTER TABLE `auth_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_message_user_id` (`user_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  ADD KEY `auth_permission_content_type_id` (`content_type_id`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_user_id` (`user_id`),
  ADD KEY `auth_user_groups_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permissions_user_id` (`user_id`),
  ADD KEY `auth_user_user_permissions_permission_id` (`permission_id`);

--
-- Indexes for table `bank_account_type`
--
ALTER TABLE `bank_account_type`
  ADD PRIMARY KEY (`account_type_id`);

--
-- Indexes for table `checkinout`
--
ALTER TABLE `checkinout`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userid` (`userid`,`checktime`),
  ADD KEY `checkinout_userid` (`userid`),
  ADD KEY `checkinout_SN` (`SN`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`company_id`),
  ADD KEY `FK_companies_account_type` (`account_type_id`);

--
-- Indexes for table `company_modules`
--
ALTER TABLE `company_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `devcmds`
--
ALTER TABLE `devcmds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devcmds_SN_id` (`SN_id`),
  ADD KEY `devcmds_User_id` (`User_id`);

--
-- Indexes for table `devlog`
--
ALTER TABLE `devlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devlog_SN_id` (`SN_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_user_id` (`user_id`),
  ADD KEY `django_admin_log_content_type_id` (`content_type_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `app_label` (`app_label`,`model`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `employee_bank_details`
--
ALTER TABLE `employee_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_kin_details`
--
ALTER TABLE `employee_kin_details`
  ADD PRIMARY KEY (`employee_kin_detail_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `iclock`
--
ALTER TABLE `iclock`
  ADD PRIMARY KEY (`SN`),
  ADD KEY `iclock_DeptID` (`DeptID`);

--
-- Indexes for table `iclock_adminlog`
--
ALTER TABLE `iclock_adminlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iclock_adminlog_User_id` (`User_id`);

--
-- Indexes for table `iclock_deptadmin`
--
ALTER TABLE `iclock_deptadmin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iclock_deptadmin_user_id` (`user_id`),
  ADD KEY `iclock_deptadmin_dept_id` (`dept_id`);

--
-- Indexes for table `iclock_iclockmsg`
--
ALTER TABLE `iclock_iclockmsg`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iclock_iclockmsg_SN_id` (`SN_id`);

--
-- Indexes for table `iclock_messages`
--
ALTER TABLE `iclock_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iclock_messages_DeptID_id` (`DeptID_id`);

--
-- Indexes for table `iclock_oplog`
--
ALTER TABLE `iclock_oplog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SN` (`SN`,`OPTime`),
  ADD KEY `iclock_oplog_SN` (`SN`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `marital_statuses`
--
ALTER TABLE `marital_statuses`
  ADD PRIMARY KEY (`marital_status_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD PRIMARY KEY (`perm_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `states_copy`
--
ALTER TABLE `states_copy`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`templateid`),
  ADD UNIQUE KEY `userid` (`userid`,`FingerID`),
  ADD UNIQUE KEY `USERFINGER` (`userid`,`FingerID`),
  ADD KEY `template_userid` (`userid`),
  ADD KEY `template_SN` (`SN`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`userid`),
  ADD KEY `userinfo_defaultdeptid` (`defaultdeptid`),
  ADD KEY `userinfo_SN` (`SN`);

--
-- Indexes for table `user_perms`
--
ALTER TABLE `user_perms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `access_level_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `account_types`
--
ALTER TABLE `account_types`
  MODIFY `account_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attendance_shifts`
--
ALTER TABLE `attendance_shifts`
  MODIFY `attendance_shift_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_message`
--
ALTER TABLE `auth_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_account_type`
--
ALTER TABLE `bank_account_type`
  MODIFY `account_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checkinout`
--
ALTER TABLE `checkinout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `company_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `company_modules`
--
ALTER TABLE `company_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `department_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `devcmds`
--
ALTER TABLE `devcmds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `devlog`
--
ALTER TABLE `devlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `employee_bank_details`
--
ALTER TABLE `employee_bank_details`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_kin_details`
--
ALTER TABLE `employee_kin_details`
  MODIFY `employee_kin_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `gender_id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `gender_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iclock_adminlog`
--
ALTER TABLE `iclock_adminlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `iclock_deptadmin`
--
ALTER TABLE `iclock_deptadmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iclock_iclockmsg`
--
ALTER TABLE `iclock_iclockmsg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iclock_messages`
--
ALTER TABLE `iclock_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iclock_oplog`
--
ALTER TABLE `iclock_oplog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=363;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `level_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `location_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `marital_statuses`
--
ALTER TABLE `marital_statuses`
  MODIFY `marital_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `module_perms`
--
ALTER TABLE `module_perms`
  MODIFY `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `states_copy`
--
ALTER TABLE `states_copy`
  MODIFY `state_id` int(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `templateid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user_perms`
--
ALTER TABLE `user_perms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
