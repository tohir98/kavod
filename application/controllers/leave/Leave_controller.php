<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Leave_controller
 *
 * @author tohir
 * @property Leave_model $l_model Description
 */
class Leave_controller extends CI_Controller {
    
    private $company_id;
    private $user_id;

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('user/Employee_model', 'emp_model');
        $this->load->model('leave/leave_model', 'l_model');
        $this->load->model('basic_model');
        $this->load->library(['User_nav_lib']);

        $this->_init();
    }

    private function _init() {
        $this->company_id = $this->user_auth_lib->get('company_id');
        $this->user_id = $this->user_auth_lib->get('user_id');
    }

    
    public function config() {
        
        $this->user_auth_lib->check_login();
        $data = [
                    'period' => $this->basic_model->get_leave_period(TBL_LEAVE_PERIOD, ['creator_id' => $this->user_id]),
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                ];

        $this->user_nav_lib->run_page('leave/leave', $data, 'Leave | ' . BUSINESS_NAME);
        
    }

    public function leave_type()
    {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->l_model->add_leave_type($this->company_id, request_post_data())) {
                notify('success', 'Leave Type Added Sucessfully');
            } else {
                notify('error', 'Oops! Unable to create leave type, pls try again');
            }
            redirect(site_url('/leave/leave_type'));
        }

        $data = [
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                    'type' => $this->basic_model->fetch_leave_types(TBL_LEAVE_TYPE, ['company_id' => $this->company_id]),
                ];

        $this->user_nav_lib->run_page('leave/leave_type', $data, 'Leave | ' . BUSINESS_NAME);
    }

    public function work_week()
    {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->create_work_week(TBL_LEAVE_WORK_WEEK, $this->company_id, $this->user_id, request_post_data())) {
                notify('success', 'Work Week Saved Sucessfully');
            } else {
                notify('error', 'Oops! Unable to save work week, pls try again');
            }
            redirect(site_url('/leave/work_week'));
        }

        $data = [
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                    'weeks' => $this->basic_model->fetch_leave_type(TBL_LEAVE_WORK_WEEK, ['company_id' => $this->company_id]),
                ];

        $this->user_nav_lib->run_page('leave/work_week', $data, 'Leave | ' . BUSINESS_NAME);
    }

    public function holidays()
    {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->add_holiday(TBL_LEAVE_HOLIDAY, $this->company_id, $this->user_id, request_post_data())) {
                notify('success', 'Holiday added Sucessfully');
            } else {
                notify('error', 'Oops! Unable to add holiday, pls try again');
            }
            redirect(site_url('/leave/holidays'));
        }

        $data = [
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                    'holidays' => $this->basic_model->fetch_leave_types(TBL_LEAVE_HOLIDAY, ['company_id' => $this->company_id]),
                ];

        $this->user_nav_lib->run_page('leave/holidays', $data, 'Leave | ' . BUSINESS_NAME);
    }

    public function entitlement()
    {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->add_entitlement(TBL_ENTITLEMENT, TBL_ENTITLEMENT_SUM, $this->company_id, $this->user_id, request_post_data())) {
                notify('success', 'Entitlement added Sucessfully');
            } else {
                notify('error', 'Oops! Unable to add entitlement, pls try again');
            }
            redirect(site_url('/leave/entitlement'));
        }

        $data = [
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                    'employees' => $this->emp_model->fetchCompanyRecord($this->company_id),
                    'leave_types' => $this->basic_model->fetch_leave_types(TBL_LEAVE_TYPE, ['company_id' => $this->company_id]),
                    'entitlements' => $this->basic_model->fetch_entitlements(TBL_ENTITLEMENT, TBL_ENTITLEMENT_SUM, ['company_id' => $this->company_id]),
                ];
        // var_dump($this->basic_model->fetch_entitlements(TBL_ENTITLEMENT, ['company_id' => $this->company_id]));
        // exit;
        $this->user_nav_lib->run_page('leave/entitlement', $data, 'Leave | ' . BUSINESS_NAME);
    }

    public function entitlement_details($employee_id)
    {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->add_entitlement(TBL_ENTITLEMENT, TBL_ENTITLEMENT_SUM, $this->company_id, $this->user_id, request_post_data())) {
                notify('success', 'Entitlement added Sucessfully');
            } else {
                notify('error', 'Oops! Unable to add entitlement, pls try again');
            }
            redirect(site_url('/leave/entitlement'));
        }

        $user = $this->db->get_where(TBL_USERS, ['userid' => $employee_id])->row();
        $data = [
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                    'employees' => $this->emp_model->fetchCompanyRecord($this->company_id),
                    'leave_types' => $this->basic_model->fetch_leave_types(TBL_LEAVE_TYPE, ['company_id' => $this->company_id]),
                    'entitlements' => $this->basic_model->fetch_entitlement(TBL_ENTITLEMENT, ['employee_id' => $employee_id]),
                    'username' => $user->name,
                    'employee_id' => $employee_id,
                ];
        // var_dump($this->basic_model->fetch_entitlements(TBL_ENTITLEMENT, ['company_id' => $this->company_id]));
        // exit;
        $this->user_nav_lib->run_page('leave/entitlement_details', $data, 'Leave | ' . BUSINESS_NAME);
    }

    public function months_pop()
    {
        $months =   ['0' => 'January',
                     '1' => 'February',
                     '2' => 'March',
                     '3' => 'April',
                     '4' => 'May',
                     '5' => 'June',
                     '6' => 'July',
                     '7' => 'August',
                     '8' => 'September',
                     '9' => 'October',
                     '10' => 'November',
                     '11' => 'December',
                    ];
        echo json_encode($months);
    }

    public function addLeavePeriod()
    {
        $this->user_auth_lib->check_login();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $start = new DateTime($this->input->post('start_date'));
            $end = new DateTime($this->input->post('end_date'));
            
            $data = ['start_date' => date_format($start, 'Y-m-d'),
                     'end_date' => date_format($end, 'Y-m-d H:i:s'),
                     'creator_id' => $this->user_id,
                     'company_id' => $this->company_id,
                    ];
            if($this->basic_model->add_leave_period(TBL_LEAVE_PERIOD, $data, ['company_id' => $this->company_id])){
                notify('success', "Leave Period created successfully");
            }else{
                notify('error', "Oops! An error occured while adding leave period, please try again");
            }
            redirect(site_url('/leave/config'));
        }
    }

    public function edit_type($type_id)
    {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->edit_leave_type(TBL_LEAVE_TYPE, $type_id, request_post_data())) {
                notify('success', 'Leave Type Edited Sucessfully');
            } else {
                notify('error', 'Oops! Unable to edit leave type, pls try again');
            }
            redirect(site_url('/leave/leave_type'));
        }

        $data = [
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                    'type' => $this->basic_model->fetch_leave_type(TBL_LEAVE_TYPE, ['type_id' => $type_id]),
                ];

        $this->user_nav_lib->run_page('leave/edit_type', $data, 'Leave | ' . BUSINESS_NAME);
    }

    public function edit_entitlement($ent_id, $employee_id)
    {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            $data = request_post_data();
            
            $sum = $this->db->get_where(TBL_ENTITLEMENT_SUM, ['employee_id' => $employee_id])->row();
            $days = $this->db->get_where(TBL_ENTITLEMENT, ['id' => $ent_id])->row();

            $cummulative = (int)$sum->days_sum - (int)$days->days;

            $final = (int)$cummulative + (int)$data['days'];
            
            $this->db->where('id', $sum->id);
            $this->db->update(TBL_ENTITLEMENT_SUM, ['days_sum' => $final]);
            if ($this->basic_model->edit_entitlement(TBL_ENTITLEMENT, $ent_id, request_post_data())) {
                notify('success', 'Allocation Edited Sucessfully');
            } else {
                notify('error', 'Oops! Unable to edit allocation, pls try again');
            }
            redirect(site_url('/leave/entitlement_details/' . $employee_id));
        }
    }

    public function delete_type($type_id)
    {
        $this->user_auth_lib->check_login();

        if ($this->basic_model->delete(TBL_LEAVE_TYPE, ['type_id' => $type_id])) {
            notify('success', 'Leave Type deleted successfully');
        } else {
            notify('error', 'An error occured while deleting leave type');
        }

        redirect(site_url('/leave/leave_type'));
    }

    public function delete_holiday($hol_id)
    {
        $this->user_auth_lib->check_login();

        if ($this->basic_model->delete(TBL_LEAVE_HOLIDAY, ['id' => $hol_id])) {
            notify('success', 'Holiday deleted successfully');
        } else {
            notify('error', 'An error occured while deleting holiday');
        }

        redirect(site_url('/leave/holidays'));
    }

    public function delete_entitlement($ent_id, $employee_id)
    {
        $this->user_auth_lib->check_login();

        $sum = $this->db->get_where(TBL_ENTITLEMENT_SUM, ['employee_id' => $employee_id])->row();
        $days = $this->db->get_where(TBL_ENTITLEMENT, ['id' => $ent_id])->row();

        $cummulative = (int)$sum->days_sum - (int)$days->days;

        $this->db->where('id', $sum->id);
        $this->db->update(TBL_ENTITLEMENT_SUM, ['days_sum' => $cummulative]);

        if ($this->basic_model->delete(TBL_ENTITLEMENT, ['id' => $ent_id])) {

            notify('success', 'Allocation deleted successfully');
        } else {
            notify('error', 'An error occured while deleting allocation');
        }

        redirect(site_url('/leave/entitlement_details/' . $employee_id));
    }

    public function leave_allowance()
    {
        $this->user_auth_lib->check_login();
        $data = [
                    'period' => $this->basic_model->get_leave_period(TBL_LEAVE_PERIOD, ['creator_id' => $this->user_id]),
                    'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
                    'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
                ];

        $this->user_nav_lib->run_page('leave/allowance', $data, 'Leave | ' . BUSINESS_NAME);
    }
}
