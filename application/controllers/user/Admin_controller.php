<?php

/**
 * Description of Admin_controller
 *
 * @author TOHIR
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property CI_Loader $load Description
 * @property User_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Admin_nav_lib $admin_nav_lib Description
 * @property Employee_model $e_model Description
 */
class Admin_controller extends CI_Controller {

    private $company_id;
    private $user_id;

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('basic_model');
        $this->load->library(['user_nav_lib']);

        $this->_init();
    }

    private function _init() {
        $this->company_id = $this->user_auth_lib->get('company_id');
        $this->user_id = $this->user_auth_lib->get('user_id');

    }

    public function dashboard() {
        $this->user_auth_lib->check_login();

        $data = array(
            'markets' => '',
            'active_staff' => $this->e_model->fetch_all_users($this->company_id, 1),
            'inactive_staff' => $this->e_model->fetch_all_users($this->company_id, 0)
        );

        $this->user_nav_lib->run_page('dashboard/landing', $data, BUSINESS_NAME);
    }

    public function users() {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('admin:users');

        if (request_is_post()) {
            $data = request_post_data();
            $data['date_created'] = date('Y-m-d h:i:s');
            $data['password'] = $this->user_auth_lib->encrypt('password');
            $data['status'] = USER_STATUS_ACTIVE;
            if ($this->u_model->save($data)) {
                notify('success', 'User created successfully');
            } else {
                notify('error', 'Unable to create user at moment, pls try again.');
            }
        }

        $data = array(
            'users' => $this->u_model->fetchUsers()
        );

        $this->user_nav_lib->run_page('user/list', $data, 'Users Directiry | ' . BUSINESS_NAME);
    }

    public function user_types() {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('admin:user_types');
        $data = array(
            'user_types' => $this->basic_model->fetch_all_records(User_model::TBL_USER_TYPE)
        );

        $this->user_nav_lib->run_page('user/types', $data, 'User Types | ' . BUSINESS_NAME);
    }

    public function changeUserStatus($user_id) {
//        $this->user_auth->check_perm('employee:checklist_edit');
        if ($this->u_model->toggleStatus($user_id)) {
            notify('success', "User status changed successfully");
        } else {
            notify('error', "Status change could not be completed");
        }

        redirect($this->input->server('HTTP_REFERER') ? : site_url('/admin/users'));
    }

    public function companies() {
        $data = [];
        $this->admin_nav_lib->run_page('admin/companies', $data, 'Companies');
    }

}
