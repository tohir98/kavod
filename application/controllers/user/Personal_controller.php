<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Personal_controller
 *
 * @author tohir
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property CI_Loader $load Description
 * @property Employee_model $emp_model Description
 */
class Personal_controller extends CI_Controller {

    private $company_id;
    private $user_id;

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('user/Employee_model', 'emp_model');
        $this->load->model('user/Settings_model', 'set_model');
        $this->load->model('basic_model');
        $this->load->library(['User_nav_lib']);

        $this->_init();
    }

    private function _init() {
        $this->company_id = $this->user_auth_lib->get('company_id');
        $this->user_id = $this->user_auth_lib->get('user_id');
    }

    public function my_profile() {
        $this->user_auth_lib->check_login();

        $emergency_content = $this->load->view('user/personal/profile_page_contents/emergency_contacts', array(
            'e_contacts' => '' // Fetch emergency contact
                ), true);

        $bank_content = $this->load->view('user/personal/profile_page_contents/bank_details', array(
            'bank_details' => ''    // Fetch bank details
                ), true);
        
        $pension_content = $this->load->view('user/personal/profile_page_contents/pension_details', array(
            'pension_details' => '' // pension
                ), true);

        $profile_content = $this->load->view('user/personal/profile_page_contents/profile', array(
            'p_info' => $this->emp_model->fetchPersonalInfo($this->company_id, $this->user_id)[0],
            'w_info' => $this->emp_model->fetchWorkInfo($this->company_id, $this->user_id) ? : [],
                ), true);

        $data = array(
            'profile_content' => $profile_content,
            'bank_content' => $bank_content,
            'pension_content' => $pension_content,
            'emergency_content' => $emergency_content,
            'profile_picture' => $this->emp_model->getStaffProfilePic($this->user_id, $this->company_id),
        );

        $this->user_nav_lib->run_page('user/personal/profile_page', $data, 'My Profile |' . BUSINESS_NAME);
    }

}
