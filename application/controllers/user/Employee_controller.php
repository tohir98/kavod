<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Employee_controller
 *
 * @author tohir
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property CI_Loader $load Description
 * @property User_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Employee_model $emp_model Description
 * @property Settings_model $set_model Description
 */
class Employee_controller extends CI_Controller {

    private $company_id;
    private $user_id;

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('user/Employee_model', 'emp_model');
        $this->load->model('user/Settings_model', 'set_model');
        $this->load->model('basic_model');
        $this->load->library(['User_nav_lib']);

        $this->_init();
    }

    private function _init() {
        $this->company_id = $this->user_auth_lib->get('company_id');
        $this->user_id = $this->user_auth_lib->get('user_id');

        $this->set_model->createDefaultDept($this->company_id);
    }

    public function staff_directory() {
        $this->user_auth_lib->check_login();

        $data = array(
            //'employees' => $this->basic_model->fetch_all_records(TBL_USERS, ['company_id' => $this->company_id]),
            'employees' => $this->emp_model->fetch_all_users($this->company_id),
            'account_statuses' => Basic_model::getAccountStatuses()
        );

        $this->user_nav_lib->run_page('user/employee/staff_directory', $data, 'Staff Directory | ' . BUSINESS_NAME);
    }

    public function edit_personal_info($userid) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->emp_model->updatePersonalInfo($userid, $this->company_id, request_post_data())) {
                notify('success', 'Personal Infomation was updated successfully');
            } else {
                notify('error', 'Ops! Unable to update personal Information, pls try again');
            }
            redirect(site_url('/employee/staff_directory'));
        }

        $data = [
            'emp_details' => $this->emp_model->fetchRecord($userid, $this->company_id),
            'states' => $this->basic_model->fetch_all_records(TBL_STATES),
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRY),
            'genders' => $this->basic_model->fetch_all_records(TBL_GENDER)
        ];

        $this->user_nav_lib->run_page('user/employee/edit', $data, 'Edit Staff Record | ' . BUSINESS_NAME);
    }

    public function edit_work_info($userid) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->emp_model->updateWorkInfo($userid, $this->company_id, request_post_data())) {
                notify('success', 'Work Infomation was updated successfully');
            } else {
                notify('error', 'Ops! Unable to update work Information, pls try again');
            }
            redirect(site_url('/employee/staff_directory'));
        }

        $data = [
            'work_info' => $this->emp_model->fetchWorkInfo($userid, $this->company_id),
            'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
            'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
            'roles' => $this->basic_model->fetch_all_records(TBL_ROLES, ['company_id' => $this->company_id]),
            'levels' => $this->basic_model->fetch_all_records(TBL_LEVELS, ['company_id' => $this->company_id]),
            'employment_types' => $this->basic_model->fetch_all_records(TBL_EMP_TYPE),
            'working_shifts' => $this->basic_model->fetch_all_records('attendance_shifts'),
        ];

        $this->user_nav_lib->run_page('user/employee/edit_work_info', $data, 'Edit Staff Record | ' . BUSINESS_NAME);
    }

    public function settings() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->set_model->create(TBL_DEPT, $this->company_id, request_post_data())) {
                notify('success', 'Department created successfully');
            } else {
                notify('error', 'Ops! Unable to create department, pls try again');
            }
            redirect(site_url('/employee/settings'));
        }

        $data = [
            'departments' => $this->set_model->fetchDepts($this->company_id),
            'employees' => $this->emp_model->fetch_all_users($this->company_id)
        ];

        $this->user_nav_lib->run_page('user/settings/departments', $data, 'Departments | ' . BUSINESS_NAME);
    }

    public function location_setting() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->set_model->create(TBL_LOC, $this->company_id, request_post_data())) {
                notify('success', 'Location created successfully');
            } else {
                notify('error', 'Ops! Unable to create location, pls try again');
            }
            redirect(site_url('/employee/location_setting'));
        }

        $data = [
            'locations' => $this->set_model->fetchCompanyLocations($this->company_id),
            'states' => $this->basic_model->fetch_all_records(TBL_STATES),
        ];

        $this->user_nav_lib->run_page('user/settings/locations', $data, 'Departments | ' . BUSINESS_NAME);
    }

    public function role_setting() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->set_model->create(TBL_ROLES, $this->company_id, request_post_data())) {
                notify('success', 'Role created successfully');
            } else {
                notify('error', 'Ops! Unable to create role, pls try again');
            }
            redirect(site_url('/employee/role_setting'));
        }

        $data = [
            'roles' => $this->set_model->fetchCompanyRoles($this->company_id)
        ];

        $this->user_nav_lib->run_page('user/settings/roles', $data, 'Roles | ' . BUSINESS_NAME);
    }

    public function level_setting() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->set_model->create(TBL_LEVELS, $this->company_id, request_post_data())) {
                notify('success', 'Level created successfully');
            } else {
                notify('error', 'Ops! Unable to create level, pls try again');
            }
            redirect(site_url('/employee/level_setting'));
        }

        $data = [
            'levels' => $this->set_model->fetchCompanyLevels($this->company_id)
        ];

        $this->user_nav_lib->run_page('user/settings/levels', $data, 'Levels | ' . BUSINESS_NAME);
    }

    public function view_record($userid, $fullname) {
        $this->user_auth_lib->check_login();

        $emergency_content = $this->load->view('user/personal/profile_page_contents/emergency_contacts', array(
            'e_contacts' => '' // Fetch emergency contact
                ), true);

        $bank_content = $this->load->view('user/personal/profile_page_contents/bank_details', array(
            'bank_details' => ''    // Fetch bank details
                ), true);

        $pension_content = $this->load->view('user/personal/profile_page_contents/pension_details', array(
            'pension_details' => '' // pension
                ), true);

        $profile_content = $this->load->view('user/personal/profile_page_contents/profile', array(
            'p_info' => $this->emp_model->fetchPersonalInfo($this->company_id, $userid)[0],
            'w_info' => $this->emp_model->fetchWorkInfo($this->company_id, $userid) ? : [],
                ), true);

        $data = array(
            'profile_content' => $profile_content,
            'bank_content' => $bank_content,
            'pension_content' => $pension_content,
            'emergency_content' => $emergency_content,
            'employees' => $this->emp_model->fetchPersonalInfo($this->company_id),
            'profile_picture' => $this->emp_model->getStaffProfilePic($userid, $this->company_id),
        );

        $this->user_nav_lib->run_page('user/personal/profile_page', $data, 'My Profile |' . BUSINESS_NAME);
    }

    public function change_profile_picture($userid, $full_name) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if (request_is_post()) {
                if ($this->emp_model->update_employee_picture($this->input->post('userid'), $this->company_id, $_FILES['userfile'])) {
                    notify('success', 'Staff picture updated successfully');
                } else {
                    notify('error', 'Unable to update staff image at moment, pls try again');
                }
                redirect(site_url('/employee/view_record/' . $userid . '/' . $full_name));
            }
        }

        $data = [
            'profile_picture' => $this->emp_model->fetchStaffPicture($userid, $this->company_id),
            'userid' => $userid
        ];

        $this->user_nav_lib->run_page('user/employee/edit_profile_picture', $data, 'Change Profile Picture | ' . BUSINESS_NAME);
    }

    public function remove_profile_picture($userid) {
        $this->user_auth_lib->check_login();

        if ($this->emp_model->removeProfilePicture($userid, $this->company_id)) {
            notify('success', 'Profile picture removed successfully');
        } else {
            notify('error', 'Unable to remove profile picture, pls try again');
        }

        redirect($this->input->server('HTTP_REFERER'));
    }

    public function edit_department($dept_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->emp_model->updateDepartment($dept_id, $this->company_id, request_post_data())) {
                notify('success', 'Department was updated successfully');
            } else {
                notify('error', 'Ops! Unable to update Department, pls try again');
            }
            redirect(site_url('/employee/settings'));
        }

        $data = [
            'depts' => $this->emp_model->fetchDeptRecord($dept_id, $this->company_id, TBL_DEPT)[0],
            'department_details' => $this->basic_model->fetch_all_records(TBL_DEPT),
            'employees' => $this->emp_model->fetch_all_users($this->company_id)
        ];

        $this->user_nav_lib->run_page('user/settings/edit_department', $data, 'Edit Department | ' . BUSINESS_NAME);
    }

    public function edit_location($loc_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->emp_model->updateLocation($loc_id, $this->company_id, request_post_data())) {
                notify('success', 'Location was updated successfully');
            } else {
                notify('error', 'Ops! Unable to update Location, pls try again');
            }
            redirect(site_url('/employee/location_setting'));
        }

        $data = [
            'locations' => $this->emp_model->fetchLocationRecord($loc_id, $this->company_id, TBL_LOC)[0],
            'states' => $this->basic_model->fetch_all_records(TBL_STATES)
        ];

        $this->user_nav_lib->run_page('user/settings/edit_location', $data, 'Edit Location | ' . BUSINESS_NAME);
    }

    public function delete_location($loc_id) {
        $this->user_auth_lib->check_login();

        if ($this->basic_model->delete(TBL_LOC, ['location_id' => $loc_id])) {
            notify('success', 'Location deleted successfully');
        } else {
            notify('error', 'An error occured while deleting location');
        }

        redirect(site_url('employee/location_setting'));
    }

    public function edit_role($role_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->emp_model->updateRole($role_id, $this->company_id, request_post_data())) {
                notify('success', 'Role was updated successfully');
            } else {
                notify('error', 'Ops! Unable to update Role, pls try again');
            }
            redirect(site_url('/employee/role_setting'));
        }

        $data = [
            'role' => $this->emp_model->fetchRoleRecord($role_id, $this->company_id, TBL_ROLES)[0]
        ];

        $this->user_nav_lib->run_page('user/settings/edit_role', $data, 'Edit Role | ' . BUSINESS_NAME);
    }

    public function edit_level($lev_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->emp_model->updateLevel($lev_id, $this->company_id, request_post_data())) {
                notify('success', 'Level was updated successfully');
            } else {
                notify('error', 'Ops! Unable to update Level, pls try again');
            }
            redirect(site_url('/employee/level_setting'));
        }

        $data = [
            'level' => $this->emp_model->fetchLevelRecord($lev_id, $this->company_id, TBL_LEVELS)[0]
        ];

        $this->user_nav_lib->run_page('user/settings/edit_level', $data, 'Edit Level | ' . BUSINESS_NAME);
    }

    public function delete_role($role_id) {
        $this->user_auth_lib->check_login();

        if ($this->basic_model->delete(TBL_ROLES, ['role_id' => $role_id])) {
            notify('success', 'Role deleted successfully');
        } else {
            notify('error', 'An error occured while deleting role');
        }

        redirect(site_url('employee/role_setting'));
    }

    public function delete_level($level_id) {
        $this->user_auth_lib->check_login();

        if ($this->basic_model->delete(TBL_LEVELS, ['level_id' => $level_id])) {
            notify('success', 'Level deleted successfully');
        } else {
            notify('error', 'An error occured while deleting level');
        }

        redirect(site_url('employee/level_setting'));
    }

    public function delete_department($id) {
        $this->user_auth_lib->check_login();

        if ($this->basic_model->delete(TBL_DEPT, ['department_id' => $id])) {
            notify('success', 'Department deleted successfully');
        } else {
            notify('error', 'An error occured while deleting department');
        }

        redirect(site_url('/employee/settings'));
    }

}
