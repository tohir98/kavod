<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Attendance_controller
 *
 * @author tohir
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property CI_Loader $load Description
 * @property User_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Employee_model $emp_model Description
 * @property Settings_model $set_model Description
 * @property Attendance_model $att_model Description
 */
class Attendance_controller extends CI_Controller {

    private $company_id;
    private $user_id;

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('user/Employee_model', 'emp_model');
        $this->load->model('user/Settings_model', 'set_model');
        $this->load->model('attendance/Attendance_model', 'att_model');
        $this->load->model('basic_model');
        $this->load->library(['User_nav_lib']);

        $this->_init();
    }

    private function _init() {
        $this->company_id = $this->user_auth_lib->get('company_id');
        $this->user_id = $this->user_auth_lib->get('user_id');
    }

    public function attendance_record() {
        $this->user_auth_lib->check_login();
        $data = [
            'departments' => $this->basic_model->fetch_all_records(TBL_DEPT, ['company_id' => $this->company_id]),
            'locations' => $this->basic_model->fetch_all_records(TBL_LOC, ['company_id' => $this->company_id]),
        ];

        $this->user_nav_lib->run_page('attendance/reports/record', $data, 'Attendance Record | ' . BUSINESS_NAME);
    }

    public function fetchAttendanceReportJson() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $this->output->set_content_type('application/json');
        $date = date('Y-m-d', strtotime($request->date));
        $this->output->_display(json_encode($this->att_model->fetchAttnReport($request->department_id, $request->location_id, $date)));
    }

    public function settings() {
        $this->user_auth_lib->check_login();
        $data = [
            'shifts' => $this->att_model->getShifts(['company_id' => $this->company_id])
        ];
        $this->user_nav_lib->run_page('attendance/settings/shift', $data, 'Attendance Settings | ' . BUSINESS_NAME);
    }

    public function create_shift() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->att_model->getShifts(['company_id' => $this->company_id, 'shift_name' => request_post_data()['shift_name']])) {
                notify('error', 'Shift name already exists, please use another name.');
                redirect(site_url('/attendance/settings'));
            }

            if ($this->att_model->createShift($this->user_id, $this->company_id, request_post_data())) {
                notify('success', "Shift created successfully");
            } else {
                notify('error', "Oops! An error occured while creating shift, please try again");
            }
            redirect(site_url('/attendance/settings'));
        }
    }
    
    public function delete_shift($shift_id) {
        $this->user_auth_lib->check_login();
         if ($this->basic_model->delete(Attendance_model::TBL_SHIFT, ['attendance_shift_id' => $shift_id])) {
            notify('success', 'Shift deleted successfully');
        } else {
            notify('error', 'An error occured while deleting shift');
        }

        redirect(site_url('/attendance/settings'));
    }
    
    public function permissions() {
        $this->user_auth_lib->check_login();
        $pageData = [
            'employees' => $this->emp_model->fetch_all_users($this->company_id),
            'manage_shift_access' => $this->att_model->manage_shift_accessor($this->company_id),
            'permission' => $this->basic_model->fetch_all_records(Attendance_model::TBL_PERM, ['company_id' => $this->company_id])[0],
        ];
        
        $this->user_nav_lib->run_page('attendance/settings/permissions', $pageData, "Permission");
    }
    
    public function access_manage_shift() {
        if (request_is_post()) {
            if (!empty(request_post_data()['employees'])) {
                $this->basic_model->delete(Attendance_model::TBL_SHIFT_ACCESS, ['company_id' => $this->company_id]);
                foreach (request_post_data()['employees'] as $role) {
                    $this->att_model->add(Attendance_model::TBL_SHIFT_ACCESS, ['role' => $role, 'company_id' => $this->company_id]);
                }
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Operation could not be completed, please try again');
            }

            redirect(site_url('/attendance/permissions'));
        }
    }

}
