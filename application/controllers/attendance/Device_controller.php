<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Device_controller
 *
 * @author tohir
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property CI_Loader $load Description
 * @property User_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Employee_model $emp_model Description
 * @property Settings_model $set_model Description
 * @property Device_model $device_model Description
 */
class Device_controller extends CI_Controller {

    private $company_id;
    private $user_id;

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('user/Employee_model', 'emp_model');
        $this->load->model('user/Settings_model', 'set_model');
        $this->load->model('attendance/Device_model', 'device_model');
        $this->load->model('basic_model');
        $this->load->library(['User_nav_lib']);

        $this->_init();
    }

    private function _init() {
        $this->company_id = $this->user_auth_lib->get('company_id');
        $this->user_id = $this->user_auth_lib->get('user_id');
    }
    
    public function devices() {
        $this->user_auth_lib->check_login();
        $data = [
            'devices' => $this->basic_model->fetch_all_records(Device_model::TBL_DEVICES, []),
            'statuses' => Device_model::device_statuses()
        ];

        $this->user_nav_lib->run_page('attendance/device/list', $data, 'Device Mgt | ' . BUSINESS_NAME);
    }

}
