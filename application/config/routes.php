<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
//$route['default_controller'] = 'User/user_controller/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['login']  = 'user/user_controller/login';
$route['logout']  = 'user/user_controller/logout';
$route['forgot_password'] = 'user/user_controller/forgot_password';

// Admin route
$route['admin']  = 'user/admin_controller';
$route['admin/(:any)']  = 'user/admin_controller/$1';
$route['admin/(:any)/(:any)']  = 'user/admin_controller/$1/$2';

// employee
$route['employee']  = 'user/employee_controller';
$route['employee/(:any)']  = 'user/employee_controller/$1';
$route['employee/(:any)/(:any)']  = 'user/employee_controller/$1/$2';
$route['employee/(:any)/(:num)']  = 'user/employee_controller/$1/$2';
$route['employee/(:any)/(:num)/(:any)']  = 'user/employee_controller/$1/$2/$3';
$route['employee/settings/(:any)/(:any)']  = 'user/employee_controller/$1/$2/$3/$4';

// Attendance
$route['attendance']  = 'attendance/attendance_controller';
$route['attendance/(:any)']  = 'attendance/attendance_controller/$1';
$route['attendance/(:any)/(:any)']  = 'attendance/attendance_controller/$1/$2';
$route['attendance/(:any)/(:any)/(:any)']  = 'attendance/attendance_controller/$1/$2/$3';
$route['attendance/(:any)/(:any)/(:any)/(:any)']  = 'attendance/attendance_controller/$1/$2/$3/$4';

// Personal
$route['personal']  = 'user/personal_controller';
$route['personal/(:any)']  = 'user/personal_controller/$1';
$route['personal/(:any)/(:any)']  = 'user/personal_controller/$1/$2';

// Personal
$route['leave']  = 'leave/leave_controller';
$route['leave/(:any)']  = 'leave/leave_controller/$1';
$route['leave/(:any)/(:any)']  = 'leave/leave_controller/$1/$2';
$route['leave/(:any)/(:any)/(:any)']  = 'leave/leave_controller/$1/$2/$3';

// Devices
$route['device']  = 'attendance/device_controller';
$route['device/(:any)']  = 'attendance/device_controller/$1';
$route['device/(:any)/(:any)']  = 'attendance/device_controller/$1/$2';
$route['device/(:any)/(:any)/(:any)']  = 'attendance/device_controller/$1/$2/$3';