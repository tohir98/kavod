<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


if (!function_exists('auth_next_url')) {

    function auth_next_url() {
        $ci = get_instance();
        /* @var $ci Auth */
        $ci->load->library(['session']);
        if ($ci->input->get('next_url')) {
            return $ci->input->get('next_url');
        } else if ($ci->input->post('next_url')) {
            return $ci->input->post('next_url');
        } else if (($next = $ci->session->userdata('next_url'))) {
            $ci->session->unset_userdata('next_url');
            return $next;
        } else {
            return '';
        }
    }

}

if (!function_exists('auth_save_next_url')) {

    function auth_save_next_url($next = '') {

        $ci = get_instance();
        /* @var $ci Auth */
        $ci->load->library(['session']);
        if (!$next) {
            $next = $ci->input->get('next_url') ? : $ci->input->post('next_url');
        }

        if ($next) {
            $ci->session->set_userdata(['next_url' => $next]);
        }
    }

}
