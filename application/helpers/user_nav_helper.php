<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Navigation helper
 */
if (!function_exists('build_top_nav_part')) {

    function build_top_nav_part() {
        
    }

}

if (!function_exists('build_sidebar_nav_part')) {

    function build_sidebar_nav_part($nav_menu, $path) {
        $buffer = '';


        $buffer .= '<div class="subnav subnav-hidden">';
        foreach ($nav_menu as $module => $item) {
            $active = strstr($path, $item['id_string']) ? 'open' : '';
            $buffer .= '<div class="subnav-title">
                <a href="#" class="toggle-subnav">
                <i class="icon-angle-down"></i> 
                <span>' . $module . '</span> 
              </a>
              </div>';
            
            if (count($item) > 0) {

                $buffer .= '<ul class="subnav-menu">';
                foreach ($item['items'] as $sub) {

                    $buffer .= '<li>'
                        . '<a href="' . site_url($sub['module_id_string'] . '/' . $sub['perm_id_string']) . '" >' . $sub['perm_subject'] . '</a></li>';
                }
                $buffer .= '</ul>';
            }
            
        }
        $buffer .= '</div>';
        return $buffer;
    }

}

if (!function_exists('user_type_status')) {

    function user_type_status($access_level) {
        $arr = array(
            USER_TYPE_ADMIN => "Admin",
            USER_TYPE_SUPERVISOR => "Supervisor",
            USER_TYPE_USER => "Staff",
        );

        return $arr[$access_level];
    }

}

if (!function_exists('nav_icon')) {

    function nav_icon($module) {
        $icon = '';
        if ($module === 'account') {
            $icon = 'dashboard';
        } elseif ($module === 'setup') {
            $icon = 'cogs';
        } elseif ($module === 'brach_office') {
            $icon = 'calendar';
        } elseif ($module === 'rep_countries') {
            $icon = 'tint';
        } elseif ($module === 'rep_institutions') {
            $icon = 'building-o';
        } elseif ($module === 'reports') {
            $icon = 'envelope';
        } elseif ($module === 'lead_mgt') {
            $icon = 'dashboard';
        } else {
            $icon = 'dashboard';
        }

        return $icon;
    }

}

