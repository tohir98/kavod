<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Page_nav.php';

/**
 * Description of Admin_nav_lib
 *
 * @author tohir
 */
class Admin_nav_lib extends page_nav {
    
    protected $CI;
    private $user_id;
    private $account_type;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('user_auth_lib');
        $this->CI->load->helper('user_nav_helper');


    }
    
    public function get_user_link() {
        
        return 'admin';
    }
  

    public function get_top_menu() {
        return array(
            'dashboard_url' => '/' . $this->get_user_link() . '/dashboard',
            'logout_url' => site_url('/logout'),
            'display_name' => 'KAVOD Admin',
        );
    }

    public function company_name() {
        return BUSINESS_NAME;
    }

}
