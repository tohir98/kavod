<div class="page-header">
    <div class="pull-left">
        <h1>Dashboard <small>Control panel</small></h1>
    </div>
    <div class="clear"></div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="/"><i class="icons icon-home"></i> Home</a>
        </li>
        <li>
            <a href="/">Dashboard</a>
        </li>
    </ul>
</div>

<div class="row-fluid">
    <div class="span12">
        <ul class="tiles">
            <li class="purple long">
                <a href="#">
                    <span></span>
                    <span class="name">Pending Exemptions</span>
                </a>
            </li>
            <li class="green long">
                <a href="<?= site_url('/employee/staff_directory'); ?>"><span><?php echo count($active_staff); ?></span>
                    <span class="name">Active Staff</span>
                </a>
            </li>
            <li class="red long">
                <a href="<?= site_url('/employee/staff_directory'); ?>"><span><?php echo count($inactive_staff); ?></span><span class="name">Inactive Staff</span></a>
            </li>
            <li class="orange long">
                <a href="#"><span><i class="icon-globe"></i></span><span class="name">Staff on Leave</span></a>
            </li>
        </ul>
    </div>
</div>

<div class="row-fluid">
    <div class="span6">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-calendar"></i>My calendar</h3>
            </div>
            <div class="box-content nopadding">
                <div class="calendar"></div>
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="box box-color box-bordered green">
            <div class="box-title">
                <h3><i class="icon-bullhorn"></i>Activity Feeds</h3>
                <div class="actions">
                    <a href="#" class="btn btn-mini custom-checkbox checkbox-active">Automatic refresh<i class="icon-check-empty"></i></a>
                </div>
            </div>
            <div class="box-content nopadding scrollable" data-height="400" data-visible="true">
                <table class="table table-nohead" id="randomFeed">
                    <tbody>
                        <tr>
                            <td><span class="label"><i class="icon-plus"></i></span> <a href="#">John Doe</a> added a new photo</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"><i class="icon-user"></i></span> New user registered</td>
                        </tr>
                        <tr>
                            <td><span class="label label-info"><i class="icon-shopping-cart"></i></span> New order received</td>
                        </tr>
                        <tr>
                            <td><span class="label label-warning"><i class="icon-comment"></i></span> <a href="#">John Doe</a> commented on <a href="#">News #123</a></td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"><i class="icon-user"></i></span> New user registered</td>
                        </tr>
                        <tr>
                            <td><span class="label label-info"><i class="icon-shopping-cart"></i></span> New order received</td>
                        </tr>
                        <tr>
                            <td><span class="label label-warning"><i class="icon-comment"></i></span> <a href="#">John Doe</a> commented on <a href="#">News #123</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>