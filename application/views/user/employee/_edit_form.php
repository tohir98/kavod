<form class="form-horizontal" method="post" action="">
    <table class="table table-condensed">
        <tr>
            <td>First Name</td>
            <td style="width: 33%"><input required type="text" name="first_name" id="first_name" class="form-control" value="<?= isset($emp_details->first_name) ? $emp_details->first_name : '' ?>" /></td>
            <td>Last Name</td>
            <td><input required type="text" name="last_name" id="last_name" class="form-control" value="<?= isset($emp_details->last_name) ? $emp_details->last_name : '' ?>" /></td>
        </tr>
        <tr>
            <td>Date of Birth</td>
            <td><input required type="text" name="Birthday" id="Birthday" class="form-control" value="<?= isset($emp_details->Birthday) ? date('d/m/Y', strtotime($emp_details->Birthday)) : '' ?>" /></td>
            <td>Gender</td>
            <td>
                <select required id="gender_id" name="gender_id" class="form-control">
                    <option value="">Select Gender</option>
                    <?php
                    if (!empty($genders)):
                        $sel = '';
                        foreach ($genders as $gender):
                            if ($gender->gender_id == $emp_details->gender_id):
                                $sel = 'selected';
                            else:
                                $sel = '';
                            endif;
                            ?>
                    <option value="<?= $gender->gender_id ?>" <?= $sel;?>><?= trim($gender->gender); ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input required type="text" name="email" id="email" class="form-control" value="<?= isset($emp_details->email) ? $emp_details->email : '' ?>" /></td>
            <td>Phone</td>
            <td>
                <input required type="text" name="pager" id="pager" maxlength="11" class="form-control" value="<?= isset($emp_details->pager) ? $emp_details->pager : '' ?>" />
            </td>
        </tr>
        <tr>
            <td>Address</td>
            <td><input required type="text" name="address" id="address" class="form-control" value="<?= isset($emp_details->address) ? $emp_details->address : '' ?>" /></td>
            <td>City</td>
            <td>
                <input required type="text" name="City" id="City" class="form-control" value="<?= isset($emp_details->City) ? $emp_details->City : '' ?>" />
            </td>
        </tr>
        <tr>
            <td>Country</td>
            <td>
                <select required id="country_id" name="country_id" class="form-control">
                    <option value="">Select Country</option>
                    <?php
                    if (!empty($countries)):
                        $sel = '';
                        foreach ($countries as $country):
                            if ($country->country_id == $emp_details->country_id):
                                $sel = 'selected';
                            else:
                                $sel = '';
                            endif;
                            ?>
                    <option value="<?= $country->country_id ?>" <?= $sel;?>><?= trim($country->country); ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td>State</td>
            <td>
                <select required id="state_id" name="state_id" class="form-control">
                    <option value="">Select State</option>
                    <?php
                    if (!empty($states)):
                        $sel = '';
                        foreach ($states as $state):
                        if ($state->state_id == $emp_details->state_id):
                                $sel = 'selected';
                            else:
                                $sel = '';
                            endif;
                            ?>
                            <option value="<?= $state->state_id ?>" <?= $sel;?>><?= $state->state ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <button class="btn btn-primary" type="submit">Update</button>
                <button class="btn btn-warning" type="reset">Reset</button>
            </td>
        </tr>
    </table>
</form>