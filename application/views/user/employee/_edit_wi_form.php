<form class="form-horizontal" method="post" action="">
    <table class="table table-condensed">
        <tr>
            <td>Staff ID</td>
            <td style="width: 33%"><input required type="text" name="staff_id" id="staff_id" class="form-control" value="<?= isset($work_info->staff_id) ? $work_info->staff_id : '' ?>" /></td>
            <td>Employment Date</td>
            <td><input required type="text" name="employment_date" id="employment_date" class="form-control" value="<?= isset($work_info->employment_date) ? date('d/m/Y', strtotime($work_info->employment_date)) : '' ?>" /></td>
        </tr>
        <tr>
            <td>Department</td>
            <td>
                <select id="department_id" name="department_id" class="form-control">
                    <option value="">Select Department</option>
                    <?php
                    if (!empty($departments)):
                        $sel = '';
                        foreach ($departments as $dept):
                            if ($dept->department_id == $work_info->department_id):
                                $sel = 'selected';
                            else:
                                $sel = '';
                            endif;
                            ?>
                            <option value="<?= $dept->department_id ?>" <?= $sel; ?>><?= trim($dept->department); ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            <td>Location</td>
            <td>
                <select id="location_id" name="location_id" class="form-control">
                    <option value="">Select Location</option>
                    <?php
                    if (!empty($locations)):
                        $sel = '';
                        foreach ($locations as $loc):
                            if ($loc->location_id == $work_info->location_id):
                                $sel = 'selected';
                            else:
                                $sel = '';
                            endif;
                            ?>
                            <option value="<?= $loc->location_id ?>" <?= $sel; ?>><?= trim($loc->location_tag); ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Role</td>
            <td>
                <select required id="role_id" name="role_id" class="form-control">
                    <option value="">Select Role</option>
                    <?php
                    if (!empty($roles)):
                        $sel = '';
                        foreach ($roles as $role):
                            if ($role->role_id == $work_info->role_id):
                                $sel = 'selected';
                            else:
                                $sel = '';
                            endif;
                            ?>
                            <option value="<?= $role->role_id ?>" <?= $sel; ?>><?= trim($role->role); ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td>Level</td>
            <td>
                <select id="level_id" name="level_id" class="form-control">
                    <option value="">Select level</option>
                    <?php
                    if (!empty($levels)):
                        $sel = '';
                        foreach ($levels as $level):
                            if ($level->level_id == $work_info->level_id):
                                $sel = 'selected';
                            else:
                                $sel = '';
                            endif;
                            ?>
                            <option value="<?= $level->level_id ?>" <?= $sel; ?>><?= $level->level ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Employment Type</td>
            <td>
                <select name="employment_type_id" id="employment_type_id" class="select2-me input-large">
                    <option>Employment Type</option>
                    <?php if (!empty($employment_types)):
                        foreach ($employment_types as $type):
                            ?>
                            <option value="<?= $type->employment_type_id; ?>"><?= $type->employment_type; ?></option>
                        <?php endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td>Working Shift</td>
            <td>
                <select name="attendance_shift_id" id="attendance_shift_id" class="select2-me input-large">
                    <option selected>Select Shift</option>
                    <?php if (!empty($working_shifts)):
                        foreach ($working_shifts as $shift):
                            ?>
                            <option value="<?= $shift->attendance_shift_id; ?>"><?= $shift->shift_name; ?></option>
                        <?php endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>End of Contract</td>
            <td><input required type="text" name="endof_contract" id="endof_contract" class="datepick hide"  /></td>
            <td>&nbsp;</td>
            <td>
                <button class="btn btn-primary" type="submit">Update</button>
                <button class="btn btn-warning" type="reset">Reset</button>
            </td>
        </tr>
    </table>
</form>

<script>
    $("#employment_type_id").change(function(){
        console.log($("#employment_type_id").val());
        if (parseInt($("#employment_type_id").val()) == 5){
            $("#endof_contract").show();
        }else{
            $("#endof_contract").hide();
        }
    });
</script>