<?= show_notification();
?>

<div class="page-header">
    <div class="pull-left">
        <a class="btn btn-warning" href="<?= $this->input->server('HTTP_REFERER') ? : site_url('/employee/staff_directory'); ?>">
            <i class="fa fa-arrow-left"></i> Back
        </a>
        <h1>Edit Staff Information</h1>
    </div>
    <div class="clear"></div>
</div>

<div class="breadcrumbs">
    <ul>
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff Records</a></li>
        <li><a href="#">Edit</a></li>
    </ul>
</div>

<div class="clear"></div>

<!-- Main content -->
    <div class="row-fluid">
        <div class="span12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="<?= site_url('/employee/edit_personal_info/' . $this->uri->segment(3)); ?>" aria-expanded="false">Personal Information</a>
                    </li>
                    <li class="">
                        <a href="<?= site_url('/employee/edit_work_info/' . $this->uri->segment(3)); ?>" aria-expanded="true">Work Information</a>
                    </li>
                </ul>
            </div>
            <div class="box box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icons icon-edit"></i>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-content">
                    <?php include '_edit_form.php'; ?>
                </div>
            </div>
        </div>
    </div>

<script>
    $('document').ready(function () {
        $('#Birthday').datepicker();
    });
</script>