<script>
    window.studentDirConfig = window.staffDirConfig || {};
    window.studentDirConfig.employees = <?= json_encode($employees) ?>;
    window.studentDirConfig.account_statuses = <?= json_encode($account_statuses) ?>;

</script>

<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables-all.js" type="text/javascript"></script>
<script src="/js/staff_directory.js" type="text/javascript"></script>

<?= show_notification();
?>

<div class="page-header">
    <div class="pull-left">
        <h1>Employee Directory <small>Employees Record &AMP; Information</small></h1>
    </div>
    <div class="clear"></div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="/">Dashboard</a>
        </li>
        <li class="active">Staff Records</li>
    </ul>
</div>

<!-- Main content -->
<div class="row-fluid" ng-app="studentDir">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>Current Employees</h3>
            </div><!-- /.box-header -->
            <div class="box-content-padless" ng-controller="listCtrl">
                <?php
                if (!empty($employees)):
                    ?>
                    <table class="table table-hover dataTable" ng-cloak="">
                        <thead>
                            <tr>
                                <th><input type="checkbox" ng-click="toggleAll()" ></th>
                                <th style="width: 50px">&nbsp;</th>
                                <th>PIN</th>
                                <th>Employee</th>
                                <th>Status</th>
                                <th>Contact Details</th>
                                <th>Date of Birth</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="employee in config.employees| filter:filter">
                                <td>
                                    <input type="checkbox" value="{{employee.employee_id}}" name="employees[]" ng-model="employee.is_selected" >
                                </td>
                                <td>
                                    <img ng-src="{{employee.profile_picture_url}}" width="40px" height="40px">
                                </td>
                                <td>
                                    <a href="<?= site_url('employee/view_record'); ?>/{{employee.userid}}/{{employee.first_name}}-{{employee.last_name}}">
                                        {{employee.badgenumber}}
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= site_url('employee/view_record'); ?>/{{employee.userid}}/{{employee.first_name}}-{{employee.last_name}}">
                                        {{employee.first_name| uppercase}} {{employee.last_name| uppercase}}
                                    </a>
                                </td>
                                <td>{{config.account_statuses[employee.status]}}</td>
                                <td>
                                    {{employee.email}} <br>
                                    {{employee.pager}}
                                </td>
                                <td>{{formatDate(employee.Birthday) | date:'dd/MM/yyyy'}} </td>
                                <td>
                                    <a href="<?= site_url('employee/view_record'); ?>/{{employee.userid}}/{{employee.first_name}}-{{employee.last_name}}">
                                        <i class="icons icon-edit"></i> Edit</a>
                                    <span ng-if="employee.email !== ''"> |
                                        <a href="#" onclick="return false;" ng-click="sendEmail(employee)">
                                            <i class="icons icon-envelope"></i> Send Email</a>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<div id="modal_send_sms" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Send SMS</h4>
    </div>
    <form role="form" method="post" class="form-horizontal" action="<?= site_url('communication/send_sms') ?>">
        <div class="modal-body">
            <span id="lblRecepientName" class=""></span>
            <span id="lblrecepient" class="label label-warning"></span>
            <input type="hidden"  id="recepient" name="recepient">
            <br>
            <br>

            <textarea required name="message" id="message" rows="3" style="width: 80%" maxlength="140"></textarea>
            <div style="color: red">Max. of 140 letters</div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" >Send</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>

<div id="modal_send_email" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Send Email</h4>
    </div>
    <form role="form" method="post" class="form-horizontal" action="<?= site_url('communication/send_email') ?>">
        <div class="modal-body">
            <span id="lblRecepientName_email" class=""></span>
            <span id="lblrecepient_email" class="label label-warning"></span>
            <input type="text"  id="recepient_email" name="recepient" style="width: 80%" readonly="">
            <br>
            <br>
            <input type="text"  id="sender" name="sender" style="width: 80%" value="<?= $this->user_auth_lib->get('email') ?>" >
            <br>
            <br>
            <input type="text"  id="email_subject" name="email_subject" style="width: 80%" placeholder="Subject">
            <br>
            <br>

            <textarea required name="message" rows="3" style="width: 80%"></textarea>


        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" >Send</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        </div>
    </form>

</div>
