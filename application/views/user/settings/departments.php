<?php include '_settings_tab.php'; ?>

<!-- Main content -->

<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>Departments</h3>
                <a style="margin-right: 5px" href="#new_dept" data-toggle="modal" class="btn btn-success pull-right">Add New Department</a>
            </div><!-- /.box-header -->
            <div class="box-content-padless">
                <?php
                if (!empty($departments)):
                    ?>
                    <table class="table table-hover dataTable">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Department</th>
                                <th>Parent</th>
                                <th>Department Head</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            foreach ($departments as $dept):
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= $dept->department ?></td>
                                    <td><?= $dept->parent ?></td>
                                    <td><?= $dept->dept_head; ?></td>
                                    <td><?= $dept->first_name . ' ' . $dept->last_name; ?></td>
                                    <td>
                                        <a title="Edit department" href="<?= site_url('employee/settings/edit_department/' . $dept->department_id) ?>">
                                            <i class="icons icon-edit"></i> Edit
                                        </a>
                                        <a title="Delete department" href="<?= site_url('employee/settings/delete_department/' . $dept->department_id) ?>" class="delete_">
                                           <i class="icons icon-trash"></i> Delete
                                        </a>    
                                    </td>
                                </tr>
    <?php endforeach; ?>
                        </tbody>
                    </table>
<?php endif; ?>
            </div>
        </div>
    </div>
</div>


<?php include '_new_dept.php'; ?>

<script>
    $(function () {
        $('.delete_').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this department ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>