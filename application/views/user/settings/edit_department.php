<?= show_notification();
?>

<?php
$depart = $depts->department;
?>

<div class="page-header">
    <div class="pull-left">
        <a class="btn btn-warning btn-flat btn-sm" href="<?= $this->input->server('HTTP_REFERER') ? : site_url('/employee/settings'); ?>">
            <i class="icons icon-arrow-left"></i> Back
        </a>
        <h1>Edit Department</h1>
    </div>
    <div class="clear"></div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="/">Dashboard</a>&raquo;
        </li>
        <li class="active">Staff Records&raquo;</li>
        <li class="active">Departments&raquo;</li>
        <li><a href="#">Edit Department</a></li>
    </ul>
</div>


<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3><i class="icons icon-edit"></i></h3>
            </div><!-- /.box-header -->
            <div class="box-content-padless">

                <form method="post" class="form-horizontal" action="<?= site_url('/employee/settings/edit_department/' . $depts->department_id); ?>">
                    <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                        <div class="control-group">
                            <label class="control-label" for="department">Department</label>
                            <div class='controls'>
                                <input required type="text" class="form-control" id="department" name="department" value="<?= isset($depart) ? $depart : '' ?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" >Parent Department</label>
                            <div class='controls'>
                                <select required name="parent_id" id="parent_id" class="select2-me input-large">
                                    <option value="">Select Parent</option>
                                    <?php
                                    if (!empty($department_details)):
                                        foreach ($department_details as $dept):
                                            $sel = '';
                                            if ($depts->parent_id == $dept->department_id):
                                                $sel = 'selected';
                                            else:
                                                $sel = '';
                                            endif;
                                            ?>
                                            <option value="<?= $dept->department_id ?>" <?= $sel; ?>><?= trim($dept->department); ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Department Head</label>
                            <div class="controls">
                                <select class="select2-me input-large" name="department_head_id">
                                    <option>Select Department Head</option>
                                    <?php
                                    if (!empty($employees)):
                                        foreach ($employees as $emp):
                                            $sel = '';
                                            if ($depts->department_head_id == $emp->userid):
                                                $sel = 'selected';
                                            else:
                                                $sel = '';
                                            endif;
                                            ?>
                                            <option value="<?= $emp->userid ?>" <?= $sel; ?>><?= trim($emp->first_name . ' ' . $emp->last_name); ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Update</button>
                        <button class="btn btn-warning" type="reset">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
