<div id="new_location" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create New Shift</h4>
    </div>
    <form role="form" method="post" class="form-horizontal" action="<?= site_url('/employee/location_setting') ?>">
        <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
            <div class="form-group">
                <label for="location_tag">Location Tag</label>
                <input required type="text" class="form-control" id="location_tag" name="location_tag" placeholder="Location Tag">
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <input required type="text" class="form-control" id="address" name="address" placeholder="Address">
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input required type="text" class="form-control" id="city" name="city" placeholder="City">
            </div>
            <div class="form-group">
                <label for="state_id">State</label>
                <select required id="state_id" name="state_id" class="form-control">
                    <option value="">Select State</option>
                    <?php
                    if (!empty($states)):
                        foreach ($states as $state):
                            ?>
                            <option value="<?= $state->state_id ?>"><?= $state->state ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" >Create</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>