<div class="modal" id="new_level">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New Level</h4>
            </div>
            <form role="form" method="post" class="form-horizontal" action="<?= site_url('/employee/level_setting') ?>">
                <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                    <div class="form-group">
                        <label for="role">Level</label>
                        <input required type="text" class="form-control" id="level" name="level" placeholder="Level">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Create</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>