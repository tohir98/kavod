<?php include '_settings_tab.php'; ?>


<!-- Main content -->
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>
                    Company Locations
                </h3>
                <a style="margin-right: 5px" href="#new_location" data-toggle="modal" class="btn btn-success pull-right">Add New Location</a>
            </div><!-- /.box-header -->
            <div class="box-title">
                <?php
                if (!empty($locations)):
                    ?>
                    <table class="table table-hover dataTable">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Location</th>
                                <th>Address</th>
                                <th>State</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            foreach ($locations as $loc):
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= $loc->location_tag ?></td>
                                    <td><?= $loc->address ?></td>
                                    <td><?= $loc->state ?></td>
                                    <td><?= $loc->first_name . ' ' . $loc->last_name; ?></td>
                                    <td>
                                        <a title="Click here to edit" class="button green" href="<?= site_url('employee/settings/edit_location/' . $loc->location_id) ?>">
                                            <i class="icons icon-edit"></i> Edit
                                        </a> 
                                        <a title="Click here to delete" href="<?= site_url('employee/settings/delete_location/' . $loc->location_id) ?>" class="button red delete_">
                                            <i class="icons icon-trash"></i> Delete
                                        </a> 
                                    </td>
                                </tr>
    <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php
                else:
                    echo 'No location has been added.';
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<?php include '_new_location.php'; ?>

<script>
    $(function () {
        $('.delete_').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this location ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>