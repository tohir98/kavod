<?= show_notification();
?>

<section class="content-header">
    <h1>
        <a class="btn btn-warning btn-flat btn-sm" href="<?= $this->input->server('HTTP_REFERER') ? : site_url('/employee/role_setting'); ?>">
            <i class="fa fa-arrow-left"></i> Back
        </a>
        Edit Role
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff Records</a></li>
        <li><a href="#">Edit Role</a></li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    &nbsp;
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form role="form" method="post" class="form-horizontal" action="<?= site_url('/employee/settings/edit_role/'.$role->role_id); ?>">
                        <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                            <div class="form-group">
                                <label for="role">Role</label>
                                <input required type="text" class="form-control" id="role" name="role" value="<?= isset($role->role) ? $role->role : '' ?>">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" >Update</button>
                            <button class="btn btn-warning" type="reset">Reset</button>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>