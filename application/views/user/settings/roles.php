<?php include '_settings_tab.php'; ?>


<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="head">
                <h3>
                    Roles
                    <a href="#new_role" data-toggle="modal" class="btn btn-success pull-right">Add New Role</a>
                </h3>
            </div><!-- /.box-header -->
            <div class="data-fluid">
                <?php
                if (!empty($roles)):
                    ?>
                    <table class="table table-hover dtable lcnp ">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Role</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            foreach ($roles as $role):
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= $role->role ?></td>
                                    <td><?= $role->first_name . ' ' . $role->last_name; ?></td>
                                    <td>
                                        <a title="Click here to edit" class="button green" href="<?= site_url('employee/settings/edit_role/' . $role->role_id) ?>">
                                            <div class="icon"><span class="ico-pencil"></span></div>
                                        </a> 
                                        <a title="Click here to delete" href="<?= site_url('/employee/settings/delete_role/' . $role->role_id); ?>" class="button red">
                                            <div class="icon"><span class="ico-remove"></span></div>
                                        </a> 
                                    </td>
                                </tr>
    <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php
                else:
                    echo 'No role has been added.';
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<?php include '_new_role.php'; ?>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this role ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>