<div id="new_dept" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Add New Department</h4>
    </div>
    <form role="form" method="post" class="form-horizontal" action="<?= site_url('/employee/settings') ?>">
        <div class="modal-body" style="padding-left: 30px; padding-right: 30px">

            <div class="control-group">
                <label class="control-label">Department</label>
                <div class="controls">
                    <input required type="text" class="form-control" id="department" name="department" placeholder="Department Name">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Parent Department</label>
                <div class="controls">
                    <select required name="parent_id" id="parent_id" class="form-control">
                        <option value="">Select Parent</option>
                        <?php
                        if (!empty($departments)):
                            foreach ($departments as $dept):
                                ?>
                                <option value="<?= $dept->department_id ?>"><?= trim($dept->department); ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Department Head</label>
                <div class="controls">
                    <select class="select2-me input-large" name="department_head_id">
                        <option>Select Department Head</option>
                        <?php
                        if (!empty($employees)):
                            foreach ($employees as $emp):
                                ?>
                                <option value="<?= $emp->userid ?>"><?= trim($emp->first_name . ' ' . $emp->last_name); ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" >Create</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>
