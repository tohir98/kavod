<?= show_notification();
?>
<div class="page-header">
    <div class="pull-left">
        <h1>Settings <small>Department, Locations, Roles &AMP; Levels</small></h1>
    </div>
    <div class="clear"></div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="/">Dashboard</a>&raquo;
        </li>
        <li class="active">Staff Records&raquo;</li>
        <li class="active">Departments</li>
    </ul>
</div>

<?php $path = $this->input->server('REQUEST_URI'); ?>
<div class="row">
    <div class="col-md-12" style="margin-left: 15px; margin-top: 15px">
        <ul class="nav nav-tabs" id="myTab">
            <li class="<?= strstr($path, 'employee/settings') ? 'active' : '' ?>">
                <a href="<?= site_url('/employee/settings') ?>" >Departments</a>
            </li>
            <li class="<?= strstr($path, 'employee/location_setting') ? 'active' : '' ?>">
                <a href="<?= site_url('/employee/location_setting') ?>" >Locations</a>
            </li>
            <li class="<?= strstr($path, 'employee/role_setting') ? 'active' : '' ?>">
                <a href="<?= site_url('/employee/role_setting') ?>" >Roles</a>
            </li>
            <li class="<?= strstr($path, 'employee/level_setting') ? 'active' : '' ?>">
                <a href="<?= site_url('/employee/level_setting') ?>" >Levels</a>
            </li>
        </ul>
    </div>
</div>