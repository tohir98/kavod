<?= show_notification();
?>

<?php
$location_tags = $locations->location_tag;
$location_address = $locations->address;
$location_city = $locations->city;
?>

<section class="content-header">
    <h1>
        <a class="btn btn-warning btn-flat btn-sm" href="<?= $this->input->server('HTTP_REFERER') ? : site_url('/employee/location_settings'); ?>">
            <i class="fa fa-arrow-left"></i> Back
        </a>
        Edit Location
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff Records</a></li>
        <li><a href="#">Edit Location</a></li>
    </ol>
</section>



<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    &nbsp;
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form role="form" method="post" class="form-horizontal" action="<?= site_url('/employee/settings/edit_location/'.$locations->location_id); ?>">
                        <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                            <div class="form-group">
                                <label for="location_tag">Location Tag</label>
                                <input required type="text" class="form-control" id="location_tag" name="location_tag" value="<?= isset($location_tags) ? $location_tags : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input required type="text" class="form-control" id="address" name="address" value="<?= isset($location_address) ? $location_address : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="city">City</label>
                                <input required type="text" class="form-control" id="city" name="city" value="<?= isset($location_city) ? $location_city : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="state_id">State</label>
                                <select required id="state_id" name="state_id" class="form-control">
                                    <option value="">Select State</option>
                                    <?php
                                    if (!empty($states)):
                                        foreach ($states as $state):
                                            ?>
                                            <option value="<?= $state->state_id ?>"><?= $state->state ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" >Update</button>
                            <button class="btn btn-warning" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
