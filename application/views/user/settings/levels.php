<?php include '_settings_tab.php'; ?>


<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="head">
                <h3>
                    Levels
                    <a href="#new_level" data-toggle="modal" class="btn btn-success pull-right">Add New Level</a>
                </h3>
            </div><!-- /.box-header -->
            <div class="data-fluid">
                <?php
                if (!empty($levels)):
                    ?>
                    <table class="table table-hover dtable lcnp">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Level</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sn = 0;
                            foreach ($levels as $level):
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= $level->level ?></td>
                                    <td><?= $level->first_name . ' ' . $level->last_name; ?></td>
                                    <td>
                                        <a title="Click here to edit" class="button green" href="<?= site_url('employee/settings/edit_level/' . $level->level_id); ?>">
                                            <div class="icon"><span class="ico-pencil"></span></div>
                                        </a> 
                                        <a title="Click here to delete" href="<?= site_url('employee/settings/delete_level/' . $level->level_id); ?>" class="button red">
                                            <div class="icon"><span class="ico-remove"></span></div>
                                        </a> 
                                    </td>
                                </tr>
    <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php
                else:
                    echo 'No level has been added.';
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<?php include '_new_level.php'; ?>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this level ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>