<div class="box box-solid box-primary student_info_box" id="profile_container">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Profile
        </h3>
    </div>
    <div class="box-body">
        <h4>
            Work Information
            <a class="pull-right" href="<?= site_url('/employee/edit_work_info/' . $p_info->userid) ?>" style="font-size: small">edit</a>
        </h4>
        <table class="table table-striped table-condensed">
            <tr>
                <td>
                    Name:
                </td>
                <td>
                    <?= ucfirst($p_info->first_name) . ' ' . ucfirst($p_info->last_name) ?>
                </td>
            </tr>
            <tr>
                <td>Staff ID</td>
                <td><?= $w_info[0]->staff_id ?></td>
            </tr>
            <tr>
                <td>Department</td>
                <td><?= $w_info[0]->department ?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?= $w_info[0]->location_tag ?></td>
            </tr>
            <tr>
                <td>Roles</td>
                <td><?= $w_info[0]->role ?></td>
            </tr>
            <tr>
                <td>Level</td>
                <td><?= $w_info[0]->level ?></td>
            </tr>
        </table>
        <hr>
        <h4>
            Personal Information
        <a class="pull-right" href="<?= site_url('/employee/edit_personal_info/' . $p_info->userid) ?>" style="font-size: small">edit</a>
        </h4>
        <table class="table table-striped table-condensed">
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <?= $p_info->email ?>
                </td>
            </tr>
            <tr>
                <td>
                    Birthday:
                </td>
                <td>
                    <?= $p_info->Birthday !== '0000-00-00' ? date('d-M-Y', strtotime($p_info->Birthday)) : 'NA' ?>
                </td>
            </tr>
            <tr>
                <td>
                    Address:
                </td>
                <td>
                    <?= $p_info->address ?>
                </td>
            </tr>
            <tr>
                <td>
                    City:
                </td>
                <td>
                    <?= $p_info->City ?>
                </td>
            </tr>
            <tr>
                <td>
                    State:
                </td>
                <td>
                    <?= $p_info->state ?>
                </td>
            </tr>
            <tr>
                <td>
                    Country:
                </td>
                <td>
                    <?= $p_info->country ?>

                </td>
            </tr>
            <tr>
                <td>
                    Gender:
                </td>
                <td>
                    <?= $p_info->gender ?>
                </td>
            </tr>
            <td>
                Phone Number:
            </td>
            <td>
                <?= $p_info->phone ?>
            </td>
            </tr>
        </table>
    </div>
</div>