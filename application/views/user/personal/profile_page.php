<?= show_notification();
?>
<section class="content-header">
    <h1>
        <?php if ($this->uri->segment(2) === 'view_record'): ?>
            <a href="<?= site_url('/employee/staff_directory') ?>" class="btn btn-flat btn-warning">
                <i class="icon icon-chevron-left"></i> Back
            </a>
        <?php endif; ?>

        <a href="#" class="btn btn-flat btn-primary">
            <i class="icons icon-print"></i> Print
        </a>
        <a href="#" class="btn btn-flat btn-primary">
            <i class="icons icon-file"></i> PDF
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Records</a></li>
    </ol>
</section>

<div class="row-fluid">
    <?php if ($this->uri->segment(2) === 'view_record'): ?>
        
            <select class="select2-me input-xxlarge employees" name="cboStaffs" id="cboStaffs">
                <option></option>
                <?php
                if (!empty($employees)):
                    $sel = '';
                    foreach ($employees as $emp):
                        $sel = $emp->userid == $p_info->userid ? 'selected' : '';
                        ?>
                        <option value="<?= $emp->userid ?>" data-first_name="<?= ucfirst($emp->first_name) ?>" data-last_name="<?= ucfirst($emp->last_name) ?>" <?= $sel ?>><?= ucfirst($emp->first_name) . ' ' . ucfirst($emp->last_name); ?> (<?= $emp->badgenumber;?>)</option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
    <?php endif; ?>
    <div class="row-fluid" style="margin-top: 20px">
        <div class="span2">
            <div class="box box-solid">
                <div class="box-header with-border" style="border:1px solid #CCCCCC;height:161px;padding:0;">
                     <?php
                        $pic_src = "/img/profile-default.jpg";
                        if (!empty($profile_picture)):
                            $pic_src = $profile_picture->profile_picture_url;
                        endif;
                        ?>
                    <div class="row-fluid" style="height: 180px;overflow:hidden">
                        <img src="<?= $pic_src; ?>" style="max-width: 150px">
                        <br>
                        <!--<button class="btn btn-flat btn-primary">Edit Picture</button>-->
                    </div>
                </div>
                <div class="box-body no-padding" style="margin-top: 10px">
                    <div class="btn-group" style="width: 100%">
                        <button type="button" style="width: 100%" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Edit
                            <span class="caret"></span> 
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= site_url('/employee/change_profile_picture/' . $p_info->userid . '/' . $p_info->first_name . '-' . $p_info->last_name) ?>">Change Picture</a></li>
                            <?php if ($profile_picture): ?>
                                <li><a class="remove_pic" href="<?= site_url('/employee/remove_profile_picture/' . $p_info->userid) ?>">Remove Picture</a></li>
                            <?php endif; ?>

                        </ul>
                    </div>
                </div>
                <div class="box-bordered" style="margin-top: 10px">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a class="left_menu_button" href="#profile_container"> Profile </a></li>
                        <li> <a class="left_menu_button" href="#emergency_container">Emergency Details</a></li>
                        <li> <a class="left_menu_button" href="#bank_container">Bank Details</a></li>
                        <li> <a class="left_menu_button" href="#pension_container">Pension Details</a></li>
                    </ul>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="span8">
            <?php
            echo $profile_content;
            echo $emergency_content;
            echo $bank_content;
            echo $pension_content;
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        
        $(function () {
            $('.remove_pic').click(function (e) {
                e.preventDefault();
                var h = this.href;
                var message = 'Are you sure you want to remove this profile picture ?';
                Kavod.doConfirm({
                    title: 'Confirm ',
                    message: message,
                    onAccept: function () {
                        window.location = h;
                    }
                });
            });
        });

        $('.left_menu_button').click(function () {
            $('.left_menu_button').parent().removeClass('active');
            $(this).parent().addClass('active');

            $('.student_info_box').hide();

            var container_id = $(this).attr('href');
            $(container_id).show();
            window.location.href = container_id;
            return false;
        });

        if (window.location.hash !== '' && window.location.hash !== '#') {
            var k = window.location.hash.substring();
            $('.left_menu_button[href="' + k + '"]').click();
        }


        $('.employees').change(function () {
            var id_string = $(this).val();
            var first_name = $(this).find(':selected').attr('data-first_name');
            var last_name = $(this).find(':selected').attr('data-last_name');
            window.location.href = '<?php echo site_url('employee/view_record'); ?>/' + id_string + '/' + first_name + '-' + last_name;
        });




    });


</script>
