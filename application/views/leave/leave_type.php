<?php include '_leave_tab.php'; ?>

<!-- Main content -->

<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="head">
                <h3>Leave Types
                    <a href="#new_ltype" data-toggle="modal" class="btn btn-success pull-right">Add Leave Type</a>
                </h3>

            </div><!-- /.box-header -->
            <div class="data-fluid">
            	<?php
                if (!empty($type)){
                    ?>
                    <table class="table table-hover dtable lcnp">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Leave Type</th>
                                <th>Carry Over</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                            $sn = 0;
                            foreach ($type as $typ){
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= $typ->type_name ?></td>
                                    <td><?= $typ->carry_over ?></td>
                                    <td><?= $typ->description ?></td>
                                    <td>
                                        <a title="Edit Leave Type" class="button green" href="<?= site_url('leave/edit_type/' . $typ->type_id) ?>">
                                            <div class="icon"><span class="ico-pencil"></span></div>
                                        </a>
                                        <a title="Delete Leave Type" href="<?= site_url('leave/delete_type/' . $typ->type_id) ?>" class="button red delete_">
                                            <div class="icon"><span class="ico-remove"></span></div>
                                        </a>    
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php }else{ ?>
                        <div class="text-center">
                            <h5>No data here, please click on "Add Leave Type"</h5>
                        </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<?php include '_new_leave_type.php'; ?>

<script>
    $(function () {
        $('.delete_').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this leave type ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>