<?php include '_leave_tab.php'; ?>
<br>
<div class="row">
	<div class="col-md-12">
	    <div class="block">
	        <div class="head blue">                                
	            <h2>Leave Period</h2>                                
	        </div>
	        <div class="data dark">
	        	<form method="post" action="addLeavePeriod">
	                <div class="row">
                       <!--  <div class="col-md-4">
                            <input type="text" name="ent_employee" id="ent_employee" class="form-control" placeholder="Employee">
                        </div> -->
                        <div class="col-md-3">
                        	<label for="start_month">Start Month <em>*</em></label>
                            <select data-month="<?= date_format(new DateTime($period->end_date), 'm') ?>" disabled="disabled" id="start_month" name="ent_type" class="start_period form-control" >
                                <option>--Select Month--</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                        	<label for="start_day">Start Day <em>*</em></label>
                            <select disabled="disabled" id="start_day" name="ent_period" class="start_period form-control" >
                                <option>--Select Day--</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                        	<label for="end_date">End Date</label>
                        	<br>
                        	<h5>
	                        	<span id="end_date">
	                        	<?= empty($period) ? '--------' : date_format(new DateTime($period->end_date), 'M d') ?>
	                        	</span>
	                        	&nbsp;
	                        	<span>(Following Year)</span>
                        	</h5>
                        </div>
                        <div class="col-md-3">
                        	<label for="curr_period">Current Leave Period</label>
                        	<br>
                        	<h5>
	                        	<span id="curr_period"><?= empty($period) ? '--------' : date_format(new DateTime($period->start_date), 'Y/m/d') 
	                        	. ' to ' . date_format(new DateTime($period->end_date), 'Y/m/d') ?></span>
                        	</h5>
                        </div>
	                </div>
                    <br>
                    <div class="row">
                    	<div class="col-md-4">
                    		<span id="subb">
                        		
                        	</span>
                        	<span id="sub">
                        		<Button id="subButton" type="button" class="btn btn-primary btn-flat">Edit</Button>
                        	</span>
                    	</div>
                    </div>  
                </form> 
	        </div>
	    </div>                          
	</div>
<script>
    var strtday = $('#start_month').data('month');
    var strtmonth = strtday - 1;
    
    $('#start_month').options[strtmonth].selected = true;
    //alert(strtmonth);
</script>
</div>