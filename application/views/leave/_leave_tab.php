<?= show_notification();
?>
<section class="content-header">
    <!-- <div class="wrap-title">
        <div class="icon">
            <span class="ico-layout-7"></span>
        </div> -->
        <h1>Leave Configuration<!-- <small>Department, Locations, Roles &AMP; Levels</small> --></h1>
    <!-- </div> -->
    <ol class="breadcrumb">
        <li>
            <a href="/">Home</a>
        </li>
        <li>Leave</li>
        <li class="active">Configuration</li>
    </ol>
    <div class="clear"></div>
</section>

<?php $path = $this->input->server('REQUEST_URI'); ?>
<div class="row">
    <div class="col-md-12" style="margin-left: 15px; margin-top: 15px">
        <ul class="nav nav-tabs" id="myTab">
            <li class="<?= strstr($path, 'leave/config') ? 'active' : '' ?>">
                <a href="<?= site_url('/leave/config') ?>" >Leave Period</a>
            </li>
            <li class="<?= strstr($path, 'leave/leave_type') ? 'active' : '' ?>">
                <a href="<?= site_url('/leave/leave_type') ?>" >Leave Types</a>
            </li>
            <li class="<?= strstr($path, 'leave/leave_days') ? 'active' : '' ?>">
                <a href="<?= site_url('/leave/leave_days') ?>" >Leave Days</a>
            </li>
            <li class="<?= strstr($path, 'leave/work_week') ? 'active' : '' ?>">
                <a href="<?= site_url('/leave/work_week') ?>" >Work Week</a>
            </li>
            <li class="<?= strstr($path, 'leave/holidays') ? 'active' : '' ?>">
                <a href="<?= site_url('/leave/holidays') ?>" >Public Holidays</a>
            </li>
        </ul>
    </div>
</div>