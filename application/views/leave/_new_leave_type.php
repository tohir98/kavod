<div class="modal" id="new_ltype">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Leave Type</h4>
            </div>
            <form role="form" method="post" class="form-horizontal" action="<?= site_url('/leave/leave_type') ?>">
                <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                    <div class="form-group">
                        <label for="type_name">Name <em style="color: red">*</em></label>
                        <input required type="text" class="form-control" id="type_name" name="type_name" placeholder="Leave Type Name">
                    </div>
                </div>
                <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                    <div class="form-group">
                        <label for="type_name">Carry over allowed <em style="color: red">*</em></label>
                        <input required type="text" class="form-control" id="type_name" name="type_name" placeholder="0" value="0">
                    </div>
                </div>
                <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                    <div class="form-group">
                        <label for="type_name">Description</label>
                        <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>