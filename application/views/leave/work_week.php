<?php include '_leave_tab.php'; ?>
<br>
<div class="row">
	<div class="col-md-12">
	    <div class="block">
	        <div class="head blue">                                
	            <h2>Work Week</h2>                                
	        </div>
	        <div class="data dark">
	        	<form method="post" action="work_week">
	                <div class="row">
                        <div class="col-md-3">
                        	<h5>Monday</h5>
                        </div>
                        <div class="col-md-3">
                            <select disabled="disabled" data-var="<?= $weeks->work_mon ?>" id="work_mon" name="work_mon" class="start_period form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                                <option value="nwday">Non-working Day</option>
                            </select>
                        </div>
	                </div>
	                <div class="row">
                        <div class="col-md-3">
                        	<h5>Tuesday</h5>
                        </div>
                        <div class="col-md-3">
                            <select disabled="disabled" data-var="<?= $weeks->work_tue ?>" id="work_tue" name="work_tue" class="start_period form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                                <option value="nwday">Non-working Day</option>
                            </select>
                        </div>
	                </div>
	                <div class="row">
                        <div class="col-md-3">
                        	<h5>Wednesday</h5>
                        </div>
                        <div class="col-md-3">
                            <select disabled="disabled" data-var="<?= $weeks->work_wed ?>" id="work_wed" name="work_wed" class="start_period form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                                <option value="nwday">Non-working Day</option>
                            </select>
                        </div>
	                </div>
	                <div class="row">
                        <div class="col-md-3">
                        	<h5>Thursday</h5>
                        </div>
                        <div class="col-md-3">
                            <select disabled="disabled" data-var="<?= $weeks->work_thu ?>" id="work_thu" name="work_thu" class="start_period form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                                <option value="nwday">Non-working Day</option>
                            </select>
                        </div>
	                </div>
	                <div class="row">
                        <div class="col-md-3">
                        	<h5>Friday</h5>
                        </div>
                        <div class="col-md-3">
                            <select disabled="disabled" data-var="<?= $weeks->work_fri ?>" id="work_fri" name="work_fri" class="start_period form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                                <option value="nwday">Non-working Day</option>
                            </select>
                        </div>
	                </div>
	                <div class="row">
                        <div class="col-md-3">
                        	<h5>Saturday</h5>
                        </div>
                        <div class="col-md-3">
                            <select disabled="disabled" data-var="<?= $weeks->work_sat ?>" id="work_sat" name="work_sat" class="start_period form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                                <option value="nwday" selected>Non-working Day</option>
                            </select>
                        </div>
	                </div>
	                <div class="row">
                        <div class="col-md-3">
                        	<h5>Sunday</h5>
                        </div>
                        <div class="col-md-3">
                            <select disabled="disabled" data-var="<?= $weeks->work_sun ?>" id="work_sun" name="work_sun" class="start_period form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                                <option value="nwday" selected>Non-working Day</option>
                            </select>
                        </div>
	                </div>
                    <br>
                    <div class="row">
                    	<div class="col-md-4">
                    		<span id="subb">
                        		
                        	</span>
                        	<span id="sub">
                        		<Button id="subButton" type="button" class="btn btn-primary btn-flat">Edit</Button>
                        	</span>
                    	</div>
                    </div>  
                </form> 
	        </div>
	    </div>                          
	</div>
</div>