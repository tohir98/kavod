<?php include '_leave_tab.php'; ?>

<!-- Main content -->

<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="head">
                <h3>Holidays
                    <a href="#new_hols" data-toggle="modal" class="btn btn-success pull-right">Add Holiday</a>
                </h3>

            </div><!-- /.box-header -->
            <div class="data-fluid">
            	<?php
                if (!empty($holidays)){
                    ?>
                    <table class="table table-hover dtable lcnp">
                        <thead>
                            <tr>
                            	<th>SN</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Full day/Half day</th>
                                <th>Repeats Annually</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                            $sn = 0;
                            foreach ($holidays as $hol){
                                ?>
                                <tr>
                                	<td><?= ++$sn; ?></td>
                                	<td><?= $hol->hol_name ?></td>
                                    <td><?= date_format(new DateTime($hol->hol_date), 'd-M-Y') ?></td>
                                    <td><?= $hol->day_length == 'fullday' ? 'Full Day' : 'Half Day' ?></td>
                                    <td><?= $hol->repeat_annual ?></td>
                                    <td>
                                    	<a title="Delete Holiday" href="<?= site_url('leave/delete_holiday/' . $hol->id) ?>" class="button red delete_">
                                            <div class="icon"><span class="ico-remove"></span></div>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php }else{ ?>
            			<div class="text-center">
            				<h5>No data here, please click on "Add Holiday"</h5>
            			</div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php include '_new_holiday.php'; ?>

<script>
    $(function () {
        $('.delete_').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this holiday ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>