<div class="modal" id="edit_ent">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Edit Allocation</h4>
            </div>
            <form role="form" id="edit_form" method="post" class="form-horizontal" action="">
                <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                    <div class="form-group">
                        <label for="leave_type_name">Leave Type: </label>
                        <h6 id="leave_type_name">Maternity</h6>
                    </div>
                    <div class="form-group">
                        <label for="days">No. of days: </label>
                        <input required type="number" class="form-control" id="days" name="days">
                        <input required type="hidden" class="form-control" id="leave_type" name="leave_type">
                    </div>
                    <div class="form-group">
                        <label for="recurring">Recurring: </label>
                        <select id="recurring" name="recurring" class="form-control" >
                                <option value="No">No</option>
                                <option value="Yes">Yes</option>
                        </select>
                        <!-- <input type="checkbox" class="form-control" id="repeat_annual" name="repeat_annual"> -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Edit</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>