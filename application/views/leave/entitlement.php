<?php include '_ent_tab.php'; ?>

<!-- Main content -->

<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="head">
                <h3>Entitlements
                    <a href="#add_ent" data-toggle="modal" class="btn btn-success pull-right">Add Entitlement</a>
                </h3>

            </div><!-- /.box-header -->
            <div class="data-fluid">
            	<?php
                if (!empty($entitlements)){
                    ?>
                    <table id="DataTables_Table_2" class="table fpTable lcnp dataTable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Days Allocated</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                            // $sn = 0;
                            foreach ($entitlements as $val){
                                ?>
                                <tr>
                                	<td><?= $val->name ?></td>
                                    <td>Active</td>
                                    <td><?= $val->days_sum ?></td>
                                    <td>
                                    	<div class="btn-group">
                                            <button class="btn btn-primary">Action</button>
                                            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('/leave/entitlement_details/' . $val->employee_id); ?>">View Details</a></li>
                                                <li><a href="#">Make Vacation Requests</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php }else{ ?>
            			<div class="text-center">
            				<h5>No data here, please click on "Add Entitlement"</h5>
            			</div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

<?php include '_new_entitlemeent.php'; ?>

<script>
    $(function () {
        $('.delete_').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this holiday ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>