<div class="modal" id="new_hols">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Holiday</h4>
            </div>
            <form role="form" method="post" class="form-horizontal" action="<?= site_url('/leave/holidays') ?>">
                <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                    <div class="form-group">
                        <label for="hol_name">Name <em>*</em></label>
                        <input required type="text" class="form-control" id="hol_name" name="hol_name" placeholder="Holiday Name">
                    </div>
                    <div class="form-group">
                        <label for="hol_date">Date <em>*</em></label>
                        <input required type="text" class="form-control" id="hol_date" name="hol_date" placeholder="dd-mm-yyyy">
                    </div>
                    <div class="form-group">
                        <label for="repeat_annual">Repeats Annually </label>
                        <select id="repeat_annual" name="repeat_annual" class="form-control" >
                                <option value="No">No</option>
                                <option value="Yes">Yes</option>
                        </select>
                        <!-- <input type="checkbox" class="form-control" id="repeat_annual" name="repeat_annual"> -->
                    </div>
                    <div class="form-group">
                        <label for="day_length">Full day/Half day</label>
                        <select id="day_length" name="day_length" class="form-control" >
                                <option value="fullday">Full Day</option>
                                <option value="halfday">Half Day</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>