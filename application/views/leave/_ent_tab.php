<?= show_notification();
?>
<section class="content-header">
    <!-- <div class="wrap-title">
        <div class="icon">
            <span class="ico-layout-7"></span>
        </div> -->
        <h1>Employee Entitlements <!-- <small>Department, Locations, Roles &AMP; Levels</small> --></h1>
    <!-- </div> -->
    <ol class="breadcrumb">
        <li>
            <a href="/">Home</a>
        </li>
        <li>Leave</li>
        <li class="active">Entitlement</li>
    </ol>
    <div class="clear"></div>
</section>

<?php $path = $this->input->server('REQUEST_URI'); ?>
<div class="row">
    <div class="col-md-12" style="margin-left: 15px; margin-top: 15px">
        <ul class="nav nav-tabs" id="myTab">
            <li class="<?= strstr($path, 'leave/entitlement') ? 'active' : '' ?>">
                <a href="<?= site_url('/leave/entitlement') ?>" >Leave Days</a>
            </li>
            <li class="<?= strstr($path, 'leave/leave_allowance') ? 'active' : '' ?>">
                <a href="<?= site_url('/leave/leave_allowance') ?>" >Leave Allowance</a>
            </li>
        </ul>
    </div>
</div>