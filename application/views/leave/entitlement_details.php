<?php include '_ent_tab.php'; ?>

<!-- Main content -->

<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="head">
                <h3>
                    <a class="btn btn-warning btn-flat btn-sm" href="<?php echo site_url('/leave/entitlement'); ?>">
                        <i class="fa fa-arrow-left"></i> Back
                    </a>
                    Allocations for <?= $username ?>
                    <!-- <a href="#add_ent" data-toggle="modal" class="btn btn-success pull-right">Add Entitlement</a> -->
                </h3>

            </div><!-- /.box-header -->
            <div class="data-fluid">
            	<?php
                if (!empty($entitlements)){
                    ?>
                    <table class="table table-hover dtable lcnp">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Leave Type</th>
                                <th>Recurring</th>
                                <th>Days Allocated</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                            $sn = 0;
                            foreach ($entitlements as $val){
                                $sn++
                                ?>
                                <tr>
                                    <td><?= $sn ?></td>
                                	<td><?= $val->name ?></td>
                                    <td><?= $val->recurring ?></td>
                                    <td><?= $val->days ?></td>
                                    <td>
                                    	<div class="btn-group">
                                            <button class="btn btn-primary">Action</button>
                                            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a data-leavetype="<?= $val->leave_type ?>" data-form="<?= site_url('leave/edit_entitlement/' . $val->id . '/' . $employee_id) ?>" data-name="<?= $val->name ?>" data-recurring="<?= $val->recurring ?>" data-days="<?= $val->days ?>" class="edit_" href="#edit_ent" data-toggle="modal">Edit</a></li>
                                                <li><a href="<?= site_url('leave/delete_entitlement/' . $val->id . '/' . $employee_id) ?>" class="delete_">
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php }else{ ?>
            			<div class="text-center">
            				<h5>No data here, please click on "Add Entitlement"</h5>
            			</div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

<?php include '_edit_entitlement.php'; ?>

<script>
    $(function () {
        $('.delete_').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this allocation ?';
            Kavod.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
    $(function () {
        $('.edit_').click(function (e) {
            // e.preventDefault();
            var days = $(this).data('days');
            var recurring = $(this).data('recurring');
            var name = $(this).data('name');
            var form = $(this).data('form');
            var leave_type = $(this).data('leavetype');

            $('#edit_form').attr('action', form);
            $('#leave_type_name').html(name);
            $('#leave_type').val(leave_type);
            $('#days').val(days);
            $("#recurring option[value="+recurring+"]").prop("selected", true);
            
        });
    });
</script>