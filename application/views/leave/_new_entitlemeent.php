<div class="modal" id="add_ent">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Entitlement</h4>
            </div>
            <form role="form" method="post" class="form-horizontal" action="<?= site_url('/leave/entitlement') ?>">
                <div class="modal-body" style="padding-left: 30px; padding-right: 30px">
                    <div class="form-group">
                        <label for="employee_id">Employee <em>*</em></label>
                        <select required id="employee_id" name="employee_id" class="form-control" >
                                <option value="">Select One...</option>
                                <?php foreach ($employees as $val) {
                                ?>
                                <option value="<?= $val->userid ?>"><?= $val->name ?></option>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="days">No. of days <em>*</em></label>
                        <input required type="number" class="form-control" id="days" name="days">
                    </div>
                    <div class="form-group">
                        <label for="hol_date">Leave type <em>*</em></label>
                        <select id="repeat_annual" name="leave_type" class="form-control" >
                                <option value="">Select One...</option>
                                <?php foreach ($leave_types as $val) {
                                ?>
                                <option value="<?= $val->type_id ?>"><?= $val->type_name ?></option>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recurring">Recurring </label>
                        <select id="recurring" name="recurring" class="form-control" >
                                <option value="No">No</option>
                                <option value="Yes">Yes</option>
                        </select>
                        <!-- <input type="checkbox" class="form-control" id="repeat_annual" name="repeat_annual"> -->
                    </div>
                    <div class="form-group">
                        <label for="valid_from">Valid From</label>
                        <input required type="text" class="form-control" id="valid_from" name="valid_from" value="01-01-2016">
                    </div>
                    <div class="form-group">
                        <label for="valid_to">Valid To</label>
                        <input required type="text" class="form-control" id="valid_to" name="valid_to" value="31-12-2016">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>