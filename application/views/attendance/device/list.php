<?= show_notification();
?>

<div class="page-header">
    <div class="pull-left">
        <h1>Device Manager <small>--</small></h1>
    </div>
    <div class="clear"></div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="/">Dashboard</a>
        </li>
        <li>Attendance</li>
        <li class="active">Devices</li>
    </ul>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>Devices</h3>
            </div>
            <div class="box-content-padless">
                <?php if (!empty($devices)): ?>
                    <table class="table table-condensed table-hover table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Device Name</th>
                                <th>User Count</th>
                                <th>FP Count</th>
                                <th>Last Activity</th>
                                <th>Status</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($devices as $device): ?>
                                <tr>
                                    <td><?= $device->SN; ?></td>
                                    <td><?= $device->Alias; ?></td>
                                    <td><?= $device->UserCount; ?></td>
                                    <td><?= $device->FPCount; ?></td>
                                    <td><?= $device->LastActivity; ?></td>
                                    <td><label class="label label-<?= $device->State == 1 ? 'brown' : 'success'; ?>">
                                            <?= $statuses[$device->State]; ?>
                                        </label>
                                    </td>
                                    <td><a href="#">edit</a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php
                else:
                    echo show_no_data('Devices not available');
                endif;
                ?>
            </div>
        </div>
    </div>
</div>