<?php include '_settings_tab.php'; ?>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>Shift Management</h3>
                <a href="#NewShift" data-toggle="modal" class="btn btn-primary pull-right" style="margin-right: 5px;">Create New Shift</a>
            </div><!-- /.box-header -->
            <div class="box-content-padless">
                <?php
                if (!empty($shifts)):
                    ?>
                    <table class="table table-hover dataTable">
                        <thead>
                            <tr>
                                <th>SHIFT</th>
                                <th>CLOCK-IN TIME</th>
                                <th>CLOCK-OUT TIME</th>
                                <th>LATENESS</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($shifts as $shift): ?>
                            <tr>
                                <td> <?= $shift->shift_name; ?></td>
                                <td> <?= $shift->clock_in; ?> </td>
                                <td> <?= $shift->clock_out; ?> </td>
                                <td> <?= $shift->lateness; ?> Mins </td>
                                <td>
                                    <a href="<?= site_url('employee/edit_shift/' . $shift->attendance_shift_id) ?>" class="edit" title="Click here to edit">
                                        <i class="icons icon-edit"></i> Edit
                                    </a> | 
                                    <a href="<?= site_url('attendance/delete_shift/' . $shift->attendance_shift_id) ?>" class="delete_shift" title="Click here to delete">
                                        <i class="icons icon-trash"></i> Delete</a>
                                    
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php
                else:
                    echo show_no_data("No shift has been created, <a href=#NewShift data-toggle=modal>Click here to add one</a>");
                endif;
                ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<?php include '_shift_modal.php'; ?>


<script type = "text/javascript" >
    $(function () {
        $('#datetimepicker3').datetimepicker({
            pickDate: false
        });
    });
    
    $(function () {
    $('body').delegate('.delete_shift', 'click', function (e) {
        e.preventDefault();
        var h = this.href;
        var message = 'Are you sure you want to continue?';
        CTM.doConfirm({
            title: 'Confirm',
            message: message,
            onAccept: function () {
                window.location = h;
            }
        });
    });
});

</script>