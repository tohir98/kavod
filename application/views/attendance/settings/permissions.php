<script> var ACTION_Url = "<?= site_url('attendance/settings/update_permission'); ?>";</script>
<div class="row-fluid" ng-app="permission">


    <?php include '_settings_tab.php';
    ?>



    <?php include '_shift_permissions.php'; ?>

</div>


<div id="modal_shift_permission" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<div id="analytics_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Who can view Analytics & Reports</h4>
    </div>
    <form method="post" class="form-horizontal" action="<?= site_url('attendance/settings/set_report_viewers') ?>">
        <div class="modal-body">
            <div class="control-group">
                <label for="employees" class="control-label">Employees</label>
                <div class="controls">
                    <select name="employees[]" id="employees" multiple="multiple" class="chosen-select">
                        <?php
                        $employee_ = array();
                        if (!empty($report_viewers)) :
                            foreach ($report_viewers as $cp) :
                                ?>
                                <option value="<?php echo $cp->id_user; ?>" selected><?php echo ucwords($cp->first_name . ' ' . $cp->last_name); ?></option>
                                <?php
                                $employee_[] = $cp->id_user;
                            endforeach;
                        endif;
                        ?>

                        <?php
                        foreach ($emps as $em) {
                            if (!in_array($em->id_user, $employee_)) {
                                ?>
                                <option value="<?php echo $em->id_user; ?>"><?php echo ucwords($em->first_name . ' ' . $em->last_name); ?></option>
                                <?php
                            }
                        }
                        ?>

                    </select>
                </div>
            </div>

        </div>

        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button type="submit" class="btn btn-primary pull-right">Update</button>
        </div>

    </form>
</div>

<div id="shif_access_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Can Access Manage Shift</h4>
    </div>
    <form method="post" class="form-horizontal" action="<?= site_url('attendance/access_manage_shift') ?>">
        <div class="modal-body">
            <div class="control-group" style="background: none; border: none;" >
                <label class="control-label">Employees</label>
                <div class="controls" style="border: none;">
                    <select name="employees[]" id="employees_cbo" multiple class="chosen-select input-xlarge">
                        <optgroup label="By Role">
                            <option value="<?= ADMIN ?>">Administrator</option>
                            <option value="<?= DEPT_HEAD ?>">Department Head</option>
                            <option value="<?= LOC_HEAD ?>">Location Head</option>
                        </optgroup>
                    </select>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Close</button>
            <button type="submit" class="btn btn-primary pull-right">Save</button>
        </div>

    </form>
</div>


<script src="/js/attendance_settings.js"></script>