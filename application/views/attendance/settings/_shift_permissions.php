<?php
$approverList = [];
if (!empty($shift_approvers)) {
    foreach ($shift_approvers as $approver) {
        array_push($approverList, (int) $approver->id_user > 0 ? $approver->id_user : $approver->role);
    }
}
?>

<script>
    window.shiftApprovers = <?= json_encode($approverList); ?>;
    window.employees = <?= json_encode($emps); ?>;
    window.accessLevels = <?= json_encode($accessLevel); ?>;
    window.shiftApprovalUrl = '<?= site_url("/attendance/settings/add_shift_approval"); ?>';
</script>

<div class="row-fluid" ng-controller="shiftCtrl">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title" style="padding:10px !important;">
                <h3> Shift Scheduling</h3>
            </div>

            <div class="box-content-padless">
                <table class="table table-user table-hover table-nomargin">
                    <tbody>
                        <tr>
                            <td>Who Can Access Manage Shifts?</td>
                            <td>
                                <?php
                                if (!empty($manage_shift_access)):
                                    foreach ($manage_shift_access as $access) :
                                        ?><span class='label'><?= ucfirst($access->role) ?></span><?php
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <a href="#" onclick="return false;" class="edit_shift_access">Edit</a>
                            </td>
                        </tr>
                        <tr>
                            <td> Can Managers Schedule Shifts ?</td>
                            <td>
                                &nbsp;
                            </td>
                            <td style=" width: 35%;">
                                <div class="controls">
                                    <input type="checkbox" id="manager_schedule_shift" class="input-block-level bswitch small sToggle" data-on-text="Yes" data-off-text="No" data-id="manager_schedule_shift" data-val="<?= $permission->manager_schedule_shift; ?>" <?= (int) $permission->manager_schedule_shift == 1 ? 'checked' : ''; ?> >
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
