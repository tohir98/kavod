<div id="NewShift" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create New Shift</h4>
    </div>
    <form method="post" class="form-horizontal" action="<?= site_url('/attendance/create_shift') ?>">
        <div class="modal-body">

            <div class="control-group">
                <label class="control-label">Shift Name:</label>
                <div class="controls">
                    <input class="form-control" type="text" id="shift_name" name="shift_name" placeholder="Shift Name">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Start Time:</label>
                <div class="controls">
                    <div class="bootstrap-timepicker">
                        <input type="text" name="clock_in" id="clock_in" class="input-small timepick" style="cursor: pointer">
                        <span class="help-block">Clock-In time</span>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">End Time:</label>
                <div class="controls">
                    <div class="bootstrap-timepicker">
                        <input type="text" name="clock_out" id="clock_out" class="input-small timepick" style="cursor: pointer">
                        <span class="help-block">Clock-Out time</span>
                    </div>
                </div>
            </div>
<!--            <div class="control-group">
                <label class="control-label">Overtime Allowed:</label>
                <div class="controls">
                    <input required class="form-control" type="number" id="overtime" name="overtime" min="0" max="6"></div>
            </div>-->
            <div class="control-group">
                <label class="control-label">Lateness After:</label>
                <div class="controls">
                    <input required class="input-small" type="number" id="lateness" name="lateness" min="0" max="30" placeholder="20"></div>
            </div>

        </div>
        <div class="modal-footer">
            <a class="btn btn-default" data-dismiss='modal'>Cancel</a>
            <button type="submit" class="btn btn-primary" >Save</button>
        </div>
    </form>

</div>