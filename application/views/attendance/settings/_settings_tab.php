
<div class="page-header">
    <div class="pull-left">
        <h1>Settings <small>Shift Mgt, Permissions, Reminders</small></h1>
    </div>
    <div class="clear"></div>
</div>

<?= show_notification(); ?>

<?php $path = $this->input->server('REQUEST_URI'); ?>
<div class="row">
    <div class="col-md-12" style="margin-left: 15px; margin-top: 15px">
        <ul class="nav nav-tabs" id="myTab">
            <li class="<?= strstr($path, 'attendance/permissions') ? 'active' : '' ?>">
                <a href="<?= site_url('/attendance/permissions') ?>" >Permissions</a>
            </li>
            <li class="<?= strstr($path, 'attendance/settings') ? 'active' : '' ?>">
                <a href="<?= site_url('/attendance/settings') ?>" >Shift Management</a>
            </li>
            <li class="<?= strstr($path, 'employee/role_setting') ? 'active' : '' ?>">
                <a href="<?= site_url('/attendance/reminder') ?>" >Reminders</a>
            </li>
        </ul>
    </div>
</div>