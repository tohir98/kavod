<style>
    .form-control{
        display: inline;
        width: 220px
    }
    #date{
        cursor: pointer;
    }
</style>

<script> var currentDate = '<?= date('d-m-Y'); ?>';</script>
<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables-all.js" type="text/javascript"></script>
<script src="/js/record.js" type="text/javascript"></script>

<?= show_notification();
?>

<?php include '_report_tab.php'; ?>

<!-- Main content -->
<div ng-app="record" ng-controller="recordCtrl">
    <div class="row">
        <div class="span12">
            <table class="table">
                <tr>
                    <td> &nbsp;
                        <select id="department_id" name="department_id" class="form-control" ng-model="data.department_id">
                            <option value="">All Departments</option>
                            <?php
                            if (!empty($departments)):
                                foreach ($departments as $dept):
                                    ?>
                                    <option value="<?= $dept->department_id ?>"><?= trim($dept->department); ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select> &nbsp;
                        <select id="location_id" name="location_id" class="form-control" ng-model="data.location_id">
                            <option value="">All Locations</option>
                            <?php
                            if (!empty($locations)):
                                foreach ($locations as $loc):
                                    ?>
                                    <option value="<?= $loc->location_id ?>"><?= trim($loc->location_tag); ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                        &nbsp;
                        <input type="text" name="date" id="date" class="form-control" ng-model="data.date" />
                        <button type="button" class="btn btn-primary btn-flat" ng-click="filter()">Generate</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>


    <div class="row-fluid" cg-busy="myPromise">
        <div class="span12">
            <div class="box box-bordered">

                <div class="box-content-padless">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Employee</th>
                                <th>Shift</th>
                                <th>Clock In</th>
                                <th>Clock Out</th>
                                <th>Total Hr.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="clock in clocks">
                                <td>{{clock.name}}</td>
                                <td>Shift</td>
                                <td>{{clock.checkin}}</td>
                                <td>{{clock.checkout}}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('document').ready(function () {
        $('#date').datepicker("option", "dateFormat", "dd-mm-yy");
    });
</script>