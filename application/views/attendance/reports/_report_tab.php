<?= show_notification();
?>
<div class="page-header">
    <div class="pull-left">
        <h1>Attendance Report <small>__</small></h1>
    </div>
    <div class="clear"></div>
</div>


<?php $path = $this->input->server('REQUEST_URI'); ?>
<div class="row">
    <div class="col-md-12" style="margin-left: 15px; margin-top: 15px">
        <ul class="nav nav-tabs" id="myTab">
            <li class="<?= strstr($path, 'report/punctuality') || strstr($path, 'attendance/attendance_record') ? 'active' : '' ?>">
                <a href="<?= site_url('/report/punctuality') ?>" >Punctuality report</a>
            </li>
            <li class="<?= strstr($path, 'report/lateness') ? 'active' : '' ?>">
                <a href="<?= site_url('/report/lateness') ?>" >Lateness report</a>
            </li>
            <li class="<?= strstr($path, 'report/absenteeism') ? 'active' : '' ?>">
                <a href="<?= site_url('/report/absenteeism') ?>" >Absenteeism report</a>
            </li>
            <li class="<?= strstr($path, 'report/overtime') ? 'active' : '' ?>">
                <a href="<?= site_url('/report/overtime') ?>" >Overtime report</a>
            </li>
            <li class="<?= strstr($path, 'report/leave') ? 'active' : '' ?>">
                <a href="<?= site_url('/report/leave') ?>" >Leave report</a>
            </li>
            <li class="<?= strstr($path, 'report/retirement') ? 'active' : '' ?>">
                <a href="<?= site_url('/report/retirement') ?>" >Retirement report</a>
            </li>
            <li class="<?= strstr($path, 'report/birthday') ? 'active' : '' ?>">
                <a href="<?= site_url('/report/birthday') ?>" >Birthday report</a>
            </li>
        </ul>
    </div>
</div>