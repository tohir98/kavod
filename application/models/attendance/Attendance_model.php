<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Attendance_model
 *
 * @author tohir
 * @property CI_DB_query_builder $db Description
 */
class Attendance_model extends CI_Model {

    const CHECK_IN = 0;
    const CHECK_OUT = 1;
    const TBL_SHIFT = 'attendance_shifts';
    const TBL_PERM = 'attendance_permissions';
    const TBL_SHIFT_ACCESS = 'attendance_shift_access';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetchAttnReport($department_id, $location_id, $date) {
        // $sql = "SELECT u.userid, u.name, (SELECT c.checktime FROM checkinout c WHERE u.userid = c.userid AND c.checktype = 0  LIMIT 1 ) as checkin,  (SELECT c.checktime FROM checkinout c WHERE u.userid = c.userid AND c.checktype = 1 LIMIT 1 ) as checkout FROM userinfo u";
        //return $this->db->query($sql)->result();
        if ($department_id) {
            $this->db->where('d.department_id', $department_id);
        }
        if ($location_id) {
            $this->db->where('l.location_id', $location_id);
        }
        return $this->db
                        ->select("u.userid, u.name, (SELECT c.checktime FROM checkinout c WHERE u.userid = c.userid AND c.checktype = 0 AND SUBSTRING(checktime,1,10) ={$date}   LIMIT 1 ) as checkin,  (SELECT c.checktime FROM checkinout c WHERE u.userid = c.userid AND c.checktype = 1 AND SUBSTRING(checktime,1,10) ={$date} LIMIT 1 ) as checkout")
                        ->from('userinfo u')
                        ->join(TBL_EMP . ' as e', 'u.userid=e.userid')
                        ->join(TBL_DEPT . ' as d', 'e.department_id=d.department_id', 'left')
                        ->join(TBL_LOC . ' as l', 'e.location_id=l.location_id', 'left')
                        ->get()->result();
    }

    public function createShift($user_id, $company_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        return $this->db->insert(self::TBL_SHIFT, [
                    'shift_name' => $data['shift_name'],
                    'clock_in' => $data['clock_in'],
                    'clock_out' => $data['clock_out'],
                    'lateness' => $data['lateness'],
                    'date_created' => date('Y-m-d h:i:s'),
                    'created_by' => $user_id,
                    'company_id' => $company_id,
        ]);
    }

    public function getShifts($params) {
        return $this->db->get_where(self::TBL_SHIFT, $params)->result();
    }

    public function manage_shift_accessor($company_id) {
        return $this->db->select('*')
                        ->from('attendance_shift_access asa')
                        ->where('asa.company_id', $company_id)
                        ->get()->result();
    }
    
    public function add($table, $data) {
        return $this->db->insert($table, $data);
    }
    
    

}
