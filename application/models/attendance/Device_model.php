<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Device_model
 *
 * @author tohir
 */
class Device_model extends CI_Model {
    const TBL_DEVICES = 'iclock';
    
    static $statuses = [1=> 'Offline', 2 => 'Online'];

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public static function device_statuses(){
        return static::$statuses;
    }
}
