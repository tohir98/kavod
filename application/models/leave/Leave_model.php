<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Leave_model
 *
 * @author tohir
 */
class Leave_model extends CI_Model {
    
    const TBL_LEAVE_TYPES = 'leave_type';
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function add_leave_type($company_id, $data)
    {
        if (empty($data)){
            return FALSE;
        }
        
        $data['date_created'] = date('Y-m-d');
        $data['creator_id'] = $this->user_auth_lib->get('user_id');
        $data['company_id'] = $company_id;
        
        return $this->db->insert(self::TBL_LEAVE_TYPES, $data);
    }
    
}
