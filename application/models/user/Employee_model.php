<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Employee_model
 *
 * @author tohir
 */
class Employee_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetchRecord($userid, $company_id) {
        return $this->db->get_where(TBL_USERS, ['userid' => $userid, 'company_id' => $company_id])->row();
    }

    public function fetchCompanyRecord($company_id) {
        return $this->db->get_where(TBL_USERS, ['company_id' => $company_id])->result();
    }

    public function updatePersonalInfo($userid, $company_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $data['Birthday'] = date('Y-m-d', strtotime($data['Birthday']));
        $this->db->update(TBL_USERS, $data, ['userid' => $userid, 'company_id' => $company_id]);

        return $this->db->affected_rows() > 0;
    }

    public function updateWorkInfo($userid, $company_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $data['employment_date'] = date('Y-m-d', strtotime($data['employment_date']));
        $data['userid'] = $userid;
        $data['company_id'] = $company_id;

        $response = FALSE;
        if ($this->fetchWorkInfo($userid, $company_id)) {
            $data['date_updated'] = date('Y-m-d h:i:s');
            $this->db->update(TBL_EMP, $data, ['userid' => $userid, 'company_id' => $company_id]);
            $response = $this->db->affected_rows() > 0;
        } else {
            $data['date_created'] = date('Y-m-d h:i:s');
            $response = $this->db->insert(TBL_EMP, $data);
        }

        return $response;
    }

    public function fetchPersonalInfo($company_id, $userid = null) {
        if ($userid) {
            $this->db->where('u.userid', $userid);
        }
        return $this->db
                        ->select('u.userid, u.badgenumber, u.first_name, u.last_name, u.email, u.address, u.City, u.Birthday, u.pager as phone, g.gender, s.state, c.country')
                        ->from(TBL_USERS . ' as u')
                        ->join(TBL_GENDER . ' as g', 'g.gender_id=u.gender_id', 'left')
                        ->join(TBL_STATES . ' as s', 's.state_id=u.state_id', 'left')
                        ->join(TBL_COUNTRY . ' as c', 'c.country_id=u.country_id', 'left')
                        ->where('u.company_id', $company_id)
                        ->get()->result();
    }

    public function fetchWorkInfo($company_id, $userid) {
        return $this->db
                        ->select('e.*, d.department, l.location_tag, r.role, le.level')
                        ->from(TBL_EMP . ' as e')
                        ->join(TBL_DEPT . ' as d', 'd.department_id=e.department_id', 'left')
                        ->join(TBL_LOC . ' as l', 'l.location_id=e.location_id', 'left')
                        ->join(TBL_ROLES . ' as r', 'r.role_id=e.role_id', 'left')
                        ->join(TBL_LEVELS . ' as le', 'le.level_id=e.level_id', 'left')
                        ->where('e.company_id', $company_id)
                        ->where('e.userid', $userid)
                        ->get()->result();
    }

    public function fetchStaffPicture($userid, $company_id) {
        return $this->db->get_where(TBL_EMP_PICTURE, ['userid' => $userid, 'company_id' => $company_id])->result();
    }

    public function getStaffProfilePic($userid, $company_id) {
        return $this->db->get_where(TBL_EMP_PICTURE, ['userid' => $userid, 'company_id' => $company_id])->row();
    }

    public function update_employee_picture($userid, $company_id, $file) {

        if (!empty($file) && $file['name'] !== '') {

            $ext = end((explode(".", $file['name'])));

            $config = array(
                'upload_path' => FILE_PATH_PROFILE_PIC,
                'allowed_types' => "jpg|jpeg|gif|png",
                'overwrite' => TRUE,
                'max_size' => "102400", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "3000",
                'max_width' => "3000",
            );

            $picture_name = md5(microtime()) . '.' . $ext;
            $config['file_name'] = $picture_name;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {

                $error = array('error' => $this->upload->display_errors());

                notify('error', 'Invalid file uploaded');
                return FALSE;
            }

            $data_img['profile_picture'] = $picture_name;
            $data_img['userid'] = $userid;
            $data_img['profile_picture_url'] = site_url('/files/profile_pic/' . $picture_name);
            $data_img['created_by'] = $this->user_auth_lib->get('user_id');
            $data_img['company_id'] = $company_id;


            if (!$this->db->get_where(TBL_EMP_PICTURE, ['userid' => $userid, 'company_id' => $company_id])->row()) {
                $data_img['date_created'] = date('Y-m-d h:i:s');
                return $this->db->insert(TBL_EMP_PICTURE, $data_img);
            } else {
                $data_img['date_updated'] = date('Y-m-d h:i:s');
                $this->db->where('userid', $userid)
                        ->where('company_id', $company_id)
                        ->update(TBL_EMP_PICTURE, $data_img);
                return $this->db->affected_rows() > 0;
            }
        }
    }

    public function fetch_all_users($company_id, $status = null) {
        if (!is_null($status)) {
            $this->db->where('u.status', $status);
        }
        
        return $this->db
                        ->select('u.*, p.profile_picture_url')
                        ->from(TBL_USERS . ' as u')
                        ->join(TBL_EMP_PICTURE . ' as p', 'p.userid=u.userid', 'left')
                        ->where('u.company_id', $company_id)
                        ->get()->result();
    }

    public function removeProfilePicture($userid, $company_id) {
        return $this->db->where('userid', $userid)
                        ->where('company_id', $company_id)
                        ->delete(TBL_EMP_PICTURE);
    }

    public function fetchDeptRecord($userid, $company_id, $table) {
        return $this->db->get_where($table, ['department_id' => $userid, 'company_id' => $company_id])->result();
    }

    public function updateDepartment($userid, $company_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $this->db->update(TBL_DEPT, $data, ['department_id' => $userid, 'company_id' => $company_id]);

        return $this->db->affected_rows() > 0;
    }

    public function fetchLocationRecord($userid, $company_id, $table) {
        return $this->db->get_where($table, ['location_id' => $userid, 'company_id' => $company_id])->result();
    }

    public function updateLocation($userid, $company_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $this->db->update(TBL_LOC, $data, ['location_id' => $userid, 'company_id' => $company_id]);

        return $this->db->affected_rows() > 0;
    }

    public function fetchRoleRecord($userid, $company_id, $table) {
        return $this->db->get_where($table, ['role_id' => $userid, 'company_id' => $company_id])->result();
    }

    public function updateRole($userid, $company_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $this->db->update(TBL_ROLES, $data, ['role_id' => $userid, 'company_id' => $company_id]);

        return $this->db->affected_rows() > 0;
    }

    public function fetchLevelRecord($userid, $company_id, $table) {
        return $this->db->get_where($table, ['level_id' => $userid, 'company_id' => $company_id])->result();
    }

    public function updateLevel($userid, $company_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $this->db->update(TBL_LEVELS, $data, ['level_id' => $userid, 'company_id' => $company_id]);

        return $this->db->affected_rows() > 0;
    }

}
