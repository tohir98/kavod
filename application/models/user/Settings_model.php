<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Settings_model
 *
 * @author tohir
 */
class Settings_model extends CI_Model {

    const DEFAULT_DEPT = 'Corporate';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function createDefaultDept($company_id) {
        if ((int) $company_id > 0) {
            if (!$this->hasDefaultDept($company_id)) {
                $this->db->insert(TBL_DEPT, [
                    'company_id' => $company_id,
                    'department' => self::DEFAULT_DEPT,
                    'date_created' => date('Y-m-d'),
                    'creator_id' => $this->user_auth_lib->get('user_id'),
                    'parent_id' => 0
                ]);
            }
        }
    }

    public function hasDefaultDept($company_id) {
        return $this->db->get_where(TBL_DEPT, ['company_id' => (int) $company_id])->row();
    }

    public function fetchDepts($company_id) {
        return $this->db
                        ->select('d.*, u.first_name, u.last_name, d2.department as parent, CONCAT(u2.first_name, " ", u2.last_name) as dept_head')
                        ->from(TBL_DEPT . ' as d')
                        ->join(TBL_USERS . ' as u', 'u.userid=d.creator_id')
                        ->join(TBL_USERS . ' as u2', 'u2.userid=d.department_head_id', 'left')
                        ->join(TBL_DEPT . ' as d2', 'd2.department_id=d.parent_id', 'left')
                        ->where('d.company_id', $company_id)
                        ->get()->result();
    }

    public function fetchCompanyLocations($company_id) {
        return $this->db
                        ->select('l.*, u.first_name, u.last_name, s.state')
                        ->from(TBL_LOC . ' as l')
                        ->join(TBL_USERS . ' as u', 'u.userid=l.creator_id')
                        ->join(TBL_STATES . ' as s', 's.state_id=l.state_id')
                        ->where('l.company_id', $company_id)
                        ->get()->result();
    }
    
    public function fetchCompanyRoles($company_id) {
        return $this->db
                        ->select('r.*, u.first_name, u.last_name')
                        ->from(TBL_ROLES . ' as r')
                        ->join(TBL_USERS . ' as u', 'u.userid=r.creator_id')
                        ->where('r.company_id', $company_id)
                        ->get()->result();
    }
    
    public function fetchCompanyLevels($company_id) {
        return $this->db
                        ->select('l.*, u.first_name, u.last_name')
                        ->from(TBL_LEVELS . ' as l')
                        ->join(TBL_USERS . ' as u', 'u.userid=l.creator_id')
                        ->where('l.company_id', $company_id)
                        ->get()->result();
    }
    
    public function create($table, $company_id, $data) {
        if (empty($data)){
            return FALSE;
        }
        
        $data['date_created'] = date('Y-m-d');
        $data['creator_id'] = $this->user_auth_lib->get('user_id');
        $data['company_id'] = $company_id;
        
        return $this->db->insert($table, $data);
    }

}
