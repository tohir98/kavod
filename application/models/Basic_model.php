<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of basic_model
 *
 * @author TOHIR
 */
class Basic_model extends CI_Model {
    
    protected static $account_type;
    protected static $statuses;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        
        static::$account_type = [ACCOUNT_TYPE_FREE_TRIAL => 'Free Trial', ACCOUNT_TYPE_PREMIUM => 'Premium'];
        static::$statuses = [ACCOUNT_STATUS_ACTIVE => 'Active', ACCOUNT_STATUS_INACTIVE => 'Inactive'];
    }

    public function fetch_all_records($table, $where = NULL) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result();
        } else {
            $result = $this->db->get_where($table, $where)->result();
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function saveReg($data) {
        if ($this->_recordExist($data['email'])) {
            return FALSE;
        }

        $data['dateof_birth'] = date('Y-m-d', strtotime($data['dateof_birth']));
        $data['date_added'] = date('Y-m-d h:i:s');

        $this->db->trans_start();
        $user_id = $this->_createStudentUser($data);

        $this->db->insert('student_event_reg', $data);
        $this->_sendConfirmationEmail($user_id, array('first_name' => $data['first_name'], 'username' => $data['email']));
        $this->db->trans_complete();

        return TRUE;
    }

    private function _createStudentUser($data) {
        $user_data = array(
            'username' => $data['email'],
            'password' => $this->user_auth_lib->encrypt(DEFAULT_PASSWORD),
            'status' => USER_STATUS_INACTIVE,
            'created_at' => date('Y-m-d h:i:s'),
            'user_type' => USER_TYPE_STUDENT,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
        );

        $this->db->insert(TBL_USERS, $user_data);
        return $this->db->insert_id();
    }

    private function _sendConfirmationEmail($user_id, $params) {
        $mail_data = array(
            'header' => REG_CONFIRMATION,
            'first_name' => $params['first_name'],
            'username' => $params['username'],
            'verify_link' => site_url('verify_email/' . $user_id . '/' . $this->user_auth_lib->encrypt($params['username'] . $user_id) . '/acc'),
        );

        $msg = $this->load->view('email_templates/student_reg_confirmation', $mail_data, true);

        return $this->mailer
                ->sendMessage('Welcome | OAAStudy', $msg, $params['username']);
    }

    private function _recordExist($email) {
        return $this->db->get_where('student_event_reg', ['email' => $email])->result();
    }

    public function delete($table, $array) {
        $this->db->where($array);
        $q = $this->db->delete($table);
        return $q;
    }
    
    public function deleteBulkMarket($table, $ids) {
        if (empty($ids)){
            return FALSE;
        }
        
        $sql = "DELETE FROM {$table} WHERE market_price_id IN ( ";
        
        $join = '';
        foreach ($ids as $id) {
            $join .= $id . ',' ;
        }
        
        $sql .= rtrim($join, ','). " )";
        
        
        return $this->db->query($sql);
    }

    public function update($table, $data, $where) {
        foreach ($where as $k => $v) {
            $this->db->where($k, $v);
        }
        $query = $this->db->update($table, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function insert($table, $data) {
        return $this->db->insert($table, $data);
        //return $this->db->insert_id();
    }
    
    public static function getAccountTypes() {
        return static::$account_type;
    }
    
    public static function getAccountStatuses() {
        return static::$statuses;
    }

    public function get_leave_period($table, $data)
    {
        $result = $this->db->get_where($table, $data)->row();
        return $result;
    }

    public function add_leave_period($table, $data, $where)
    {
        //$newData = array_merge($data, $where);
        if($this->checkInput($table, $where)){
            $this->update($table, $data, $where);
            return true;     
        }else{
            $this->insert($table, $data);
            return true;            
        }
    }

    private function checkInput($table, $where)
    {
        $result = $result = $this->db->get_where($table, $where)->result();
        if (empty($result)) {
            return false;
        }else{
            return true;
        }
    }

    

    public function edit_leave_type($table, $type_id, $data)
    {
        if (empty($data)){
            return FALSE;
        }
        
        $where = ['type_id' => $type_id];
        $this->update($table, $data, $where);
        
        return true;
    }

    public function edit_entitlement($table, $ent_id, $data)
    {
        if (empty($data)){
            return FALSE;
        }
        
        $where = ['id' => $ent_id];
        $this->update($table, $data, $where);
        
        return true;
    }

    public function fetch_leave_types($table, $where)
    {
        $result = $this->db->get_where($table, $where)->result();
        return $result;
    }

    public function fetch_leave_type($table, $where)
    {
        $result = $this->db->get_where($table, $where)->row();
        return $result;
    }

    public function create_work_week($table, $company_id, $user_id, $data)
    {
        $data['creator_id'] = $user_id;
        $data['company_id'] = $company_id;
        $check = $this->fetch_leave_type($table, ['company_id' => $company_id]);

        if(!empty($check)){
            if($this->update($table, $data, ['company_id' => $company_id])){
                return true;
            }else{
                return false;
            }
        }else{
            if($this->insert($table, $data)){
                return true;
            }else{
                return false;
            }
        }
    }

    public function add_holiday($table, $company_id, $user_id, $data)
    {
        $date = new DateTime($data['hol_date']);
        $holdate = $date->format('Y-m-d H:i:s');

        $data['creator_id'] = $user_id;
        $data['company_id'] = $company_id;
        $data['hol_date'] = $holdate;

        if($this->insert($table, $data)){
            return true;
        }else{
            return false;
        }
    }

    public function add_entitlement($table1, $table2, $company_id, $user_id, $data)
    {
        $validfrom = new DateTime($data['valid_from']);
        $newvalidfrom = $validfrom->format('Y-m-d H:i:s');

        $validto = new DateTime($data['valid_to']);
        $newvalidto = $validto->format('Y-m-d H:i:s');

        $data['creator_id'] = $user_id;
        $data['company_id'] = $company_id;
        $data['valid_from'] = $newvalidfrom;
        $data['valid_to'] = $newvalidto;

        if($this->insert($table1, $data)){
            $sum_check = $this->db->get_where($table2, ['employee_id' => $data['employee_id']])->row();
            if (empty($sum_check)) {
                $this->insert($table2, ['employee_id' => $data['employee_id'], 'company_id' => $data['company_id'], 'days_sum' => $data['days']]);
            }else{
                $current_sum = (int)$sum_check->days_sum;
                $new_sum = (int)$data['days'];
                $cummulative = $current_sum + $new_sum;
                $this->update($table2, ['days_sum' => $cummulative], ['employee_id' => $data['employee_id']]);
            }
            return true;
        }else{
            return false;
        }

    }

    public function fetch_entitlements($table1, $table2, $where)
    {
        $result = $this->db->get_where($table2, $where)->result();
        $results;
        $resulting;
        // var_dump($result);
        // exit;
        if(empty($result)){
            return $result;
        }else{
            foreach ($result as $val) {
                $results = $this->db->get_where(TBL_USERS, ['userid' => $val->employee_id])->row();
                $object = new stdClass();
                $object->id = $val->id;
                $object->employee_id = $val->employee_id;
                $object->days_sum = $val->days_sum;
                // $object->recurring = $val->recurring;
                // $object->leave_type = $val->leave_type;
                // $object->valid_from = $val->valid_from;
                // $object->valid_to = $val->valid_to;
                // $object->days = $val->days;
                $object->company_id = $val->company_id;
                // $object->creator_id = $val->creator_id;
                $object->name = $results->name;
                //unset($result);
                $resulting[] = $object;
            }
            // var_dump($resulting);
            // exit;
            return $resulting;
        }
        
    }

    public function fetch_entitlement($table, $where)
    {
        $result = $this->db->get_where($table, $where)->result();
        $results;
        $resulting;
        // var_dump($result);
        // exit;
        if(empty($result)){
            return $result;
        }else{
            foreach ($result as $val) {
                $results = $this->db->get_where(TBL_LEAVE_TYPE, ['type_id' => $val->leave_type])->row();
                $object = new stdClass();
                $object->id = $val->id;
                $object->employee_id = $val->employee_id;
                $object->recurring = $val->recurring;
                $object->leave_type = $val->leave_type;
                $object->valid_from = $val->valid_from;
                $object->valid_to = $val->valid_to;
                $object->days = $val->days;
                $object->company_id = $val->company_id;
                $object->creator_id = $val->creator_id;
                $object->name = $results->type_name;
                //unset($result);
                $resulting[] = $object;
            }
            // var_dump($resulting);
            // exit;
            return $resulting;
        }
        
    }

}
