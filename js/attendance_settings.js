var permissionApp = angular.module('permission', ['app.Kavod']);

permissionApp.run(function ($rootScope, $http, SiteUrl, $timeout) {
    $rootScope.shiftApprovers = window.shiftApprovers || [];
    $rootScope.timeSheetApprovers = window.timeSheetApprovers || [];
    $rootScope.employees = window.employees || {};
    $rootScope.accessLevels = window.accessLevels || {};


});

permissionApp.controller('shiftCtrl', function ($scope, $http, SiteUrl) {
    $scope.addApprover = function () {
        // show modal
        $("#approval_type").val('shift');
        $("#approval_modal").modal();
    };

    $scope.removeShiftApprover = function ($idx) {
        $scope.shiftApprovers.splice($idx, 1);

        $.post(window.shiftApprovalUrl,
                {
                    'approvers[]': $scope.shiftApprovers,
                    'approval_type': 'shift',
                },
                function (data) {
                    if (data === 'OK') {
                        toastr["success"]("Shift approver removed successfully");
                    } else {
                        toastr["error"]("Unable to remove shift approver at moment, pls try again");
                    }
                });

    };

    $scope.updateApprover = function () {
        console.log($("#shift_approver_frm").serializeArray());
        $.post(window.shiftApprovalUrl, $("#shift_approver_frm").serializeArray(),
                function (data) {
                    if (data === 'OK') {
                        toastr["success"]("Shift approver updated successfully");
                    } else {
                        toastr["error"]("Unable to update shift approver at moment, pls try again");
                    }
                });
    };
});

permissionApp.controller('timesheetCtrl', function ($scope, $http, SiteUrl) {
    $scope.addApprover = function () {
        // show modal
        $("#approval_type").val('timesheet');
        $("#approval_modal").modal();
    };

    $scope.removeTimesheetApprover = function ($idx) {
        $scope.timeSheetApprovers.splice($idx, 1);

        $.post(window.shiftApprovalUrl,
                {
                    'approvers[]': $scope.timeSheetApprovers,
                    'approval_type': 'timesheet'
                },
                function (data) {
                    if (data === 'OK') {
                        toastr["success"]("Timesheet approver removed successfully");
                    } else {
                        toastr["error"]("Unable to remove timesheet approver at moment, pls try again");
                    }
                });

    };

    $scope.updateTimesheetApprover = function () {
        $.post(window.shiftApprovalUrl, $("#timesheet_approver_frm").serializeArray(),
                function (data) {
                    toastr["success"]("Timesheet approver updated    successfully");
                });
    };
});

$(".bswitch").bootstrapSwitch();
$('.edit_shift_perm').click(function (eve) {

    eve.preventDefault();
    $('#modal_shift_permission').modal('show');
    $('#modal_shift_permission').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

    var page = $(this).attr("href");
    $.get(page, function (html) {

        $('#modal_shift_permission').html('');
        $('#modal_shift_permission').html(html).show();

    });
});

$('.add_approver').click(function (eve) {

    eve.preventDefault();
    $('#analytics_modal').modal('show');
    $('.chzn-container').css('width', '100%');
    $('.chzn-drop').css('width', '100%');
    $('.modal-body').css('overflow-y', 'visible');

});

$(function () {
    $('.sToggle').click(function (e) {
        e.preventDefault();
        var h = this.href;
        var message = parseInt($(this).data('enabled')) ? 'Are you sure you want to disable this setting? ' : 'Are you sure you want to enable this settings ?';
        TalentBase.doConfirm({
            title: 'Confirm',
            message: message,
            onAccept: function () {
                window.location = h;
            }
        });
    });
});

$('.bswitch').on('switchChange.bootstrapSwitch', function (event, state) {
    var id_ = $(this).data('id');
    var value_ = $(this).data('val');

    var new_val = parseInt(value_) > 0 ? 0 : 1;


    $.post(
            ACTION_Url + '/' + id_ + '/' + value_,
            function (res) {
                hide_loader();
                if (res === 'OK') {
                    toastr["success"]("Operation was successful");
                    $("#" + id_).data('val', new_val);
                } else {
                    toastr["error"]("Sorry, Unable complete your request.");
                }
            }
    );

    return false;

});

$('.edit_shift_access').click(function (eve) {
    eve.preventDefault();
    $('#shif_access_modal').modal('show');
    $('.chzn-container').css('width', '100%');
    $('.chzn-drop').css('width', '100%');
    $('.modal-body').css('overflow-y', 'visible');

});

$('.edit_timesheet_access').click(function (eve) {
    eve.preventDefault();
    $('#timesheet_access_modal').modal('show');
    $('.chzn-container').css('width', '100%');
    $('.chzn-drop').css('width', '100%');
    $('.modal-body').css('overflow-y', 'visible');

});