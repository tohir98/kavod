var recordApp = angular.module('record', ['cgBusy']);

recordApp.controller('recordCtrl', function ($scope, $http) {

    $scope.clocks = [];
    $scope.data = {department_id:'', location_id:'', date:currentDate};
    
    var getSiteUrl = function () {
        return window.location.protocol + '//' + window.location.host + '/';
    };
    
    $scope.filter = function(){
        var data = {
                department_id: $scope.data.department_id,
                location_id: $scope.data.location_id,
                date: $scope.data.date
            };
        console.log(data);
        
        $scope.myPromise = $http.post(getSiteUrl() + "attendance/fetchAttendanceReportJson/", data).success(function (response) {
                    $scope.clocks = response;
                });
        
//        $scope.myPromise = $http.get(getSiteUrl() + "attendance/fetchAttendanceReportJson/" + $scope.data.department_id + '/' + $scope.data.location_id )
//                .success(function (response) {
//                    $scope.clocks = response;
//                });
    };



});

recordApp.filter('capitalize', function () {
    return function (input) {
        if (!input) {
            return input;
        }

        var parts = input.split(/[_\s]+/);
        for (var i = 0; i < parts.length; i++) {
            parts[i] = parts[i][0].toUpperCase() + parts[i].substr(1);
        }
        return parts.join(' ');
    };
});

