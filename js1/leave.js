$(document).ready(function(){

	var mon = $('#work_mon').data('var');
	var tue = $('#work_tue').data('var');
	var wed = $('#work_wed').data('var');
	var thu = $('#work_thu').data('var');
	var fri = $('#work_fri').data('var');
	var sat = $('#work_sat').data('var');
	var sund = $('#work_sun').data('var');

	$("#work_mon option[value="+mon+"]").prop("selected", true);
	$("#work_tue option[value="+tue+"]").prop("selected", true);
	$("#work_wed option[value="+wed+"]").prop("selected", true);
	$("#work_thu option[value="+thu+"]").prop("selected", true);
	$("#work_fri option[value="+fri+"]").prop("selected", true);
	$("#work_sat option[value="+sat+"]").prop("selected", true);
	$("#work_sun option[value="+sund+"]").prop("selected", true);

	//$("#start_month option[value="+strtmonth+"]").prop("selected", true);



	var month_id = "";
	$.ajax({
        url: 'months_pop',
        type: 'GET',
        success: function(data) {
        	var sdata = jQuery.parseJSON(data);
        	var options = "";
			$.each(sdata, function(index, val) {
				//console.log(val);
				// if (index < 10) index="0" + index;
				options += "<option value='" + index + "'>" + val + "</option>";
			});
			$('#start_month').append(options);
        	// console.log(sdata);
        },
        error: function (xhr) {
        //alert(xhr);
        }   
    });

    var strtday = $('#start_month').data('month');
	var strtmonth = strtday - 1;
	$("#start_month option[value="+strtmonth+"]").prop("selected", true);

	//$('#start_month').append(options);
	$('#subButton').click(function() {
		$('.start_period').removeAttr("disabled");
		$('#sub').html("<input type='submit' class='btn btn-primary btn-flat' value='Save'>");
	});

	$('#start_month').change(function(event) {
		var options = "";
		month_id = $(this).val();
        if (month_id == "" ){
            $('#start_day').html("<option value=''>--Select Day--</option>");
        }
        else{
        	$('#start_day').html('<option>Loading...</option>');
        	var currentTime = new Date();
        	var year = currentTime.getFullYear();
        	var date_num = 1;
        	console.log(getDaysInMonth(month_id, year));
        	$.each(getDaysInMonth(month_id, year), function(index, val) {
				//console.log(val);
				options += "<option value='" + date_num + "'>" + date_num + "</option>";
				date_num++;
			});
        	$('#start_day').html("<option value=''>--Select Day--</option>");
        	$('#start_day').append(options);
    	}
	});

	$('#start_day').change(function(event) {
		var month_text = $( "#start_month option:selected" ).text();
		var currentTime = new Date();
        var year = currentTime.getFullYear();
        day_id = $(this).val();
		var date = new Date(year, month_id, day_id);

		var end_day = date.getDate()
		if (end_day < 10) end_day="0" + end_day;
		 
		var end_month = date.getMonth()+ 1
		if (end_month < 10) end_month = "0" + end_month;

		var end_year = date.getFullYear()+ 1
		 
		start_date = end_month + "/" + end_day + "/" + year;
		end_date = end_month + "/" + end_day + "/" + end_year;
		//alert(end_date);

		$('#end_date').html(month_text + ' ' + end_day);
		$('#subb').html("<input type='hidden' name='start_date' id='ent_employee' class='form-control' value='" + start_date + "'>");
		$('#subb').append("<input type='hidden' name='end_date' id='ent_employee' class='form-control' value='" + end_date + "'>");
	});

	/**
	 * @param {int} The month number, 0 based
	 * @param {int} The year, not zero based, required to account for leap years
	 * @return {Date[]} List with date objects for each day of the month
	 */
	function getDaysInMonth(month, year) {
	     var date = new Date(year, month, 1);
	     var days = [];
	     while (date.getMonth() == month) {
	        days.push(new Date(date));
	        date.setDate(date.getDate() + 1);
	     }
	     return days;
	}

});