var studentDirApp = angular.module('studentDir', []);

studentDirApp.controller('listCtrl', function ($scope) {

    $scope.config = window.studentDirConfig || {};

    $scope.toggleAll = function () {
        angular.forEach($scope.config.students, function (student) {
            student.is_selected = !student.is_selected;
        });
    };
    
    $scope.sendSms = function (student) {
        $("#lblRecepientName").text('Message will be sent to ' + student.g_fname + ' ' + student.g_lname);
        $("#lblrecepient").text(student.g_phone);
        $("#recepient").val(student.g_phone);
        $("#modal_send_sms").modal();
    };
    
    $scope.sendEmail = function (student) {
        $("#lblRecepientName_email").text('Message will be sent to ' + student.g_fname + ' ' + student.g_lname);
        $("#lblrecepient_email").text(student.g_email);
        $("#recepient_email").val(student.g_email);
        $("#modal_send_email").modal();
    };
    
    $scope.formatDate = function(date){
        if (!date){
            return;
        }
                var date = date.split("-").join("/");
                var dateOut = new Date(date);
                return dateOut;
        }; 



});

studentDirApp.filter('capitalize', function () {
    return function (input) {
        if (!input) {
            return input;
        }

        var parts = input.split(/[_\s]+/);
        for (var i = 0; i < parts.length; i++) {
            parts[i] = parts[i][0].toUpperCase() + parts[i].substr(1);
        }
        return parts.join(' ');
    };
});

